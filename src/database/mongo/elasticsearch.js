'use strict';

module.exports = function (db, module) {
  // 增加相关话题mongodb 存储
	module.elasticSearchFind = function (key, callback) {
    if(!key) {
      return callback()
    }
		db.collection("elasticsearch").findOne({
			_key: 'tid:' + key
		}, function (err, result) {
			if (err) {
				return callback(err)
      }
			if (!result || (result.timestamp && (Date.now() - result.timestamp > 24*60*60*1000))) {
				return callback(null, {
					exit: false
				})
			} else {
				return callback(null, {
          exit: true,
          title: JSON.parse(result.title)
				})
			}
		})
	}

	module.elasticSearchSave = function (key, value, callback) {
    if (!key) {
    	return callback()
    }
		db.collection('elasticsearch').update({
				_key: 'tid:' + key
			}, {
				$set: {
					timestamp: Date.now(),
					title: JSON.stringify(value)
				}
			}, {
				upsert: true
			},
			function (err, data) {
				if (!err) {
					return callback(null)
				}
			})
	}
};