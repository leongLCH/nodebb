'use strict';

var async = require('async');
var passport = require('passport');
var passportLocal = require('passport-local').Strategy;
var nconf = require('nconf');
var winston = require('winston');
var express = require('express');
var request = require('request');
var controllers = require('../controllers');
var plugins = require('../plugins');
var hotswap = require('../hotswap');
var Crypto = require('./encypt');
var Users = require('../user');
var db = require('../database');
var _ = require('lodash');
var nbbAuthController = require('../controllers/authentication');

var loginStrategies = [];

var Auth = module.exports;
Auth.initialize = function (app, middleware) {
	app.use(passport.initialize());
	app.use(passport.session());

	app.use(function (req, res, next) {
		var isSpider = req.isSpider();
		req.loggedIn = !isSpider && !!req.user;
		// if (isSpider) {
		// 	req.uid = -1;
		// } else 
		if (req.user) {
			req.uid = parseInt(req.user.uid, 10);
		} else {
			req.uid = 0;
		}
		next();
	});

	// sign校验
	// app.use(function (req, res, next) {
	// 	if (req.url.startsWith(nconf.get('relative_path') + '/t/'))next();
	// 	else if (req.uid) {
	// 		db.getSortedSignBySessionID(req.sessionID, function (err, sign) {
	// 			// if(_.head(sign) === '' && req.cookies.sign) { 
	// 			// 	// var token = Crypto.decode(req.cookies.sign).split('.')[0];
	// 			// 	// res.redirect(nconf.get('relative_path') + '/t/' + token + '?redirect=' + req.path);
	// 			// 	reLogin(req.cookies.sign, req, res, next)
	// 			//  }
	// 			if (req.cookies.sign && _.head(sign) !== req.cookies.sign) {
	// 				reLogin(req.cookies.sign, req, res, next)
	// 			} else {
	// 				next()
	// 			}
	// 		})
	// 	} else {
	// 		next()
	// 	}
	// })

	app.use(function (req, res, next) {
		if (req.url.startsWith(nconf.get('relative_path') + '/t/')) next();
		else if (!req.cookies.sign) {
			if (req.uid === 1 || req.uid === 0) {
				next();
			} else {
				req.body.noscript = 'true';
				controllers.authentication.logout(req, res, next);
			}
		} else {
			db.getSortedSignBySessionID(req.sessionID, function (err, sign) {
				if (!err) {
					if ((_.head(sign) !== req.cookies.sign) || req.uid === 0) {
						reLogin(req.cookies.sign, req, res, next);
					} else {
						next();
					}
					// if ((_.head(sign) !== req.cookies.sign)) {
					// 	reLogin(req.cookies.sign, req, res, next);
					// } else {
					// 	next();
					// }
				}
			});
		}
	});


	// app.use(function (req, res, next) {
	// 	if (req.url.startsWith(nconf.get('relative_path') + '/t/')) next();
	// 	else if (req.cookies.sign && !req.user) {
	// 		var token = Crypto.decode(req.cookies.sign).split('.')[0];
	// 		res.redirect(nconf.get('relative_path') + '/t/' + token + '?redirect=' + req.path);
	// 	} else if (!req.cookies.sign && req.user && req.user.uid !== 1) {
	// 		req.body.noscript = 'true';
	// 		controllers.authentication.logout(req, res, next);
	// 	} else {
	// 		next();
	// 	}
	// });
	Auth.app = app;
	Auth.middleware = middleware;
};

Auth.getLoginStrategies = function () {
	return loginStrategies;
};

Auth.reloadRoutes = function (callback) {
	var router = express.Router();
	router.hotswapId = 'auth';

	loginStrategies.length = 0;

	if (plugins.hasListeners('action:auth.overrideLogin')) {
		winston.warn('[authentication] Login override detected, skipping local login strategy.');
		plugins.fireHook('action:auth.overrideLogin');
	} else {
		passport.use(new passportLocal({
			passReqToCallback: true
		}, controllers.authentication.localLogin));
	}

	async.waterfall([
		function (next) {
			plugins.fireHook('filter:auth.init', loginStrategies, next);
		},
		function (loginStrategies, next) {
			loginStrategies.forEach(function (strategy) {
				if (strategy.url) {
					router.get(strategy.url, passport.authenticate(strategy.name, {
						scope: strategy.scope,
						prompt: strategy.prompt || undefined,
					}));
				}

				router.get(strategy.callbackURL, passport.authenticate(strategy.name, {
					successReturnToOrRedirect: nconf.get('relative_path') + (strategy.successUrl !== undefined ? strategy.successUrl : '/'),
					failureRedirect: nconf.get('relative_path') + (strategy.failureUrl !== undefined ? strategy.failureUrl : '/login'),
				}));
			});

			router.post('/register', Auth.middleware.applyCSRF, Auth.middleware.applyBlacklist, controllers.authentication.register);
			router.post('/register/complete', Auth.middleware.applyCSRF, Auth.middleware.applyBlacklist, controllers.authentication.registerComplete);
			router.get('/register/abort', controllers.authentication.registerAbort);
			router.post('/login', Auth.middleware.applyCSRF, Auth.middleware.applyBlacklist, controllers.authentication.login);
			router.post('/logout', Auth.middleware.applyCSRF, controllers.authentication.logout);

			hotswap.replace('auth', router);
			next();
		},
	], callback);
};

passport.serializeUser(function (user, done) {
	done(null, user.uid);
});

passport.deserializeUser(function (uid, done) {
	done(null, {
		uid: uid,
	});
});

function reLogin(sign, req, res, callback) {
	var token = Crypto.decode(sign).split('.')[0];
	winston.verbose('[session]', token);
	fetchUserProfile(token, function (err, resp) {
		if (err) {
			res.send('error' + err);
		} else {
			if (resp && resp.user_id) {
				getOrCreatUser(resp, function (err, uid) {
					Users.getUserField(uid, 'myquantid', function (err, id) {
						winston.verbose('[session]myquantid' + id + err);
					});
					if (!err && uid) {
						nbbAuthController.doLogin(req, uid, req.cookies.sign, function () {
							winston.verbose('[session]login' + uid);
							callback();
						});
					} else {
						res.send('err:' + err + 'resp:' + uid);
					}
				});
			} else {
				callback()
			}

		}
	});

}

function fetchUserProfile(token, cb) {
	var url = nconf.get('sso').url + nconf.get('sso').getBaseInfo;
	var options = {
		url: url,
		headers: {
			Authorization: 'Bearer ' + token
		},
		timeout: 5000
	};

	request(options, function callback(error, response, body) {
		if (!error && response.statusCode === 200) {
			try {
				cb(null, JSON.parse(body));
			} catch (e) {
				cb(e, null);
			}
		} else {
			cb(error, null);
		}
	});
}


function getOrCreatUser(profile, cb) {
	getUidByMyquantId(profile.user_id, function (err, uid) {
		winston.verbose('[session]user exist' + err + uid);
		if (err) {
			winston.verbose('[session]create user error ' + err);
			cb(err, 0);
		} else if (uid) {
			Users.setUserField(uid, 'picture', profile.avatar_file || '', function (err, resp) {
				winston.verbose('[session]update profile ' + err + resp);
				cb(null, uid);
			});
		} else {
			winston.verbose('[session]create new user');
			createUser(profile, cb);
		}
	});
}

function createUser(profile, cb) {
	Users.create({
		username: profile.nick_name,
		myquantid: profile.user_id,
		picture: profile.avatar_file || '',
		aboutme: profile.introduction
	}, cb);
}

function getUidByMyquantId(id, cb) {
	db.sortedSetScore('myquantid:uid', id, cb);
}