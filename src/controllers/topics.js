'use strict';


var async = require('async');
var nconf = require('nconf');

var user = require('../user');
var meta = require('../meta');
var topics = require('../topics');
var posts = require('../posts');
var privileges = require('../privileges');
var plugins = require('../plugins');
var helpers = require('./helpers');
var categories = require('../categories');
var pagination = require('../pagination');
var utils = require('../utils');
var _ = require('lodash');
var validator = require('validator');
var group = require('../groups');
var db = require('../database')

var topicsController = module.exports;

topicsController.get = function (req, res, callback) {
	var tid = req.params.topic_id;
	var currentPage = parseInt(req.query.page, 10) || 1;
	var pageCount = 1;
	var userPrivileges;
	var settings;
	var rssToken;
	var postCounts;
	var monthlyPopular;

	var getResults;
	var allCategoriesData;
	if ((req.params.post_index && !utils.isNumber(req.params.post_index)) || !utils.isNumber(tid)) {
		return callback();
	}

	async.waterfall([
		function (next) {
			async.parallel({
				privileges: function (next) {
					privileges.topics.get(tid, req.uid, next);
				},
				settings: function (next) {
					user.getSettings(req.uid, next);
				},
				topic: function (next) {
					topics.getTopicData(tid, next);
				},
				rssToken: function (next) {
					user.auth.getFeedToken(req.uid, next);
				},
				// 获取所有版块
				allCategoriesData: function (next) {
					categories.getCategoriesByPrivilege('cid:0:children', req.uid, 'find', next);
				},
				// 最近热门
				monthlyPopular: function (next) {
					topics.getPopularTopics('month', req.uid, 0, 6, next);
				},
				// getMyFollow: function (next) {
				// 	var method = 'getFollowing';
				// 	user[method](req.uid, start, stop, next);
				// },
				// bookmarks: function (next) {
				// 	posts.hasBookmarked(tid, req.uid, next);
				// },
			}, next);
		},
		function (results, next) {
			if (!results.topic) {
				return callback();
			}
			allCategoriesData = results.allCategoriesData;
			monthlyPopular = results.monthlyPopular.topics;
			getResults = results;
			topics.getMainPost(results.topic && results.topic.tid, req.uid, next);
		},
		function (results, next) {
			getResults.topic.content = results.content;
			getResults.topic.bookmarked = results.bookmarked;
			if (!getResults.topic) {
				return callback();
			}
			userPrivileges = getResults.privileges;
			rssToken = getResults.rssToken;

			if (!userPrivileges['topics:read'] || (parseInt(getResults.topic.deleted, 10) && !userPrivileges.view_deleted)) {
				return helpers.notAllowed(req, res);
			}
			// if (!res.locals.isAPI && (!req.params.slug || getResults.topic.slug !== tid + '/' + req.params.slug) && (getResults.topic.slug && getResults.topic.slug !== tid + '/')) {
			// 	var url = '/topic/' + getResults.topic.slug;
			// 	if (req.params.post_index) {
			// 		url += '/' + req.params.post_index;
			// 	}
			// 	if (currentPage > 1) {
			// 		url += '?page=' + currentPage;
			// 	}
			// 	return helpers.redirect(res, url);
			// }

			settings = getResults.settings;
			var postCount = parseInt(getResults.topic.postcount, 10);
			// 暂时令主题回复数量为返回的postCount
			postCounts = postCount;
			pageCount = Math.max(1, Math.ceil((postCount) / settings.postsPerPage));
			getResults.topic.postcount = postCount - 1;
			if (utils.isNumber(req.params.post_index) && (req.params.post_index < 1 || req.params.post_index > postCount)) {
				return helpers.redirect(res, '/topic/' + req.params.topic_id + '/' + req.params.slug + (req.params.post_index > postCount ? '/' + postCount : ''));
			}

			if (settings.usePagination && (currentPage < 1 || currentPage > pageCount)) {
				return callback();
			}

			var set = 'tid:' + tid + ':posts';
			var reverse = false;
			// `sort` qs has priority over user setting
			var sort = req.query.sort || settings.topicPostSort;
			if (sort === 'newest_to_oldest') {
				reverse = true;
			} else if (sort === 'most_votes') {
				reverse = true;
				set = 'tid:' + tid + ':posts:votes';
			}

			var postIndex = 0;

			req.params.post_index = parseInt(req.params.post_index, 10) || 0;
			if (reverse && req.params.post_index === 1) {
				req.params.post_index = 0;
			}
			if (!settings.usePagination) {
				if (req.params.post_index !== 0) {
					currentPage = 1;
				}
				if (reverse) {
					postIndex = Math.max(0, postCount - (req.params.post_index || postCount) - Math.ceil(settings.postsPerPage / 2));
				} else {
					postIndex = Math.max(0, (req.params.post_index || 1) - Math.ceil(settings.postsPerPage / 2));
				}
			} else if (!req.query.page) {
				var index;
				if (reverse) {
					index = Math.max(0, postCount - (req.params.post_index || postCount) + 2);
				} else {
					index = Math.max(0, req.params.post_index) || 0;
				}

				currentPage = Math.max(1, Math.ceil(index / settings.postsPerPage));
			}

			var start = ((currentPage - 1) * settings.postsPerPage) + postIndex;
			var stop = start + settings.postsPerPage - 1;
			topics.getTopicWithPosts(getResults.topic, set, req.uid, start, stop, reverse, next);
		},
		function (topicData, next) {
			if (topicData.category.disabled) {
				return callback();
			}

			topics.modifyPostsByPrivilege(topicData, userPrivileges);

			plugins.fireHook('filter:controllers.topic.get', {
				topicData: topicData,
				uid: req.uid
			}, next);
		},
		function (data, next) {
			buildBreadcrumbs(data.topicData, next);
		},
		function (data, next) {
			user.isFollowing(req.uid, data.uid, function (err, result) {
				if (err) {
					next(null, data)
				} else {
					data.hasFollowing = result;
					next(null, data)
				}
			});
		},
		function (topicData) {
			topicData.privileges = userPrivileges;
			topicData.topicStaleDays = parseInt(meta.config.topicStaleDays, 10) || 60;
			topicData['reputation:disabled'] = parseInt(meta.config['reputation:disabled'], 10) === 1;
			topicData['downvote:disabled'] = parseInt(meta.config['downvote:disabled'], 10) === 1;
			topicData['feeds:disableRSS'] = parseInt(meta.config['feeds:disableRSS'], 10) === 1;
			topicData.bookmarkThreshold = parseInt(meta.config.bookmarkThreshold, 10) || 5;
			topicData.postEditDuration = parseInt(meta.config.postEditDuration, 10) || 0;
			topicData.postDeleteDuration = parseInt(meta.config.postDeleteDuration, 10) || 0;
			topicData.scrollToMyPost = settings.scrollToMyPost;
			topicData.rssFeedUrl = nconf.get('relative_path') + '/topic/' + topicData.tid + '.rss';
			if (req.loggedIn) {
				topicData.rssFeedUrl += '?uid=' + req.uid + '&token=' + rssToken;
			}

			addTags(topicData, req, res);

			topicData.postIndex = req.params.post_index;
			topicData.pagination = pagination.create(currentPage, pageCount, req.query);
			topicData.pagination.rel.forEach(function (rel) {
				rel.href = nconf.get('url') + '/topic/' + topicData.slug + rel.href;
				res.locals.linkTags.push(rel);
			});

			req.session.tids_viewed = req.session.tids_viewed || {};
			// if (!req.session.tids_viewed[tid] || req.session.tids_viewed[tid] < Date.now() - 3600000) {
			// if (!req.session.tids_viewed[tid] || req.session.tids_viewed[tid] < Date.now() - 3600000) {
			topics.increaseViewCount(tid);
			req.session.tids_viewed[tid] = Date.now();
			// }

			if (req.loggedIn) {
				topics.markAsRead([tid], req.uid, function (err, markedRead) {
					if (err) {
						return callback(err);
					}
					if (markedRead) {
						topics.pushUnreadCount(req.uid);
						topics.markTopicNotificationsRead([tid], req.uid);
					}
				});
			}
			if (topicData.timestamp) {
				topicData.releaseT = utils.getFormateTime(topicData.timestamp);
			}
			// topicData.mainPost = {};
			if (_.size(topicData.posts) > 0) {
				topicData.posts = _.filter(topicData.posts, function (t) {
					return t.pid !== topicData.mainPid;
				});
			}
			// 过滤回复数据

			_.forEach(topicData.posts, function (t) {
				t.replyT = utils.getFormateTime(new Date(t.timestampISO).getTime());
				t.iframeUrl = t.strategyId ? nconf.get('thumb') + t.strategyId + '?size=small' : '';
				t.isUser = req.uid === t.uid ? true : false;
				if (t.user && Array.isArray(t.user.group)) {
					_.forEach(t.user.group, (value) => {
						if (value.description === 'goldminer') {
							t.isOfficial = true;
							t.groupName = value.groupName;
						}
					});
				}
			});

			// 增加iframe链接
			if (topicData.strategyId) {
				if (topicData.hasOwnProperty('hasPerformance') && !topicData.hasPerformance) {
					topicData.iframeUrl = nconf.get('strategyCenter') + topicData.strategyId + '?ignoreperformance=true';
				} else {
					topicData.iframeUrl = nconf.get('strategyCenter') + topicData.strategyId;
				}
				topicData.iframeThumb = nconf.get('thumb') + topicData.strategyId + '?size=small';
			} else {
				topicData.iframeUrl = '';
				topicData.iframeThumb = '';
			}
			topicData.originUrl = nconf.get('url') + '/topic/' + topicData.tid;
			topicData.domainLogin = nconf.get('domain').url + nconf.get('domain').login;
			topicData.offical = nconf.get('domain').url;

			// 返回复数量
			// 用户删除回帖后，回帖数量无法查询
			// var replyNum = 0;
			// _.forEach(topicData.posts, function (t) {
			// 	if (!t.deleted || topicData.privileges.isAdminOrMod) {
			// 		replyNum += 1;
			// 	}
			// });
			// topicData.postcount = replyNum - 1 > 0 ? replyNum - 1 : 0;
			topicData.postCount = postCounts - 1 > 0 ? postCounts - 1 : 0;
			// 增加分享链接
			// wb
			topicData.shareWB = nconf.get('share').weibo + '?title=' + topicData.title + '&url=' + topicData.originUrl + '&content=utf-8&sourceUrl=' + topicData.originUrl + '&pic=' + nconf.get('share').logo;
			// xueqiu
			topicData.shareXQ = nconf.get('share').xueqiu + '?url=' + topicData.originUrl + '&title=掘金量化&content' + topicData.title;

			// 获取终端下载地址
			topicData.downLoadGoldminer = nconf.get('downLoadGoldminer');
			allCategoriesData.unshift({
				cid: 0,
				name: '所有分类',
				slug: '0/所有',
			});
			topicData.allCategoriesData = allCategoriesData;
			// 最近热门
			topicData.monthlyPopular = monthlyPopular;
			if (topicData.uid) {
				topicData.getUserData = {};
				getUserData(req.uid, [topicData.uid], function (err, resp) {
					if (!err) {
						var userData;
						if (_.size(resp) > 0) {
							userData = _.first(resp);
						} else {
							userData = {};
						}
						topicData.getUserData.picture = _.get(userData, 'picture', '');
						topicData.getUserData.uid = _.get(userData, 'uid', 0);
						topicData.getUserData.userslug = _.get(userData, 'userslug', '');
						topicData.getUserData.myquantid = _.get(userData, 'myquantid', 0);
						topicData.getUserData.email = validator.escape(String(_.get(userData, 'email') || ''));
						topicData.getUserData.username = validator.escape(String(_.get(userData, 'username') || ''));
						topicData.getUserData.fullname = validator.escape(String(_.get(userData, 'fullname') || ''));
						topicData.getUserData.topiccount = parseInt(_.get(userData, 'topiccount'), 10) || 0;
						var num = parseInt(_.get(userData, 'postcount', 0), 10) - parseInt(_.get(userData, 'topiccount', 0), 10);
						topicData.getUserData.replycount = num < 0 ? 0 : num;
						topicData.getUserData.bookmark = _.get(userData, 'bookmark', 0) || 0;
						topicData.getUserData.isOfficial = false;
						if (_.size(userData.group)) {
							_.forEach(userData.group, (value) => {
								if (value.description === 'goldminer') {
									topicData.getUserData.isOfficial = true;
									topicData.getUserData.groupName = value.groupName;
								}
							});
						}
					}
					topicData.isUser = req.uid === topicData.getUserData.uid ? true : false;
					topicData.selfPost = topicData.uid === req.uid ? true : false;
					topicData.classify = topicData.category.name;
					if (req.uid) topicData.hasLogin = true;
					else topicData.hasLogin = false;
					topicData.domainLogin = nconf.get('domain').url + nconf.get('domain').login;
					topicData.originUrl = nconf.get('url') + '/topic/' + topicData.tid;


					if (req.query && req.query.keyword) {
						// async.waterfall([
						// 	plugins.fireHook('filter:controllers.topic.get', { topicData: topicData, uid: req.uid }, next),
						// 	function (data, next) {
						// 		res.render('topic', topicData)
						// 	},
						// ], callback)
						async.waterfall([
							function (next) {
								plugins.fireHook('filter:search.word', {
									search_query: req.query.keyword
								}, next)
							},
							function (data, next) {
								topicData.searchQuery = data.join(',');
								plugins.fireHook('filter:search.relativeTopic', {
									tid: topicData.tid,
									title: topicData.title
								}, next)
							}
						], function (err, data) {
							if (!err && data.length > 0) {
								var d = _.slice(_.uniqBy(_.filter(data, k => k.tid), 'tid'), 0, 5);
								_.forEach(d, (v, k) => {
									v.cidname = _.get(_.find(allCategoriesData, {
										cid: Number(v.cid)
									}), 'name')
								})
								topicData.relativeTopic = d;
							}
							res.render('topic', topicData);
						})
					} else {
						plugins.fireHook('filter:search.relativeTopic', {
							tid: topicData.tid,
							title: topicData.title
						}, function (err, data) {
							if (!err && data.length > 0) {
								var d = _.slice(_.uniqBy(_.filter(data, k => k.tid), 'tid'), 0, 5);
								_.forEach(d, (v, k) => {
									v.cidname = _.get(_.find(allCategoriesData, {
										cid: Number(v.cid)
									}), 'name')
								})
								topicData.relativeTopic = d;
							}
							res.render('topic', topicData);
						})
					}
				});
			} else {
				callback();
			}
		},
	], callback);
};

function buildBreadcrumbs(topicData, callback) {
	var breadcrumbs = [{
			text: topicData.category.name,
			url: nconf.get('relative_path') + '/category/' + topicData.category.slug,
		},
		{
			text: topicData.title,
		},
	];

	async.waterfall([
		function (next) {
			helpers.buildCategoryBreadcrumbs(topicData.category.parentCid, next);
		},
		function (crumbs, next) {
			topicData.breadcrumbs = crumbs.concat(breadcrumbs);
			next(null, topicData);
		},
	], callback);
}

function addTags(topicData, req, res) {
	var postAtIndex = topicData.posts.find(function (postData) {
		return parseInt(postData.index, 10) === parseInt(Math.max(0, req.params.post_index - 1), 10);
	});

	var description = '';
	if (postAtIndex && postAtIndex.content) {
		description = utils.stripHTMLTags(utils.decodeHTMLEntities(postAtIndex.content));
	}

	if (description.length > 255) {
		description = description.substr(0, 255) + '...';
	}
	description = description.replace(/\n/g, ' ');

	res.locals.metaTags = [{
			name: 'title',
			content: topicData.titleRaw,
		},
		// {
		// 	name: 'description',
		// 	content: description,
		// },
		// {
		// 	property: 'og:title',
		// 	content: topicData.titleRaw,
		// },
		// {
		// 	property: 'og:description',
		// 	content: description,
		// },
		// {
		// 	property: 'og:type',
		// 	content: 'article',
		// },
		{
			property: 'article:published_time',
			content: utils.toISOString(topicData.timestamp),
		},
		{
			property: 'article:modified_time',
			content: utils.toISOString(topicData.lastposttime),
		},
		{
			property: 'article:section',
			content: topicData.category ? topicData.category.name : '',
		},
	];

	addOGImageTags(res, topicData, postAtIndex);

	res.locals.linkTags = [{
		rel: 'canonical',
		href: nconf.get('url') + '/topic/' + topicData.slug,
	}, ];

	if (!topicData['feeds:disableRSS']) {
		res.locals.linkTags.push({
			rel: 'alternate',
			type: 'application/rss+xml',
			href: topicData.rssFeedUrl,
		});
	}

	if (topicData.category) {
		res.locals.linkTags.push({
			rel: 'up',
			href: nconf.get('url') + '/category/' + topicData.category.slug,
		});
	}
}

function addOGImageTags(res, topicData, postAtIndex) {
	var ogImageUrl = '';
	if (topicData.thumb) {
		ogImageUrl = topicData.thumb;
	} else if (topicData.category.backgroundImage && (!postAtIndex || !postAtIndex.index)) {
		ogImageUrl = topicData.category.backgroundImage;
	} else if (postAtIndex && postAtIndex.user && postAtIndex.user.picture) {
		ogImageUrl = postAtIndex.user.picture;
	} else if (meta.config['og:image']) {
		ogImageUrl = meta.config['og:image'];
	} else if (meta.config['brand:logo']) {
		ogImageUrl = meta.config['brand:logo'];
	} else {
		ogImageUrl = '/logo.png';
	}

	addOGImageTag(res, ogImageUrl);
	addOGImageTagsForPosts(res, topicData.posts);
}

function addOGImageTagsForPosts(res, posts) {
	posts.forEach(function (postData) {
		var regex = /src\s*=\s*"(.+?)"/g;
		var match = regex.exec(postData.content);
		while (match !== null) {
			var image = match[1];

			if (image.startsWith(nconf.get('url') + '/plugins')) {
				return;
			}

			addOGImageTag(res, image);

			match = regex.exec(postData.content);
		}
	});
}

function addOGImageTag(res, imageUrl) {
	if (typeof imageUrl === 'string' && !imageUrl.startsWith('http')) {
		imageUrl = nconf.get('url') + imageUrl;
	}
	res.locals.metaTags.push({
		property: 'og:image',
		content: imageUrl,
		noEscape: true,
	});
	res.locals.metaTags.push({
		property: 'og:image:url',
		content: imageUrl,
		noEscape: true,
	});
}

topicsController.teaser = function (req, res, next) {
	var tid = req.params.topic_id;

	if (!utils.isNumber(tid)) {
		return next();
	}

	async.waterfall([
		function (next) {
			privileges.topics.can('read', tid, req.uid, next);
		},
		function (canRead, next) {
			if (!canRead) {
				return res.status(403).json('[[error:no-privileges]]');
			}
			topics.getLatestUndeletedPid(tid, next);
		},
		function (pid, next) {
			if (!pid) {
				return res.status(404).json('not-found');
			}
			posts.getPostSummaryByPids([pid], req.uid, {
				stripTags: false
			}, next);
		},
		function (posts) {
			if (!posts.length) {
				return res.status(404).json('not-found');
			}
			res.json(posts[0]);
		},
	], next);
};

topicsController.pagination = function (req, res, callback) {
	var tid = req.params.topic_id;
	var currentPage = parseInt(req.query.page, 10) || 1;

	if (!utils.isNumber(tid)) {
		return callback();
	}

	async.parallel({
		privileges: async.apply(privileges.topics.get, tid, req.uid),
		settings: async.apply(user.getSettings, req.uid),
		topic: async.apply(topics.getTopicData, tid),
	}, function (err, results) {
		if (err || !results.topic) {
			return callback(err);
		}

		if (!results.privileges.read || (parseInt(results.topic.deleted, 10) && !results.privileges.view_deleted)) {
			return helpers.notAllowed(req, res);
		}

		var postCount = parseInt(results.topic.postcount, 10);
		var pageCount = Math.max(1, Math.ceil(postCount / results.settings.postsPerPage));

		var paginationData = pagination.create(currentPage, pageCount);
		paginationData.rel.forEach(function (rel) {
			rel.href = nconf.get('url') + '/topic/' + results.topic.slug + rel.href;
		});

		res.json(paginationData);
	});
};

// add
function getUserData(uids, topicUid, callback) {
	async.parallel([
		async.apply(user.getUsersFields, topicUid, ['uid', 'username', 'fullname', 'userslug', 'reputation', 'postcount', 'picture', 'signature', 'banned', 'status', 'myquantid']),
			function (next) {
				group.getUserGroups(topicUid, next)
			}
	], function (err, results) {
		if (!err) {
			var userLists = results[0];
			if (_.size(userLists) > 0) {
				var newLists = _.map(userLists, (value, index) => {
					if (_.size(results[1][index]) > 0) {
						var groupArry = [];
						_.forEach(results[1][index], (value) => {
							groupArry.push({
								groupName: value.name,
								description: (value.description || '')
							});
						});
						value.group = groupArry;
					}
					return value;
				});
				callback(null, newLists);
			} else {
				callback(null, results[0]);
			}
		}
	});
}