
'use strict';

var async = require('async');
var validator = require('validator');
var nconf = require('nconf');

var meta = require('../meta');
var plugins = require('../plugins');
var search = require('../search');
var categories = require('../categories');
var pagination = require('../pagination');
var helpers = require('./helpers');
var _ = require('lodash');
var utils = require('../utils');
var htmlToText = require('html-to-text');
var group = require('../groups');
var user = require('../user');

var searchController = module.exports;
var userData;
searchController.search = function (req, res, next) {
	if (!plugins.hasListeners('filter:search.query')) {
		return next();
	}

	// if (!req.loggedIn && parseInt(meta.config.allowGuestSearching, 10) !== 1) {
	// 	return helpers.notAllowed(req, res);
	// }

	var page = Math.max(1, parseInt(req.query.page, 10)) || 1;
	if (req.query.categories && !Array.isArray(req.query.categories)) {
		req.query.categories = [req.query.categories];
	}

	var data = {
		query: utils.escapeHTML(req.query.term.trim()),
		searchIn: req.query.in || 'posts',
		matchWords: req.query.matchWords || 'all',
		postedBy: req.query.by,
		categories: req.query.categories,
		searchChildren: req.query.searchChildren,
		hasTags: req.query.hasTags,
		replies: req.query.replies,
		repliesFilter: req.query.repliesFilter,
		timeRange: req.query.timeRange,
		timeFilter: req.query.timeFilter,
		sortBy: req.query.sortBy || meta.config.searchDefaultSortBy || '',
		sortDirection: req.query.sortDirection,
		page: page,
		uid: req.uid,
		qs: req.query,
	};

	async.parallel({
		userData: function (next) {
			if (req.uid) {
				getUserData([req.uid], next);
			} else {
				next(null, []);
			}
		},
		categories: async.apply(categories.buildForSelect, req.uid, 'read'),
		search: async.apply(search.search, data),
	}, function (err, results) {
		if (err) {
			return next(err);
		}
		if (_.size(results.userData) > 0) {
			userData = _.first(results.userData);
		} else {
			userData = {};
		}
		results.categories = results.categories.filter(function (category) {
			return category && !category.link;
		});

		var categoriesData = [
			{ value: 'all', text: '[[unread:all_categories]]' },
			{ value: 'watched', text: '[[category:watched-categories]]' },
		].concat(results.categories);

		var searchData = results.search;
		searchData.categories = categoriesData;
		searchData.categoriesCount = Math.max(10, Math.min(20, categoriesData.length));
		searchData.pagination = pagination.create(page, searchData.pageCount, req.query);
		searchData.showAsPosts = !req.query.showAs || req.query.showAs === 'posts';
		searchData.showAsTopics = req.query.showAs === 'topics';
		searchData.title = '[[global:header.search]]';
		searchData.breadcrumbs = helpers.buildBreadcrumbs([{ text: '[[global:search]]' }]);
		searchData.expandSearch = !req.query.term;
		searchData.searchDefaultSortBy = meta.config.searchDefaultSortBy || '';
		searchData.search_query = validator.escape(String(req.query.term || ''));
		searchData.term = utils.escapeHTML(req.query.term);
		var allTopics = searchData.posts;
		_.forEach(allTopics, function (t) {
			t.releaseT = {
				year: utils.getFormateTime(t.timestamp, 'year'),
				date: utils.getFormateTime(t.timestamp, 'date'),
			};
			var content = t.content;
			t.bookmarked = t.bookmarks || 0;
			var text = htmlToText.fromString(content, {
				hideLinkHrefIfSameAsText: true,
				ignoreHref: true,
				ignoreImage: true,
				wordwrap: 20,
				singleNewLineParagraphs: true,
			});
			t.thumbnail = '<p>' + utils.cutByte(text, 250) + '</p>';
			t.unreplied = true;
			if (t.topic.teaser) {
				t.topic.teaser.timestamp = utils.getFormateTime(t.topic.lastposttime);
				t.topic.teaser.isUser = t.topic.teaser.user.uid === req.uid ? true : false;
				t.unreplied = false;
				if (Array.isArray(t.topic.teaser.user.group)) {
					_.forEach(t.topic.teaser.user.group, (value) => {
						if (value.description === 'goldminer') {
							t.topic.teaser.user.isOfficial = true;
							t.topic.teaser.user.groupName = value.groupName;
						}
					});
				}
			}
			t.postcount -= 1;
			if (t.topic.strategyId && (t.hasPerformance || !t.hasOwnProperty('hasPerformance'))) {
				t.hasIframe = true;
				t.iframeUrl = nconf.get('thumb') + t.topic.strategyId + '?size=small';
			} else {
				t.hasIframe = false;
			}
			t.isUser = t.uid === req.uid ? true : false;
			t.isOfficial = false;
			if (t.user && Array.isArray(t.user.group)) {
				_.forEach(t.user.group, (value) => {
					if (value.description === 'goldminer') {
						t.isOfficial = true;
						t.groupName = value.groupName;
					}
				});
			}
			t.topic.slug += '?keyword=' + searchData.search_query;
		});

		// 获取个人信息
		searchData.getUserData = {};
		if (userData) {
			searchData.getUserData.picture = _.get(userData, 'picture', '');
			searchData.getUserData.userslug = _.get(userData, 'userslug', '');
			searchData.getUserData.myquantid = _.get(userData, 'myquantid', 0);
			searchData.getUserData.aboutme = _.get(userData, 'aboutme', 0);
			searchData.getUserData.email = validator.escape(String(_.get(userData, 'email') || ''));
			searchData.getUserData.username = validator.escape(String(_.get(userData, 'username') || ''));
			searchData.getUserData.fullname = validator.escape(String(_.get(userData, 'fullname') || ''));
			searchData.getUserData.topiccount = parseInt(_.get(userData, 'topiccount'), 10) || 0;
			var num = parseInt(_.get(userData, 'postcount', 0), 10) - parseInt(_.get(userData, 'topiccount', 0), 10);
			searchData.getUserData.replycount = num < 0 ? 0 : num;
			searchData.getUserData.bookmark = _.get(userData, 'bookmark', 0);
			searchData.getUserData.isOfficial = false;
			if (_.size(userData.group)) {
				_.forEach(userData.group, (value) => {
					if (value.description === 'goldminer') {
						searchData.getUserData.isOfficial = true;
						searchData.getUserData.groupName = value.groupName;
					}
				});
			}
		}
		searchData.posts = allTopics;
		if (searchData.term) {
			plugins.fireHook('filter:search.word', { search_query: searchData.term }, function (err, data) {
				if (!err) {
					searchData.searchQuery = data.join(',');
					res.render('search', searchData);
				}
			});
		} else {
			res.render('search', searchData);
		}
	});
};

function getUserData(uids, callback) {
	async.parallel([
		async.apply(user.getUsersFields, uids, ['uid', 'username', 'fullname', 'userslug', 'reputation', 'postcount', 'picture', 'signature', 'banned', 'status', 'myquantid']),
		async.apply(group.getUserGroups, uids),
	], function (err, results) {
		if (!err) {
			var userLists = results[0];
			if (_.size(userLists) > 0) {
				var newLists = _.map(userLists, (value, index) => {
					if (_.size(results[1][index]) > 0) {
						var groupArry = [];
						_.forEach(results[1][index], (value) => {
							groupArry.push({ groupName: value.name, description: (value.description || '') });
						});
						value.group = groupArry;
					}
					return value;
				});
				callback(null, newLists);
			} else {
				callback(null, results[0]);
			}
		}
	});
}