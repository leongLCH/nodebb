'use strict';


var async = require('async');
var nconf = require('nconf');
var _ = require('lodash');
var db = require('../database');
var privileges = require('../privileges');
var user = require('../user');
var categories = require('../categories');
var meta = require('../meta');
var pagination = require('../pagination');
var helpers = require('./helpers');
var utils = require('../utils');
var translator = require('../translator');
var topics = require('../topics');
var validator = require('validator');
var htmlToText = require('html-to-text');
var group = require('../groups');
var categoryController = module.exports;

categoryController.get = function (req, res, callback) {
	var cid = req.params.category_id;
	var currentPage = parseInt(req.query.page, 10) || 1;
	var pageCount = 1;
	var userPrivileges;
	var settings;
	var rssToken;
	var userData;
	var popular;
	var allCategoriesData;
	var monthlyPopular;
	if ((req.params.topic_index && !utils.isNumber(req.params.topic_index)) || !utils.isNumber(cid)) {
		return callback();
	}

	var topicIndex = utils.isNumber(req.params.topic_index) ? parseInt(req.params.topic_index, 10) - 1 : 0;

	async.waterfall([
		function (next) {
			async.parallel({
				categoryData: function (next) {
					// categories.getCategoryFields(cid, ['slug', 'disabled', 'topic_count'], next);
					// 增加所有标签
					if (cid > 0) {
						categories.getCategoryFields(cid, ['slug', 'disabled', 'topic_count'], next);
					} else {
						categories.getCategoryAllFields(cid, ['slug', 'disabled', 'topic_count'], next)
					}
				},
				privileges: function (next) {
					privileges.categories.get(cid, req.uid, next);
				},
				userSettings: function (next) {
					user.getSettings(req.uid, next);
				},
				rssToken: function (next) {
					user.auth.getFeedToken(req.uid, next);
				},
				// 列表页获取个人信息
				userData: function (next) {
					if (req.uid) {
						getUserData([req.uid], next)
					} else {
						next(null, []);
					}
				},
				// userData: function (next) {
				// 	if (req.uid) {
				// 		user.getUserData(req.uid, next);
				// 	} else {
				// 		next(null, {});
				// 	}
				// },
				// 获取热门
				popular: function (next) {
					topics.getPopular('alltime', req.uid, 4, next);
				},
				// 获取所有版块
				allCategoriesData: function (next) {
					categories.getCategoriesByPrivilege('cid:0:children', req.uid, 'find', next);
				},
				monthlyPopular: function (next) {
					topics.getPopularTopics('month', req.uid, 0, 6, next);
				}
			}, next);
		},
		function (results, next) {
			userPrivileges = results.privileges;
			rssToken = results.rssToken;
			// userData = results.userData;
			if (_.size(results.userData) > 0) {
				userData = _.first(results.userData);
			} else {
				userData = {};
			}
			popular = results.popular;
			allCategoriesData = results.allCategoriesData;
			monthlyPopular = results.monthlyPopular.topics;
			if (!results.categoryData.slug || (results.categoryData && parseInt(results.categoryData.disabled, 10) === 1)) {
				return callback();
			}

			if (!results.privileges.read) {
				return helpers.notAllowed(req, res);
			}

			if (!res.locals.isAPI && (!req.params.slug || results.categoryData.slug !== cid + '/' + req.params.slug) && (results.categoryData.slug && results.categoryData.slug !== cid + '/')) {
				return helpers.redirect(res, '/category/' + results.categoryData.slug);
			}

			settings = results.userSettings;

			var topicCount = parseInt(results.categoryData.topic_count, 10);
			pageCount = Math.max(1, Math.ceil(topicCount / settings.topicsPerPage));

			if (topicIndex < 0 || topicIndex > Math.max(topicCount - 1, 0)) {
				return helpers.redirect(res, '/category/' + cid + '/' + req.params.slug + (topicIndex > topicCount ? '/' + topicCount : ''));
			}

			if (settings.usePagination && (currentPage < 1 || currentPage > pageCount)) {
				return callback();
			}

			if (!settings.usePagination) {
				topicIndex = Math.max(0, topicIndex - (Math.ceil(settings.topicsPerPage / 2) - 1));
			} else if (!req.query.page) {
				var index = Math.max(parseInt((topicIndex || 0), 10), 0);
				currentPage = Math.ceil((index + 1) / settings.topicsPerPage);
				topicIndex = 0;
			}
			user.getUidByUserslug(req.query.author, next);
		},
		function (targetUid, next) {
			var start = ((currentPage - 1) * settings.topicsPerPage) + topicIndex;
			var stop = start + settings.topicsPerPage - 1;
			categories.getCategoryById({
				uid: req.uid,
				cid: cid,
				start: start,
				stop: stop,
				sort: req.query.sort || settings.categoryTopicSort,
				settings: settings,
				query: req.query,
				tag: req.query.tag,
				targetUid: targetUid,
			}, next);
		},
		function (categoryData, next) {
			categories.modifyTopicsByPrivilege(categoryData.topics, userPrivileges);

			if (categoryData.link) {
				db.incrObjectField('category:' + categoryData.cid, 'timesClicked');
				return helpers.redirect(res, categoryData.link);
			}

			buildBreadcrumbs(req, categoryData, next);
		},
		function (categoryData, next) {
			if (!categoryData.children.length) {
				return next(null, categoryData);
			}

			var allCategories = [];
			categories.flattenCategories(allCategories, categoryData.children);
			categories.getRecentTopicReplies(allCategories, req.uid, function (err) {
				next(err, categoryData);
			});
		},
		function (categoryData) {
			categoryData.description = translator.escape(categoryData.description);
			categoryData.privileges = userPrivileges;
			categoryData.showSelect = categoryData.privileges.editable;
			categoryData.rssFeedUrl = nconf.get('url') + '/category/' + categoryData.cid + '.rss';
			if (parseInt(req.uid, 10)) {
				categories.markAsRead([cid], req.uid);
				categoryData.rssFeedUrl += '?uid=' + req.uid + '&token=' + rssToken;
			}

			addTags(categoryData, res);

			categoryData['feeds:disableRSS'] = parseInt(meta.config['feeds:disableRSS'], 10) === 1;
			categoryData['reputation:disabled'] = parseInt(meta.config['reputation:disabled'], 10) === 1;
			categoryData.title = translator.escape(categoryData.name);
			pageCount = Math.max(1, Math.ceil(categoryData.topic_count / settings.topicsPerPage));
			categoryData.pagination = pagination.create(currentPage, pageCount, req.query);
			categoryData.pagination.rel.forEach(function (rel) {
				rel.href = nconf.get('url') + '/category/' + categoryData.slug + rel.href;
				res.locals.linkTags.push(rel);
			});
			var allTopics = categoryData.topics;
			var filterT = _.filter(allTopics, function (t) {
				return !t.deleted || categoryData.privileges.isAdminOrMod;
			});
			_.forEach(filterT, function (t) {
				t.releaseT = {
					year: utils.getFormateTime(t.timestamp, 'year'),
					date: utils.getFormateTime(t.timestamp, 'date'),
				};
				var content = t.mainPost && t.mainPost.content;
				t.bookmarked = (t.mainPost && t.mainPost.bookmarks) || 0;
				var text = htmlToText.fromString(content, {
					hideLinkHrefIfSameAsText: true,
					ignoreHref: true,
					ignoreImage: true,
					wordwrap: 20,
					singleNewLineParagraphs: true,
				});
				t.thumbnail = '<p>' + utils.cutByte(text, 250) + '</p>';
				if (t.teaser) {
					t.teaser.timestamp = utils.getFormateTime(t.lastposttime);
					t.teaser.isUser = t.teaser.user.uid === req.uid ? true : false;
					if (Array.isArray(t.teaser.user.group)) {
						_.forEach(t.teaser.user.group, (value) => {
							if (value.description === 'goldminer') {
								t.teaser.user.isOfficial = true;
								t.teaser.user.groupName = value.groupName;
							}
						});
					}
				}
				t.postcount -= 1;
				if (t.strategyId && (t.hasPerformance || !t.hasOwnProperty('hasPerformance'))) {
					t.hasIframe = true;
					t.iframeUrl = nconf.get('thumb') + t.strategyId + '?size=small';
				} else {
					t.hasIframe = false;
				}
				t.isUser = t.uid === req.uid ? true : false;
				t.isOfficial = false;
				if (t.user && Array.isArray(t.user.group)) {
					_.forEach(t.user.group, (value) => {
						if (value.description === 'goldminer') {
							t.isOfficial = true;
							t.groupName = value.groupName;
						}
					});
				}
			});
			categoryData.topics = filterT;
			categoryData.domainLogin = nconf.get('domain').url + nconf.get('domain').login;
			categoryData.domainReg = nconf.get('domain').url + nconf.get('domain').reg;

			categoryData.offical = nconf.get('domain').url;
			
			// 获取个人信息
			categoryData.getUserData = {};
			if (userData) {
				categoryData.getUserData.picture = _.get(userData, 'picture', '');
				categoryData.getUserData.userslug = _.get(userData, 'userslug', '');
				categoryData.getUserData.myquantid = _.get(userData, 'myquantid', 0);
				categoryData.getUserData.aboutme = _.get(userData, 'aboutme', 0);
				categoryData.getUserData.email = validator.escape(String(_.get(userData, 'email') || ''));
				categoryData.getUserData.username = validator.escape(String(_.get(userData, 'username') || ''));
				categoryData.getUserData.fullname = validator.escape(String(_.get(userData, 'fullname') || ''));
				categoryData.getUserData.topiccount = parseInt(_.get(userData, 'topiccount'), 10) || 0;
				var num = parseInt(_.get(userData, 'postcount', 0), 10) - parseInt(_.get(userData, 'topiccount', 0), 10);
				categoryData.getUserData.replycount = num < 0 ? 0 : num;
				categoryData.getUserData.bookmark = _.get(userData, 'bookmark', 0);
				categoryData.getUserData.isOfficial = false;
				if (_.size(userData.group)) {
					_.forEach(userData.group, (value) => {
						if (value.description === 'goldminer') {
							categoryData.getUserData.isOfficial = true;
							categoryData.getUserData.groupName = value.groupName;
						}
					});
				}
			}

			// 获取热门话题
			categoryData.getPopular = [];
			if (_.size(popular) > 0) {
				_.forEach(popular, function (p) {
					categoryData.getPopular.push(_.pick(p, ['title', 'slug']));
				});
			}

			// 过滤版块数据
			categoryData.allCategoriesData = [];
			if (_.size(allCategoriesData) > 0) {
				categoryData.allCategoriesData = _.map(allCategoriesData, function (g) {
					const obj = _.pick(g, ['cid', 'name']);
					if (String(g.cid) === cid) {
						obj.css = 'css-ch';
					}
					return obj;
				});
			}
			categoryData.allCategoriesData.unshift({
				cid: 0,
				name: '所有分类',
				slug: '0/所有',
			});
			categoryData.originUrl = nconf.get('url') + '/category/' + categoryData.slug;
			categoryData.monthlyPopular = monthlyPopular;
			res.render('category', categoryData);
		},
	], callback);
};

function getUserData(uids, callback) {
	async.parallel([
		async.apply(user.getUsersFields, uids, ['uid', 'username', 'fullname', 'userslug', 'reputation', 'postcount', 'picture', 'signature', 'banned', 'status', 'myquantid']),
		async.apply(group.getUserGroups, uids),
	], function (err, results) {
		if (!err) {
			var userLists = results[0];
			if (_.size(userLists) > 0) {
				var newLists = _.map(userLists, (value, index) => {
					if (_.size(results[1][index]) > 0) {
						var groupArry = [];
						_.forEach(results[1][index], (value) => {
							groupArry.push({ groupName: value.name, description: (value.description || '') });
						});
						value.group = groupArry;
					}
					return value;
				});
				callback(null, newLists);
			} else {
				callback(null, results[0]);
			}
		}
	});
}
function buildBreadcrumbs(req, categoryData, callback) {
	var breadcrumbs = [
		{
			text: categoryData.name,
			url: nconf.get('relative_path') + '/category/' + categoryData.slug,
		},
	];
	async.waterfall([
		function (next) {
			helpers.buildCategoryBreadcrumbs(categoryData.parentCid, next);
		},
		function (crumbs, next) {
			if (req.originalUrl.startsWith(nconf.get('relative_path') + '/api/category') || req.originalUrl.startsWith(nconf.get('relative_path') + '/category')) {
				categoryData.breadcrumbs = crumbs.concat(breadcrumbs);
			}
			next(null, categoryData);
		},
	], callback);
}

function addTags(categoryData, res) {
	res.locals.metaTags = [
		{
			name: 'title',
			content: categoryData.name,
		},
		// {
		// 	property: 'og:title',
		// 	content: categoryData.name,
		// },
		// {
		// 	name: 'description',
		// 	content: categoryData.description,
		// },
		// {
		// 	property: 'og:type',
		// 	content: 'website',
		// },
	];

	if (categoryData.backgroundImage) {
		if (!categoryData.backgroundImage.startsWith('http')) {
			categoryData.backgroundImage = nconf.get('url') + categoryData.backgroundImage;
		}
		res.locals.metaTags.push({
			property: 'og:image',
			content: categoryData.backgroundImage,
		});
	}

	res.locals.linkTags = [
		{
			rel: 'up',
			href: nconf.get('url'),
		},
	];

	if (!categoryData['feeds:disableRSS']) {
		res.locals.linkTags.push({
			rel: 'alternate',
			type: 'application/rss+xml',
			href: categoryData.rssFeedUrl,
		});
	}
}
