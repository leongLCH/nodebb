
'use strict';

var async = require('async');
var nconf = require('nconf');
var querystring = require('querystring');
var _ = require('lodash');

var user = require('../user');
var topics = require('../topics');
var meta = require('../meta');
var helpers = require('./helpers');
var pagination = require('../pagination');
var validator = require('validator');
var privileges = require('../privileges');
var utils = require('../utils');
var htmlToText = require('html-to-text');
var db = require('../database');
var group = require('../groups');


var recentController = module.exports;

recentController.get = function (req, res, next) {
	var page = parseInt(req.query.page, 10) || 1;
	var stop = 0;
	var settings;
	var cid = req.query.cid;
	var filter = req.params.filter || '';
	var categoryData;
	var userPrivileges;
	var rssToken;
	var popular;
	var userData;
	var statistics;
	var monthlyPopular;

	if (!helpers.validFilters[filter]) {
		return next();
	}
	async.waterfall([
		function (next) {
			async.parallel({
				settings: function (next) {
					user.getSettings(req.uid, next);
				},
				privileges: function (next) {
					var mcid = cid ? cid[0] : 1;
					privileges.categories.get(mcid, req.uid, next);
				},
				watchedCategories: function (next) {
					helpers.getWatchedCategories(req.uid, cid, next);
				},
				rssToken: function (next) {
					user.auth.getFeedToken(req.uid, next);
				},
				// 列表页获取个人信息
				userData: function (next) {
					if (req.uid) {
						getUserData([req.uid], next)
					} else {
						next(null, []);
					}
				},
				// 获取热门
				popular: function (next) {
					topics.getPopular('alltime', req.uid, 4, next);
				},
				statistics: function (next) {
					getStatsForSet('topics:tid', 'topicCount', next);
				},
				monthlyPopular: function (next) {
					topics.getPopularTopics('month', req.uid, 0, 6, next);
				}
			}, next);
		},
		function (results, next) {
			userPrivileges = results.privileges;
			rssToken = results.rssToken;
			settings = results.settings;
			// userData = results.userData;
			if (_.size(results.userData) > 0) {
				userData = _.first(results.userData);
			} else {
				userData = {};
			}
			popular = results.popular;
			categoryData = results.watchedCategories;
			statistics = results.statistics;
			monthlyPopular = results.monthlyPopular.topics;

			var start = Math.max(0, (page - 1) * settings.topicsPerPage);
			stop = start + settings.topicsPerPage - 1;

			topics.getRecentTopics(cid, req.uid, start, stop, filter, next);
		},
		function (data) {
			data.categories = categoryData.categories;
			data.categories.unshift({
				cid: 0,
				name: '所有分类',
			});
			data.selectedCategory = categoryData.selectedCategory;
			data.selectedCids = categoryData.selectedCids;
			if (!data.selectedCategory) {
				data.selectedCategory = {
					cid: 0,
					name: '所有分类',
					slug: '1/策略研究',
				};
			}
			data.nextStart = stop + 1;
			data.set = 'topics:recent';
			data['feeds:disableRSS'] = parseInt(meta.config['feeds:disableRSS'], 10) === 1;
			data.rssFeedUrl = nconf.get('relative_path') + '/recent.rss';
			if (req.loggedIn) {
				data.rssFeedUrl += '?uid=' + req.uid + '&token=' + rssToken;
			}
			data.title = meta.config.homePageTitle || '[[pages:home]]';
			data.filters = helpers.buildFilters('recent', filter);

			data.selectedFilter = data.filters.find(function (filter) {
				return filter && filter.selected;
			});

			var pageCount = Math.max(1, Math.ceil(data.topicCount / settings.topicsPerPage));
			data.pagination = pagination.create(page, pageCount, req.query);

			if (req.originalUrl.startsWith(nconf.get('relative_path') + '/api/recent') || req.originalUrl.startsWith(nconf.get('relative_path') + '/recent')) {
				data.title = '[[pages:recent]]';
				data.breadcrumbs = helpers.buildBreadcrumbs([{ text: '[[recent:title]]' }]);
			}

			data.querystring = cid ? '?' + querystring.stringify({ cid: cid }) : '';

			data.privileges = userPrivileges;

			// 话题列表过滤
			var allTopics = data.topics;
			var filterT = _.filter(allTopics, function (t) {
				return !t.deleted || data.privileges.isAdminOrMod;
			});
			_.forEach(filterT, function (t) {
				t.releaseT = {
					year: utils.getFormateTime(t.timestamp, 'year'),
					date: utils.getFormateTime(t.timestamp, 'date'),
				};
				var content = t.mainPost.content;
				t.bookmarked = t.mainPost.bookmarks || 0;
				var text = htmlToText.fromString(content, {
					hideLinkHrefIfSameAsText: true,
					ignoreHref: true,
					ignoreImage: true,
					wordwrap: 20,
					singleNewLineParagraphs: true,
				});
				t.thumbnail = '<p>' + utils.cutByte(text, 250) + '</p>';
				if (t.teaser) {
					t.teaser.timestamp = utils.getFormateTime(t.lastposttime);
					t.teaser.isUser = t.teaser.user.uid === req.uid ? true : false;
					if (Array.isArray(t.teaser.user.group)) {
						_.forEach(t.teaser.user.group, (value) => {
							if (value.description === 'goldminer') {
								t.teaser.user.isOfficial = true;
								t.teaser.user.groupName = value.groupName;
							}
						});
					}
				}
				t.postcount -= 1;
				// console.log(t.hasProper hasPerformance)
				// t.ignoreP = !t.hasPerformance;
				if (t.strategyId && (t.hasPerformance || !t.hasOwnProperty('hasPerformance'))) {
					t.hasIframe = true;
					t.iframeUrl = nconf.get('thumb') + t.strategyId + '?size=small';
				} else {
					t.hasIframe = false;
				}
				t.isUser = t.uid === req.uid ? true : false;
				t.isOfficial = false;
				if (t.user && Array.isArray(t.user.group)) {
					_.forEach(t.user.group, (value) => {
						if (value.description === 'goldminer') {
							t.isOfficial = true;
							t.groupName = value.groupName;
						}
					});
				}
			});
			data.topics = filterT;
			data.domainLogin = nconf.get('domain').url + nconf.get('domain').login;
			data.domainReg = nconf.get('domain').url + nconf.get('domain').reg;

			data.offical = nconf.get('domain').url;
			
			// 个人信息
			data.getUserData = {};
			if (userData) {
				data.getUserData.picture = _.get(userData, 'picture', '');
				data.getUserData.userslug = _.get(userData, 'userslug', '');
				data.getUserData.myquantid = _.get(userData, 'myquantid', 0);
				data.getUserData.aboutme = _.get(userData, 'aboutme', '');
				data.getUserData.email = validator.escape(String(_.get(userData, 'email') || ''));
				data.getUserData.username = validator.escape(String(_.get(userData, 'username') || ''));
				data.getUserData.fullname = validator.escape(String(_.get(userData, 'fullname') || ''));
				data.getUserData.topiccount = parseInt(_.get(userData, 'topiccount'), 10) || 0;
				var num = parseInt(_.get(userData, 'postcount', 0), 10) - parseInt(_.get(userData, 'topiccount', 0), 10);
				data.getUserData.replycount = num < 0 ? 0 : num;
				data.getUserData.bookmark = _.get(userData, 'bookmark', 0);
				data.getUserData.isOfficial = false;
				if (_.size(userData.group)) {
					_.forEach(userData.group, (value) => {
						if (value.description === 'goldminer') {
							data.getUserData.isOfficial = true;
							data.getUserData.groupName = value.groupName;
						}
					});
				}
			}

			// 获取热门话题
			data.getPopular = [];
			if (_.size(popular) > 0) {
				_.forEach(popular, function (p) {
					data.getPopular.push(_.pick(p, ['title', 'slug']));
				});
			}
			data.originUrl = nconf.get('url') + '/recent';
			data.cid = !cid ? 1 : cid[0];
			data.statistics = statistics;
			data.monthlyPopular = monthlyPopular;

			res.render('recent', data);
		},
	], next);
};
function getGlobalField(field, callback) {
	db.getObjectField('global', field, function (err, count) {
		callback(err, parseInt(count, 10) || 0);
	});
}

function getStatsForSet(set, field, callback) {
	var terms = {
		day: 86400000,
		week: 604800000,
		month: 2592000000,
	};

	var now = Date.now();
	async.parallel({
		day: function (next) {
			db.sortedSetCount(set, now - terms.day, '+inf', next);
		},
		week: function (next) {
			db.sortedSetCount(set, now - terms.week, '+inf', next);
		},
		month: function (next) {
			db.sortedSetCount(set, now - terms.month, '+inf', next);
		},
		alltime: function (next) {
			getGlobalField(field, next);
		},
	}, callback);
}

function getUserData(uids, callback) {
	async.parallel([
		async.apply(user.getUsersFields, uids, ['uid', 'username', 'fullname', 'userslug', 'reputation', 'postcount', 'picture', 'signature', 'banned', 'status', 'myquantid']),
		async.apply(group.getUserGroups, uids),
	], function (err, results) {
		if (!err) {
			var userLists = results[0];
			if (_.size(userLists) > 0) {
				var newLists = _.map(userLists, (value, index) => {
					if (_.size(results[1][index]) > 0) {
						var groupArry = [];
						_.forEach(results[1][index], (value) => {
							groupArry.push({ groupName: value.name, description: (value.description || '') });
						});
						value.group = groupArry;
					}
					return value;
				});
				callback(null, newLists);
			} else {
				callback(null, results[0]);
			}
		}
	});
}
