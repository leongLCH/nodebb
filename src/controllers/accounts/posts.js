'use strict';


var async = require('async');

var db = require('../../database');
var user = require('../../user');
var posts = require('../../posts');
var topics = require('../../topics');
var pagination = require('../../pagination');
var helpers = require('../helpers');
var accountHelpers = require('./helpers');
var nconf = require('nconf');
var _ = require('lodash');
var postsController = module.exports;
var utils = require('../../utils');
var htmlToText = require('html-to-text');
var group = require('../../groups');
var templateToData = {
	'account/bookmarks': {
		set: 'bookmarks',
		type: 'posts',
		noItemsFoundKey: '[[topic:bookmarks.has_no_bookmarks]]',
		crumb: '[[user:bookmarks]]',
	},
	'account/posts': {
		set: 'posts',
		type: 'posts',
		noItemsFoundKey: '[[user:has_no_posts]]',
		crumb: '[[global:posts]]',
	},
	'account/upvoted': {
		set: 'upvote',
		type: 'posts',
		noItemsFoundKey: '[[user:has_no_upvoted_posts]]',
		crumb: '[[global:upvoted]]',
	},
	'account/downvoted': {
		set: 'downvote',
		type: 'posts',
		noItemsFoundKey: '[[user:has_no_downvoted_posts]]',
		crumb: '[[global:downvoted]]',
	},
	'account/best': {
		set: 'posts:votes',
		type: 'posts',
		noItemsFoundKey: '[[user:has_no_voted_posts]]',
		crumb: '[[global:best]]',
	},
	'account/watched': {
		set: 'followed_tids',
		type: 'topics',
		noItemsFoundKey: '[[user:has_no_watched_topics]]',
		crumb: '[[user:watched]]',
	},
	'account/ignored': {
		set: 'ignored_tids',
		type: 'topics',
		noItemsFoundKey: '[[user:has_no_ignored_topics]]',
		crumb: '[[user:ignored]]',
	},
	'account/topics': {
		set: 'topics',
		type: 'topics',
		noItemsFoundKey: '[[user:has_no_topics]]',
		crumb: '[[global:topics]]',
	},
};

postsController.getBookmarks = function (req, res, next) {
	getFromUserSet('account/bookmarks', req, res, next);
};

postsController.getPosts = function (req, res, next) {
	getFromUserSet('account/posts', req, res, next);
};

postsController.getUpVotedPosts = function (req, res, next) {
	getFromUserSet('account/upvoted', req, res, next);
};

postsController.getDownVotedPosts = function (req, res, next) {
	getFromUserSet('account/downvoted', req, res, next);
};

postsController.getBestPosts = function (req, res, next) {
	getFromUserSet('account/best', req, res, next);
};

postsController.getWatchedTopics = function (req, res, next) {
	getFromUserSet('account/watched', req, res, next);
};

postsController.getIgnoredTopics = function (req, res, next) {
	getFromUserSet('account/ignored', req, res, next);
};

postsController.getTopics = function (req, res, next) {
	getFromUserSet('account/topics', req, res, next);
};

// add
function getUserData(uids, callback) {
	async.parallel([
		async.apply(user.getUsersFields, uids, ['uid', 'username', 'fullname', 'userslug', 'reputation', 'postcount', 'picture', 'signature', 'banned', 'status', 'myquantid']),
		function (next) {
			group.getUserGroups(uids, next)
		}
	], function (err, results) {
		if (!err) {
			var userLists = results[0];
			if (_.size(userLists) > 0) {
				var newLists = _.map(userLists, (value, index) => {
					if (_.size(results[1][index]) > 0) {
						var groupArry = [];
						_.forEach(results[1][index], (value) => {
							groupArry.push({ groupName: value.name, description: (value.description || '') });
						});
						value.group = groupArry;
					}
					return value;
				});
				callback(null, newLists);
			} else {
				callback(null, results[0]);
			}
		}
	});
}

function getUidByMyquantId(id, cb) {
	db.sortedSetScore('myquantid:uid', id, cb);
}

function getFromUserSet(template, req, res, callback) {
	var data = templateToData[template];
	data.template = template;
	data.method = data.type === 'posts' ? posts.getPostSummariesFromSet : topics.getTopicsFromSet;
	var userData;
	var itemsPerPage;
	var page = Math.max(1, parseInt(req.query.page, 10) || 1);
	async.waterfall([
		function (next) {
			async.parallel({
				settings: function (next) {
					user.getSettings(req.uid, next);
				},
				userData: function (next) {
					async.waterfall([
						async.apply(getUidByMyquantId, req.params.myquantId),
						function (uid, next) {
							if (uid) {
								getUserData([uid], next);
							} else {
								callback(null, {});
							}
						},
					], next);
					// accountHelpers.getUserDataByMyquantId(req.params.myquantId, req.uid, next);
				},
			}, next);
		},
		function (results, next) {
			if (!results.userData) {
				return callback();
			}
			if (_.size(results.userData) > 0) {
				userData = _.first(results.userData);
			} else {
				userData = {};
			}
			// userData = results.userData;

			var setName = 'uid:' + userData.uid + ':' + data.set;
			itemsPerPage = (data.template === 'account/topics' || data.template === 'account/watched') ? results.settings.topicsPerPage : results.settings.postsPerPage;
			async.parallel({
				itemCount: function (next) {
					// if (results.settings.usePagination) {
					// 	db.sortedSetCard(setName, next);
					// } else {
					next(null, 0);
					// }
				},
				data: function (next) {
					var start = (page - 1) * itemsPerPage;
					var stop = start + itemsPerPage - 1;
					data.method(setName, req.uid, start, stop, next);
				},
			}, next);
		},
		function (results) {
			userData[data.type] = results.data[data.type];
			userData.nextStart = results.data.nextStart;

			var pageCount = Math.ceil(results.itemCount / itemsPerPage);
			userData.pagination = pagination.create(page, pageCount);

			userData.noItemsFoundKey = data.noItemsFoundKey;
			userData.title = '[[pages:' + data.template + ', ' + userData.username + ']]';
			userData.breadcrumbs = helpers.buildBreadcrumbs([{ text: userData.username, url: '/user/' + userData.userslug }, { text: data.crumb }]);
			if (results.data && _.size(results.data.topics)) {
				_.forEach(results.data.topics, function (t) {
					if (t.strategyId) {
						t.iframeUrl = nconf.get('thumb') + t.strategyId + '?size=small';
					}
					t.releaseT = {
						year: utils.getFormateTime(t.timestamp, 'year'),
						date: utils.getFormateTime(t.timestamp, 'date'),
					};
					var content = t.mainPost && t.mainPost.content;
					t.bookmarked = (t.mainPost && t.mainPost.bookmarks) || 0;
					var text = htmlToText.fromString(content, {
						hideLinkHrefIfSameAsText: true,
						ignoreHref: true,
						ignoreImage: true,
						wordwrap: 20,
						singleNewLineParagraphs: true,
					});
					t.thumbnail = '<p>' + utils.cutByte(text, 250) + '</p>';
					if (t.teaser) {
						t.teaser.timestamp = utils.getFormateTime(t.teaser.timestamp);
						if (Array.isArray(t.teaser.user.group)) {
							_.forEach(t.teaser.user.group, (value) => {
								if (value.description === 'goldminer') {
									t.teaser.user.isOfficial = true;
									t.teaser.user.groupName = value.groupName;
								}
							});
						}
					}
					t.postcount -= 1;
					if (t.user && Array.isArray(t.user.group)) {
						_.forEach(t.user.group, (value) => {
							if (value.description === 'goldminer') {
								t.isOfficial = true;
								t.groupName = value.groupName;
							}
						});
					}
				});
			}
			if (results.data && _.size(results.data.posts)) {
				var newA = _.filter(results.data.posts, function (t) {
					var text = htmlToText.fromString(t.content, {
						hideLinkHrefIfSameAsText: true,
						ignoreHref: true,
						ignoreImage: true,
						wordwrap: 20,
						singleNewLineParagraphs: true,
					});
					t.content = '<p>' + utils.cutByte(text, 250) + '</p>';
					if (template === 'account/bookmarks') {
						return t
					} else {
						return t.pid !== t.topic.mainPid;
					}
				});
				userData[data.type] = newA;
			}
			if (req.query.hide) {
				userData.toIframe = true;
			}
			// 组
			userData.isOfficial = false;
			if (Array.isArray(userData.group)) {
				_.forEach(userData.group, (value) => {
					if (value.description === 'goldminer') {
						userData.isOfficial = true;
						userData.groupName = value.groupName;
					}
				});
			}

			userData.nav = [
				{
					name: '主题',
					slug: !userData.toIframe ? '' : '?hide=true',
					css: template === 'account/topics' ? 'active' : '',
				},
				{
					name: '回复',
					slug: !userData.toIframe ? '/posts' : '/posts?hide=true',
					css: template === 'account/posts' ? 'active' : '',
				},
				{
					name: '关注',
					slug: !userData.toIframe ? '/follow' : '/follow?hide=true',
					css: '',
				},
				{
					name: '收藏',
					slug: !userData.toIframe ? '/bookmarks' : '/bookmarks?hide=true',
					css: template === 'account/bookmarks' ? 'active' : '',
				},
			];
			userData.isSelf = req.uid === userData.uid ? true : false;
			res.render(data.template, userData);
		},
	], callback);
}

module.exports = postsController;
