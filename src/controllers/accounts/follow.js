'use strict';

var async = require('async');

var user = require('../../user');
var helpers = require('../helpers');
var accountHelpers = require('./helpers');
var pagination = require('../../pagination');
var _ = require('lodash');

var followController = {};

followController.getFollowing = function (req, res, next) {
	getFollow('account/following', 'following', req, res, next);
};

followController.getFollowers = function (req, res, next) {
	getFollow('account/followers', 'followers', req, res, next);
};

followController.getFollow = function (req, res, next) {
	getAllFollow('account/follow', req, res, next);
};

function getFollow(tpl, name, req, res, callback) {
	var userData;

	var page = parseInt(req.query.page, 10) || 1;
	var resultsPerPage = 50;
	var start = Math.max(0, page - 1) * resultsPerPage;
	var stop = start + resultsPerPage - 1;

	async.waterfall([
		function (next) {
			accountHelpers.getUserDataByUserSlug(req.params.userslug, req.uid, next);
		},
		function (data, next) {
			userData = data;
			if (!userData) {
				return callback();
			}
			var method = name === 'following' ? 'getFollowing' : 'getFollowers';
			user[method](userData.uid, start, stop, next);
		},
	], function (err, users) {
		if (err) {
			return callback(err);
		}

		userData.users = users;
		userData.title = '[[pages:' + tpl + ', ' + userData.username + ']]';
		var count = name === 'following' ? userData.followingCount : userData.followerCount;
		var pageCount = Math.ceil(count / resultsPerPage);
		userData.pagination = pagination.create(page, pageCount);
		userData.breadcrumbs = helpers.buildBreadcrumbs([{ text: userData.username, url: '/user/' + userData.userslug }, { text: '[[user:' + name + ']]' }]);
		res.render(tpl, userData);
	});
}

function getAllFollow(tpl, req, res, callback) {
	var userData;

	var page = parseInt(req.query.page, 10) || 1;
	var resultsPerPage = 50;
	var start = Math.max(0, page - 1) * resultsPerPage;
	var stop = start + resultsPerPage - 1;
	async.parallel({
		getMyFollow: function (next) {
			var method = 'getFollowing';
			user[method](req.uid, start, stop, next);
		},
		getUserFollow: function (next) {
			async.waterfall([
				function (next) {
					// accountHelpers.getUserDataByUserSlug(req.params.userslug, req.uid, next);
					accountHelpers.getUserDataByMyquantId(req.params.myquantId, req.uid, next);
				},
				function (data, next) {
					userData = data;
					if (!userData) {
						return callback();
					}
					var method = 'getFollowing';
					user[method](userData.uid, start, stop, next);
				},
				function (data, next) {
					userData.following = data;
					var method = 'getFollowers';
					user[method](userData.uid, start, stop, next);
				},
			], next);
		},
	}, function (err, results) {
		if (err) {
			return callback(err);
		}
		userData.followers = results.getUserFollow;
		var getmyF = results.getMyFollow;
		var idlists = [];
		_.forEach(getmyF, function (value) {
			idlists.push(value.uid);
		});
		_.forEach(userData.following, function (value) {
			if (String(value.uid) === String(req.uid)) {
				value.isUser = true;
			} else if (value.uid && idlists.indexOf(value.uid) !== -1) {
				value.isFollowing = true;
			} else {
				value.isFollowing = false;
			}
		});
		_.forEach(userData.followers, function (value) {
			if (String(value.uid) === String(req.uid)) {
				value.isUser = true;
			} else if (value.uid && idlists.indexOf(value.uid) !== -1) {
				value.isFollowing = true;
			} else {
				value.isFollowing = false;
			}
		});
		var name = 'follow';
		userData.title = '[[pages:' + tpl + ', ' + userData.username + ']]';
		// var count = name === 'following' ? userData.followingCount : userData.followerCount;
		// var pageCount = Math.ceil(count / resultsPerPage);
		// userData.pagination = pagination.create(page, pageCount);
		userData.breadcrumbs = helpers.buildBreadcrumbs([{ text: userData.username, url: '/user/' + userData.userslug }, { text: '[[user:' + name + ']]' }]);
		if (req.query.hide) {
			userData.toIframe = true;
		}
		userData.nav = [
			{
				name: '主题',
				slug: !userData.toIframe ? '' : '?hide=true',
				css: '',
			},
			{
				name: '回复',
				slug: !userData.toIframe ? '/posts' : '/posts?hide=true',
				css: '',
			},
			{
				name: '关注',
				slug: !userData.toIframe ? '/follow' : '/follow?hide=true',
				css: 'active',
			},
			{
				name: '收藏',
				slug: !userData.toIframe ? '/bookmarks' : '/bookmarks?hide=true',
				css: '',
			},
		];
		res.render(tpl, userData);
	});
}

module.exports = followController;
