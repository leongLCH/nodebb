'use strict';

var async = require('async');
var nconf = require('nconf');

var topics = require('../topics');
var meta = require('../meta');
var _ = require('lodash');
var helpers = require('./helpers');
var user = require('../user');
var privileges = require('../privileges');
var htmlToText = require('html-to-text');
var categories = require('../categories');
var utils = require('../utils');
var validator = require('validator');
var pagination = require('../pagination');
var db = require('../database');
var group = require('../groups');

var popularController = module.exports;

var anonCache = {};
var lastUpdateTime = 0;

var terms = {
	daily: 'day',
	weekly: 'week',
	monthly: 'month',
};

popularController.get = function (req, res, next) {
	var page = parseInt(req.query.page, 10) || 1;
	var term = terms[req.params.term];
	var cid = req.query.cid;
	var allCategoriesData;
	var userData;
	var popular;
	var statistics;
	var monthlyPopular;
	var mcid = cid ? cid[0] : 1;
	if (!term && req.params.term) {
		return next();
	}
	term = term || 'alltime';
	// term = 'monthly'
	var termToBreadcrumb = {
		day: '[[recent:day]]',
		week: '[[recent:week]]',
		month: '[[recent:month]]',
		alltime: '[[global:header.popular]]',
	};

	if (!req.loggedIn) {
		if (anonCache[term] && anonCache[term][page] && (Date.now() - lastUpdateTime) < 60 * 60 * 1000) {
			return res.render('popular', anonCache[term][page]);
		}
	}
	var settings;
	async.waterfall([
		function (next) {
			async.parallel({
				settings: function (next) {
					user.getSettings(req.uid, next);
				},
				privileges: function (next) {
					privileges.categories.get(mcid, req.uid, next);
				},
				rssToken: function (next) {
					user.auth.getFeedToken(req.uid, next);
				},
				// 列表页获取个人信息
				userData: function (next) {
					if (req.uid) {
						getUserData([req.uid], next)
					} else {
						next(null, []);
					}
				},
				// 获取热门
				// popular: function (next) {
				// 	topics.getPopular('alltime', req.uid, 4, next);
				// },
				// 获取所有版块
				allCategoriesData: function (next) {
					categories.getCategoriesByPrivilege('cid:0:children', req.uid, 'find', next);
				},
				statistics: function (next) {
					getStatsForSet('topics:tid', 'topicCount', next);
				},
				monthlyPopular: function (next) {
					topics.getPopularTopics('month', req.uid, 0, 6, next);
				}
			}, next);
		},
		function (results, next) {
			allCategoriesData = results.allCategoriesData;
			if (_.size(results.userData) > 0) {
				userData = _.first(results.userData);
			} else {
				userData = {};
			}
			settings = results.settings;
			// popular = results.popular;
			statistics = results.statistics;
			monthlyPopular = results.monthlyPopular.topics;
			allCategoriesData.unshift({
				cid: 0,
				name: '所有分类',
			});
			var start = Math.max(0, (page - 1) * settings.topicsPerPage);
			var stop = start + settings.topicsPerPage - 1;
			topics.getPopularTopics(term, req.uid, start, stop, next);
			// topics.getPopular(term, req.uid, meta.config.topicsPerList, next);
		},
		// function (_settings, next) {
		// 	var start = Math.max(0, (page - 1) * settings.topicsPerPage);
		// 	var stop = start + settings.topicsPerPage - 1;
		// 	topics.getPopularTopics(term, req.uid, start, stop, next);
		// },
		function (data) {
			var pageCount = Math.max(1, Math.ceil(data.topicCount / settings.topicsPerPage));

			data.title = meta.config.homePageTitle || '[[pages:home]]';
			data['feeds:disableRSS'] = parseInt(meta.config['feeds:disableRSS'], 10) === 1;
			data.rssFeedUrl = nconf.get('relative_path') + '/popular/' + (req.params.term || 'alltime') + '.rss';
			data.term = term;
			data.pagination = pagination.create(page, pageCount, req.query);

			if (req.originalUrl.startsWith(nconf.get('relative_path') + '/api/popular') || req.originalUrl.startsWith(nconf.get('relative_path') + '/popular')) {
				data.title = '[[pages:popular-' + term + ']]';
				var breadcrumbs = [{
					text: termToBreadcrumb[term]
				}];

				if (req.params.term) {
					breadcrumbs.unshift({
						text: '[[global:header.popular]]',
						url: '/popular'
					});
				}

				data.breadcrumbs = helpers.buildBreadcrumbs(breadcrumbs);
			}

			if (!req.loggedIn) {
				anonCache[term] = anonCache[term] || {};
				anonCache[term][page] = data;
				lastUpdateTime = Date.now();
			}

			data.allCategoriesData = allCategoriesData;
			// 话题列表过滤
			var allTopics = data.topics;
			var filterT = _.filter(allTopics, function (t) {
				if (!cid) return !t.deleted || data.privileges.isAdminOrMod;
				return (!t.deleted || data.privileges.isAdminOrMod) && t.cid.toString() === mcid.toString();
			});
			_.forEach(filterT, function (t) {
				t.releaseT = {
					year: utils.getFormateTime(t.timestamp, 'year'),
					date: utils.getFormateTime(t.timestamp, 'date'),
				};
				var content = t.mainPost.content;
				t.bookmarked = t.mainPost.bookmarks || 0;
				var text = htmlToText.fromString(content, {
					hideLinkHrefIfSameAsText: true,
					ignoreHref: true,
					ignoreImage: true,
					wordwrap: 20,
					singleNewLineParagraphs: true,
				});
				t.thumbnail = '<p>' + utils.cutByte(text, 250) + '</p>';
				if (t.teaser) {
					t.teaser.timestamp = utils.getFormateTime(t.lastposttime);
					t.teaser.isUser = t.teaser.user.uid === req.uid ? true : false;
					if (Array.isArray(t.teaser.user.group)) {
						_.forEach(t.teaser.user.group, (value) => {
							if (value.description === 'goldminer') {
								t.teaser.user.isOfficial = true;
								t.teaser.user.groupName = value.groupName;
							}
						});
					}
				}
				t.postcount -= 1;
				if (t.strategyId && (t.hasPerformance || !t.hasOwnProperty('hasPerformance'))) {
					t.hasIframe = true;
					t.iframeUrl = nconf.get('thumb') + t.strategyId + '?size=small';
				} else {
					t.hasIframe = false;
				}
				t.isUser = t.uid === req.uid ? true : false;
				t.isOfficial = false;
				if (t.user && Array.isArray(t.user.group)) {
					_.forEach(t.user.group, (value) => {
						if (value.description === 'goldminer') {
							t.isOfficial = true;
							t.groupName = value.groupName;
						}
					});
				}
			});
			data.topics = filterT;
			data.domainLogin = nconf.get('domain').url + nconf.get('domain').login;
			data.domainReg = nconf.get('domain').url + nconf.get('domain').reg;

			data.offical = nconf.get('domain').url;

			// 个人信息
			data.getUserData = {};
			if (userData) {
				data.getUserData.picture = _.get(userData, 'picture', '');
				data.getUserData.userslug = _.get(userData, 'userslug', '');
				data.getUserData.myquantid = _.get(userData, 'myquantid', 0);
				data.getUserData.aboutme = _.get(userData, 'aboutme', '');
				data.getUserData.email = validator.escape(String(_.get(userData, 'email') || ''));
				data.getUserData.username = validator.escape(String(_.get(userData, 'username') || ''));
				data.getUserData.fullname = validator.escape(String(_.get(userData, 'fullname') || ''));
				data.getUserData.topiccount = parseInt(_.get(userData, 'topiccount'), 10) || 0;
				var num = parseInt(_.get(userData, 'postcount', 0), 10) - parseInt(_.get(userData, 'topiccount', 0), 10);
				data.getUserData.replycount = num < 0 ? 0 : num;
				data.getUserData.bookmark = _.get(userData, 'bookmark', 0);
				data.getUserData.isOfficial = false;
				if (_.size(userData.group)) {
					_.forEach(userData.group, (value) => {
						if (value.description === 'goldminer') {
							data.getUserData.isOfficial = true;
							data.getUserData.groupName = value.groupName;
						}
					});
				}
			}

			// 获取热门话题
			// data.getPopular = [];
			// if (_.size(popular) > 0) {
			// 	_.forEach(popular, function (p) {
			// 		data.getPopular.push(_.pick(p, ['title', 'slug']));
			// 	});
			// }
			//
			var chooseCategory = _.find(data.allCategoriesData, function (o) {
				return cid ? cid[0].toString() === o.cid.toString() : o.cid === 0;
			});
			data.selectedCategory = chooseCategory;
			data.originalUrl = nconf.get('url') + '/popular';
			data.cid = !cid ? 1 : cid[0];
			data.statistics = statistics;
			data.monthlyPopular = monthlyPopular;
			res.render('popular', data);
		},
	], next);
};

function getGlobalField(field, callback) {
	db.getObjectField('global', field, function (err, count) {
		callback(err, parseInt(count, 10) || 0);
	});
}

function getStatsForSet(set, field, callback) {
	var terms = {
		day: 86400000,
		week: 604800000,
		month: 2592000000,
	};

	var now = Date.now();
	async.parallel({
		day: function (next) {
			db.sortedSetCount(set, now - terms.day, '+inf', next);
		},
		week: function (next) {
			db.sortedSetCount(set, now - terms.week, '+inf', next);
		},
		month: function (next) {
			db.sortedSetCount(set, now - terms.month, '+inf', next);
		},
		alltime: function (next) {
			getGlobalField(field, next);
		},
	}, callback);
}

function getUserData(uids, callback) {
	async.parallel([
		async.apply(user.getUsersFields, uids, ['uid', 'username', 'fullname', 'userslug', 'reputation', 'postcount', 'picture', 'signature', 'banned', 'status', 'myquantid']),
			async.apply(group.getUserGroups, uids),
	], function (err, results) {
		if (!err) {
			var userLists = results[0];
			if (_.size(userLists) > 0) {
				var newLists = _.map(userLists, (value, index) => {
					if (_.size(results[1][index]) > 0) {
						var groupArry = [];
						_.forEach(results[1][index], (value) => {
							groupArry.push({
								groupName: value.name,
								description: (value.description || '')
							});
						});
						value.group = groupArry;
					}
					return value;
				});
				callback(null, newLists);
			} else {
				callback(null, results[0]);
			}
		}
	});
}