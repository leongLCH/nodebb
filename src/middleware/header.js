'use strict';

var async = require('async');
var nconf = require('nconf');
var jsesc = require('jsesc');
var _ = require('lodash');

var db = require('../database');
var user = require('../user');
var topics = require('../topics');
var messaging = require('../messaging');
var meta = require('../meta');
var plugins = require('../plugins');
var navigation = require('../navigation');
var translator = require('../translator');
var privileges = require('../privileges');
var utils = require('../utils');
var group = require('../groups');
var controllers = {
	api: require('../controllers/api'),
	helpers: require('../controllers/helpers'),
};

module.exports = function (middleware) {
	middleware.buildHeader = function (req, res, next) {
		res.locals.renderHeader = true;
		res.locals.isAPI = false;
		async.waterfall([
			function (next) {
				middleware.applyCSRF(req, res, next);
			},
			function (next) {
				async.parallel({
					config: function (next) {
						controllers.api.getConfig(req, res, next);
					},
					plugins: function (next) {
						plugins.fireHook('filter:middleware.buildHeader', { req: req, locals: res.locals }, next);
					},
				}, next);
			},
			function (results, next) {
				res.locals.config = results.config;
				next();
			},
		], next);
	};

	middleware.renderHeader = function (req, res, data, callback) {
		var registrationType = meta.config.registrationType || 'normal';
		res.locals.config = res.locals.config || {};
		var templateValues = {
			title: meta.config.title || '',
			'title:url': meta.config['title:url'] || '',
			description: meta.config.description || '',
			'cache-buster': meta.config['cache-buster'] || '',
			'brand:logo': meta.config['brand:logo'] || '',
			'brand:logo:url': meta.config['brand:logo:url'] || '',
			'brand:logo:alt': meta.config['brand:logo:alt'] || '',
			'brand:logo:display': meta.config['brand:logo'] ? '' : 'hide',
			allowRegistration: registrationType === 'normal' || registrationType === 'admin-approval' || registrationType === 'admin-approval-ip',
			searchEnabled: plugins.hasListeners('filter:search.query'),
			config: res.locals.config,
			relative_path: nconf.get('relative_path'),
			bodyClass: data.bodyClass,
		};

		templateValues.configJSON = jsesc(JSON.stringify(res.locals.config), { isScriptContext: true });

		async.waterfall([
			function (next) {
				async.parallel({
					isAdmin: function (next) {
						user.isAdministrator(req.uid, next);
					},
					isGlobalMod: function (next) {
						user.isGlobalModerator(req.uid, next);
					},
					isModerator: function (next) {
						user.isModeratorOfAnyCategory(req.uid, next);
					},
					privileges: function (next) {
						privileges.global.get(req.uid, next);
					},
					user: function (next) {
						var userData = {
							uid: req.uid,
							username: '[[global:guest]]',
							userslug: '',
							fullname: '[[global:guest]]',
							email: '',
							picture: user.getDefaultAvatar(),
							status: 'offline',
							reputation: 0,
							'email:confirmed': 0,
							isOfficial: false,
						};
						if (req.loggedIn) {
							// user.getUserFields(req.uid, Object.keys(userData), next);
							getUserData([req.uid], next);
						} else {
							next(null, userData);
						}
					},
					isEmailConfirmSent: function (next) {
						if (!meta.config.requireEmailConfirmation || !req.uid) {
							return next(null, false);
						}
						db.get('uid:' + req.uid + ':confirm:email:sent', next);
					},
					languageDirection: function (next) {
						translator.translate('[[language:dir]]', res.locals.config.userLang, function (translated) {
							next(null, translated);
						});
					},
					browserTitle: function (next) {
						translator.translate(controllers.helpers.buildTitle(translator.unescape(data.title)), function (translated) {
							next(null, translated);
						});
					},
					navigation: navigation.get,
					tags: async.apply(meta.tags.parse, req, data, res.locals.metaTags, res.locals.linkTags),
					banned: async.apply(user.isBanned, req.uid),
					banReason: async.apply(user.getBannedReason, req.uid),

					unreadTopicCount: async.apply(topics.getTotalUnread, req.uid),
					unreadNewTopicCount: async.apply(topics.getTotalUnread, req.uid, 'new'),
					unreadWatchedTopicCount: async.apply(topics.getTotalUnread, req.uid, 'watched'),
					unreadChatCount: async.apply(messaging.getUnreadCount, req.uid),
					unreadNotificationCount: async.apply(user.notifications.getUnreadCount, req.uid),
				}, next);
			},
			function (results, next) {
				if (req.loggedIn) {
					if (_.size(results.user) > 0) {
						results.user = _.first(results.user);
					}
				} else {
					results.user = results.user;
				}
				if (results.banned) {
					req.logout();
					return res.redirect('/');
				}
				if (results.user && Array.isArray(results.user.group)) {
					_.forEach(results.user.group, (value) => {
						if (value.description === 'goldminer') {
							results.user.isOfficial = true;
							results.user.groupName = value.groupName;
						}
					});
				}
				results.user.isAdmin = results.isAdmin;
				results.user.isGlobalMod = results.isGlobalMod;
				results.user.isMod = !!results.isModerator;
				results.user.privileges = results.privileges;

				results.user.uid = parseInt(results.user.uid, 10);
				results.user.email = String(results.user.email);
				results.user['email:confirmed'] = parseInt(results.user['email:confirmed'], 10) === 1;
				results.user.isEmailConfirmSent = !!results.isEmailConfirmSent;

				setBootswatchCSS(templateValues, res.locals.config);

				var unreadCount = {
					topic: results.unreadTopicCount || 0,
					newTopic: results.unreadNewTopicCount || 0,
					watchedTopic: results.unreadWatchedTopicCount || 0,
					chat: results.unreadChatCount || 0,
					notification: results.unreadNotificationCount || 0,
				};
				Object.keys(unreadCount).forEach(function (key) {
					if (unreadCount[key] > 99) {
						unreadCount[key] = '99+';
					}
				});

				results.navigation = results.navigation.map(function (item) {
					if (item.originalRoute === '/unread' && results.unreadTopicCount > 0) {
						return Object.assign({}, item, {
							content: unreadCount.topic,
							iconClass: item.iconClass + ' unread-count',
						});
					}
					if (item.originalRoute === '/unread/new' && results.unreadNewTopicCount > 0) {
						return Object.assign({}, item, {
							content: unreadCount.newTopic,
							iconClass: item.iconClass + ' unread-count',
						});
					}
					if (item.originalRoute === '/unread/watched' && results.unreadWatchedTopicCount > 0) {
						return Object.assign({}, item, {
							content: unreadCount.watchedTopic,
							iconClass: item.iconClass + ' unread-count',
						});
					}

					return item;
				});
				// 数据上报地址
				templateValues.colbugsUrl = nconf.get('colbugsUrl');
				templateValues.browserTitle = results.browserTitle;
				templateValues.navigation = results.navigation;
				templateValues.unreadCount = unreadCount;
				templateValues.metaTags = results.tags.meta;
				templateValues.linkTags = results.tags.link;
				templateValues.isAdmin = results.user.isAdmin;
				templateValues.isGlobalMod = results.user.isGlobalMod;
				templateValues.showModMenu = results.user.isAdmin || results.user.isGlobalMod || results.user.isMod;
				templateValues.canChat = results.canChat && parseInt(meta.config.disableChat, 10) !== 1;
				templateValues.user = results.user;
				templateValues.userJSON = jsesc(JSON.stringify(results.user), { isScriptContext: true });
				templateValues.useCustomCSS = parseInt(meta.config.useCustomCSS, 10) === 1 && meta.config.customCSS;
				templateValues.customCSS = templateValues.useCustomCSS ? (meta.config.renderedCustomCSS || '') : '';
				templateValues.useCustomHTML = parseInt(meta.config.useCustomHTML, 10) === 1;
				templateValues.customHTML = templateValues.useCustomHTML ? meta.config.customHTML : '';
				templateValues.maintenanceHeader = parseInt(meta.config.maintenanceMode, 10) === 1 && !results.isAdmin;
				templateValues.defaultLang = meta.config.defaultLang || 'en-GB';
				templateValues.userLang = res.locals.config.userLang;
				templateValues.languageDirection = results.languageDirection;
				templateValues.privateUserInfo = parseInt(meta.config.privateUserInfo, 10) === 1;
				templateValues.privateTagListing = parseInt(meta.config.privateTagListing, 10) === 1;

				templateValues.template = { name: res.locals.template };
				templateValues.template[res.locals.template] = true;
				if (req.route && req.route.path === '/') {
					modifyTitle(templateValues);
				}
				// myquant header
				var myquantUrl = nconf.get('domain').url;
				var nodebbUrl = nconf.get('url');
				templateValues.myquantUrl = myquantUrl;
				templateValues.nodebbUrl = nodebbUrl;
				templateValues.joinus = myquantUrl + nconf.get('domain').joinus;
				templateValues.helpCenter = {
					guide: myquantUrl + nconf.get('domain').guide,
					python: myquantUrl + nconf.get('domain').python,
					data: myquantUrl + nconf.get('domain').data,
					strategy: myquantUrl + nconf.get('domain').strategy,
					faq: myquantUrl + nconf.get('domain').faq,
				};
				templateValues.gm2 = nconf.get('domain').gm2;
				templateValues.search = myquantUrl + nconf.get('domain').search;
				templateValues.login = myquantUrl + nconf.get('domain').login + '?redirect=' + nconf.get('url');
				templateValues.reg = myquantUrl + nconf.get('domain').reg + '?redirect=' + nconf.get('url');
				templateValues.hasBanner = false;
				if (templateValues.template && (templateValues.template.name === 'category' || templateValues.template.name === 'recent') | templateValues.template.name === 'popular') {
					templateValues.hasBanner = true;
				}
				plugins.fireHook('filter:middleware.renderHeader', {
					req: req,
					res: res,
					templateValues: templateValues,
				}, next);
			},
			function (data, next) {
				if (req.query.hide) {
					data.templateValues.toIframe = true;
				}
				req.app.render('header', data.templateValues, next);
			},
		], callback);
	};

	function addTimeagoLocaleScript(scripts, userLang) {
		var languageCode = utils.userLangToTimeagoCode(userLang);
		scripts.push({ src: nconf.get('relative_path') + '/assets/vendor/jquery/timeago/locales/jquery.timeago.' + languageCode + '.js' });
	}

	middleware.renderFooter = function (req, res, data, callback) {
		async.waterfall([
			function (next) {
				plugins.fireHook('filter:middleware.renderFooter', {
					req: req,
					res: res,
					templateValues: data,
				}, next);
			},
			function (data, next) {
				async.parallel({
					scripts: async.apply(plugins.fireHook, 'filter:scripts.get', []),
				}, function (err, results) {
					next(err, data, results);
				});
			},
			function (data, results, next) {
				data.templateValues.scripts = results.scripts.map(function (script) {
					return { src: script };
				});
				addTimeagoLocaleScript(data.templateValues.scripts, res.locals.config.userLang);

				data.templateValues.useCustomJS = parseInt(meta.config.useCustomJS, 10) === 1;
				data.templateValues.customJS = data.templateValues.useCustomJS ? meta.config.customJS : '';
				data.templateValues.domainLogin = nconf.get('domain').url + nconf.get('domain').login;
				data.templateValues.originUrl = nconf.get('url') + '/compose?cid=5';

				req.app.render('footer', data.templateValues, next);
			},
		], callback);
	};

	function modifyTitle(obj) {
		var title = controllers.helpers.buildTitle(meta.config.homePageTitle || '[[pages:home]]');
		obj.browserTitle = title;

		if (obj.metaTags) {
			obj.metaTags.forEach(function (tag, i) {
				// if (tag.property === 'og:title') {
				// 	obj.metaTags[i].content = title;
				// }
			});
		}

		return title;
	}

	function setBootswatchCSS(obj, config) {
		if (config && config.bootswatchSkin !== 'noskin') {
			var skinToUse = '';

			if (parseInt(meta.config.disableCustomUserSkins, 10) !== 1) {
				skinToUse = config.bootswatchSkin;
			} else if (meta.config.bootswatchSkin) {
				skinToUse = meta.config.bootswatchSkin;
			}

			if (skinToUse) {
				obj.bootswatchCSS = '//maxcdn.bootstrapcdn.com/bootswatch/3.3.7/' + skinToUse + '/bootstrap.min.css';
			}
		}
	}

	function getUserData(uids, callback) {
		async.parallel([
			async.apply(user.getUsersFields, uids, ['uid', 'username', 'fullname', 'userslug', 'reputation', 'postcount', 'picture', 'signature', 'banned', 'status', 'myquantid']),
			async.apply(group.getUserGroups, uids),
		], function (err, results) {
			if (!err) {
				var userLists = results[0];
				if (_.size(userLists) > 0) {
					var newLists = _.map(userLists, (value, index) => {
						if (_.size(results[1][index]) > 0) {
							var groupArry = [];
							_.forEach(results[1][index], (value) => {
								groupArry.push({ groupName: value.name, description: (value.description || '') });
							});
							value.group = groupArry;
						}
						return value;
					});
					callback(null, newLists);
				} else {
					callback(null, results[0]);
				}
			}
		});
	}
};

