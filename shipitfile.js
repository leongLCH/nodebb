'use strict';


module.exports = function (shipit) {
	shipit.initConfig({
		staging: {
			deployTo: '~/nodebb/nodebb',
			servers: ['gm3@test.sitestack.cn']
		}
	});

	shipit.task('deploy', function () {
		var srcFolder = shipit.config.deployTo + '/src';
		var pluginFolder = shipit.config.deployTo + '/plugins';
		var publicFolder = shipit.config.deployTo + '/public';
		var pkg = shipit.config.deployTo + '/package.json';

		var buildOutput = shipit.config.deployTo + '/build';

		shipit
			.remote('pwd')
			.then(function () {
				return shipit.remote('~/nodebb/nodebb/nodebb stop');
			})
			.then(function () {
				return shipit.remote('if [ -d ' + buildOutput + ' ]; then rm -rf ' + buildOutput + '; fi');
			})
			.then(function () {
				return shipit.remote('if [ -d ' + srcFolder + ' ]; then rm -rf ' + srcFolder + '; fi');
			})
			.then(function () {
				return shipit.remoteCopy('./src/', srcFolder);
			})
			.then(function () {
				return shipit.remote('if [ -d ' + pluginFolder + ' ]; then rm -rf ' + pluginFolder + '; fi');
			})
			.then(function () {
				return shipit.remoteCopy('./plugins/', pluginFolder);
			})
			.then(function () {
				return shipit.remote('if [ -d ' + publicFolder + ' ]; then rm -rf ' + publicFolder + '; fi');
			})
			.then(function () {
				return shipit.remoteCopy('./public/', publicFolder);
			})
			.then(function () {
				return shipit.remote('if [ -d ' + pkg + ' ]; then rm -rf ' + pkg + '; fi');
			})
			.then(function () {
				return shipit.remoteCopy('./package.json', pkg);
			})
			.then(function () {
				return shipit.remoteCopy('./postinstall.js', shipit.config.deployTo + '/postinstall.js');
			})
			.then(function () {
				return shipit.remote('cd ~/nodebb/nodebb && npm install')
			})
			.then(function () {
				return shipit.remote('~/nodebb/nodebb/nodebb build');
			})
			.then(function () {
				return shipit.remote('node ~/nodebb/nodebb/serve.js')
			})
			.then(function () {
				console.log('deploy complete');
			});
	});


	shipit.task('restart', function () {
		shipit
			.local('~/nodebb/nodebb/nodebb stop')
			.then(function () {
				return shipit.local('node ~/nodebb/nodebb/serve.js')
			})
			.then(function () {
				console.log('deploy complete');
			});
	});
};
