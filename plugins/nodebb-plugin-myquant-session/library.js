'use strict';

/* globals process, require, module, Promise */

var request = require('request');
var Crypto = module.parent.require('./routes/encypt');
var cookieParser = require('cookie-parser');
var express = require('express');
var app = express();
var user = module.parent.require('./user');
var db = module.parent.require('./database');
var _ = module.parent.require('underscore');
var winston = module.parent.require('winston');
var nconf = module.parent.require('nconf');
var nbbAuthController = module.parent.require('./controllers/authentication');

var plugin = {};

function fetchUserProfile(token, cb) {
	var url = nconf.get('sso').url + nconf.get('sso').getBaseInfo;
	var options = {
		url: url,
		headers: { Authorization: 'Bearer ' + token },
		timeout: 5000
	};

	request(options, function callback(error, response, body) {
		if (!error && response.statusCode === 200) {
			try {
				cb(null, JSON.parse(body));
			} catch (e) {
				cb(e, null);
			}
		} else {
			cb(error, null);
		}
	});
}

function getUidByMyquantId(id, cb) {
	db.sortedSetScore('myquantid:uid', id, cb);
}

function getOrCreatUser(profile, cb) {
	getUidByMyquantId(profile.user_id, function (err, uid) {
		winston.verbose('[session]user exist' + err + uid);
		if (err) {
			winston.verbose('[session]create user error ' + err);
			cb(err, 0);
		} else if (uid) {
			user.setUserField(uid, 'picture', profile.avatar_file || '', function (err, resp) {
				winston.verbose('[session]update profile ' + err + resp);
				cb(null, uid);
			});
		} else {
			winston.verbose('[session]create new user');
			createUser(profile, cb);
		}
	});
}
function createUser(profile, cb) {
	user.create({ username: profile.nick_name, myquantid: profile.user_id, picture: profile.avatar_file || '', aboutme: profile.introduction }, cb);
}

plugin.init = function (params, callback) {
	var router = params.router;
	app.use(cookieParser());
	router.get('/t/:token', function (req, res) {
		var token = req.params.token;
		var redirectUrl = req.query && req.query.redirect;
		var fromGoldminer = decodeURIComponent(redirectUrl) === redirectUrl ? false : true;
		winston.verbose('[session]' + token);
		fetchUserProfile(token, function (err, resp) {
			if (err) {
				res.send('error' + err);
			} else {
				getOrCreatUser(resp, function (err, uid) {
					user.getUserField(uid, 'myquantid', function (err, id) {
						winston.verbose('[session]myquantid' + id + err);
					});
					if (!err && uid) {
						var cookie = Crypto.encode(token + '.' + Date.parse( new Date())/1000);
						nbbAuthController.doLogin(req, uid, cookie, function () {
							winston.verbose('[session]login' + uid);
							if(true) {res.cookie('sign', cookie, { expires: new Date(Date.now() + 90000000) })};
							res.redirect(redirectUrl ? decodeURIComponent(redirectUrl) : nconf.get('url'));
						});
					} else {
						res.send('err:' + err + 'resp:' + uid);
					}
				});
			}
		});
	});
	callback();
};

module.exports = plugin;
