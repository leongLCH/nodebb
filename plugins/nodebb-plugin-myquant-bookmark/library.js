'use strict';

var user = module.parent.require('./user');
var db = module.parent.require('./database');
var winston = module.parent.require('winston');

var plugin = {};


function getCount(key, cb) {
    db.getSortedScoresCount(key, cb)
}
plugin.init = function (params, callback) {
    var type = params.current;
    var uid = params.uid;
    if (!type) {
        callback()
    }
    if (!uid) {
        winston.verbose('The user does not exist.');
    }
    var fields = { _id: 0, value: 1 };
    var key = 'uid:' + uid + ':bookmarks';
    getCount(key, function (err, data) {
        if (err) {
            res.send('error' + err);
        } else {
            user.setUserField(uid, 'bookmark', data || 0, function (err, resp) {
                if (!err) {
                    winston.verbose('update bookmark success');
                }
            });
        }
    })
}
module.exports = plugin;