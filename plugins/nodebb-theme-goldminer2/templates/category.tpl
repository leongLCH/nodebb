<div widget-area="header">
	<!-- BEGIN widgets.header -->
	{{widgets.header.html}}
	<!-- END widgets.header -->
</div>
<div class="recent">
	<div class="topic-banner" id="topicBanner" data-url="{offical}">
	</div>
	<div class="topic-banner-container">
		
	</div>
	<div class="category">
		<div class="nav-choose">
			<span class="choose all-category" style="padding-left:55px;">
				<i class="goldminerimg2 g-all"></i>
				<a href="/">所有</a>
			</span>
			<span>
				<i class="goldminerimg2 g-new"></i>
				<a href="/recent">最新</a>
			</span>
			<span>
				<i class="goldminerimg2 g-hot"></i>
				<a href="/popular">热门</a>
			</span>
			<nav class="navbar nav-container navbar-default nav-phone visible-xs" style="min-height:0!important;">
				<div class="navbar-header visible-xs-block visible-nav-show">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bbs-nav">
						<span class="sr-only">切换导航</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<div class="collapse navbar-collapse nav-phone-visible-xs" id="bbs-nav">
					<ul class="nav nav-pills category-nav col-lg-12 col-md-12 col-sm-12 category-select-side">
						<!-- BEGIN allCategoriesData -->
						<li data-cid="{allCategoriesData.cid}" class="main-labels"><a href="{config.relative_path}/category/{allCategoriesData.cid}/"  class="categorylink">{allCategoriesData.name}</a></li>
						<!-- END allCategoriesData -->
					</ul>
				</div>
			</nav>
			<div class="bbs-search hidden-xs">
				<input type="text" class="search-form" placeholder="[[global:search]]" name="query" value="" id="bbs-search-input">
				<a href="#" component="" class="btn btn-primary search-btn" role="button" id="bbs-search-button">搜索</a>
			</div>
		</div>


		<a href="{config.relative_path}/{selectedFilter.url}{querystring}">
			<div class="alert alert-warning hide" id="new-topics-alert"></div>
		</a>
		<div class="recent-container">
			<div class="container-topic-list">
				<!-- IF !topics.length -->
				<div class="alert alert-warning" id="category-no-topics">[[recent:no_recent_topics]]</div>
				<!-- ENDIF !topics.length -->
				<!-- IMPORT partials/topics_list.tpl -->

				<!-- IF config.usePagination -->
				<!-- IMPORT partials/paginator.tpl -->
				<!-- ENDIF config.usePagination -->
			</div>
			<div class="container-side-right hidden-xs hidden-sm">
			<div class="personal">
				<!-- IF loggedIn -->
				<p class="personal-newtopic">
					<a href="{config.relative_path}/compose?cid={cid}" component="category/post" class="btn btn-primary col-lg-12 col-md-12 new-topic-btn"
					role="button" target="_blank" target="_blank">
						<!-- <span class="goldminerimg g-post-category"></span> -->发表新主题</a>
				</p>
				<!-- ELSE -->
				<p class="personal-newtopic">
					<a href="{domainLogin}?redirect={originUrl}" class="btn btn-primary col-lg-12 col-md-12 new-topic-btn">发表新主题</a>
				</p>
				<!-- ENDIF loggedIn -->
			</div>
				<p class="title	"><i class="hotTopic-ico"></i>热门标签</p>
				<!-- <ul class="category-select-dropdown"> -->
				<ul class="category-select-side">
					<!-- BEGIN allCategoriesData -->
					<li data-cid="{allCategoriesData.cid}" class="main-labels"><a href="{config.relative_path}/category/{allCategoriesData.cid}/">{allCategoriesData.name}</a></li>
					<!-- END allCategoriesData -->
				</ul>
				<p class="title" style="margin-top:28px;"><i class="hotTopic-ico"></i>热门推荐</p>
				<ol class="row hotTopic">
					<!-- BEGIN monthlyPopular -->
					<li><a href="{config.relative_path}/topic/{monthlyPopular.tid}" target="_blank">{monthlyPopular.title}</a></li>
					<!-- END monthlyPopular -->
				</ol>
				<div class="row advert" data-url="{offical}" id="advert">
				</div>
			</div>
		</div>
	</div>
</div>