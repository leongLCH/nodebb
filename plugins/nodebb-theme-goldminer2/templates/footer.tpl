</div><!-- END container -->
<!-- IF !toIframe -->
<!-- IMPORT partials/myquantFooter.tpl -->
<!-- ENDIF !toIframe -->
</main>
<!-- IF !toIframe -->
<div class="container-fluid" id="footer">
		<div class="container">
			<div class="item hidden-xs">
				<a href="/docs/data" target="_blank" title="数据文档" class="strong">数据文档</a>
				<ul>
					<li>
						<a href="/docs/data/98" target="_blank" title="基础数据">基础数据</a>
					</li>
					<li>
						<a href="/docs/data/86" target="_blank" title="股票数据">股票数据</a>
					</li>
					<li>
						<a href="/docs/data/88" target="_blank" title="期货数据">期货数据</a>
					</li>
					<li>
						<a href="/docs/data/90" target="_blank" title="财务数据">财务数据</a>
					</li>
					<li>
						<a href="/docs/data/81" target="_blank" title="指数数据">指数数据</a>
					</li>
					<li>
						<a href="/docs/data/96" target="_blank" title="行业概念数据">行业概念数据</a>
					</li>
					<li>
						<a href="/docs/data/84" target="_blank" title="交易日数据">交易日数据</a>
					</li>
				</ul>
			</div>
			<div class="item hidden-xs">
				<a href="/docs/python" class="strong">Python API</a>
				<ul>
					<li>
						<a href="/docs/python/73" target="_blank" title="快速开始">快速开始</a>
					</li>
					<li>
						<a href="/docs/python/152" target="_blank" title="策略程序架构">策略程序架构</a>
					</li>
					<li>
						<a href="/docs/python/45" target="_blank" title="重要概念">重要概念</a>
					</li>
					<li>
						<a href="/docs/python/47" target="_blank" title="数据结构">数据结构</a>
					</li>
					<li>
						<a href="/docs/python/43" target="_blank" title="API介绍">API介绍</a>
					</li>
					<li>
						<a href="/docs/python/50" target="_blank" title="枚举常量">枚举常量</a>
					</li>
					<li>
						<a href="/docs/python/71" target="_blank" title="错误码">错误码</a>
					</li>
				</ul>
			</div>
			<div class="item">
				<div class="strong">
					联系我们
				</div>
				<ul>
					<li>
						<a href="javascript:void(0);">深圳市南山区滨海大道海天二路19号盈峰中心1818</a>
					</li>
					<li>
						<a href="javascript:void(0);">电话: 0755-86962125</a>
					</li>
					<li>
						<a href="javascript:void(0);">邮箱: bd@myquant.cn</a>
					</li>
					<li>
						<a href="javascript:void(0);">邮编: 518000</a>
					</li>
				</ul>
			</div>
			<div class="item qrcode">
				<div class="qrcode-item">
					<div>
						<a title="掘金2交流群" href="https://jq.qq.com/?_wv=1027&k=tWIVoKM2"
						 target="_blank">
							<img src="{config.relative_path}/images/qrcode.jpg" alt="掘金2交流群">
						</a>
					</div>
					<div>
						掘金2交流群
					</div>
					<div>
						<a href="/docs/gm3_faq/154" target="_blank" title="更多群组" class="mq-btn">更多群组</a>
					</div>
				</div>
				<div class="qrcode-item">
					<div>
						<a title="微信公众号" href="javascript:void(0);" target="_blank">
							<img src="{config.relative_path}/images/qrcode-wechat.jpg" alt="微信公众号">
						</a>
					</div>
					<div>
						微信公众号
					</div>
				</div>
			</div>
		</div>
	</div>


	<div class="container-fluid" id="footer-bottom" data-referer="http://192.168.0.244:8181/">
		<div class="container">
			<div class="row">
				<div class="col-xs-12" style="padding-left:0;">
					<div class="pull-left">
						<div class="col-xs-12 nopadding" style="padding-left:0;">
							©2013-2018 深圳红树科技有限公司 版权所有
							<a href="https://beian.miit.gov.cn/" class="hidden-xs" title="粤ICP备14007024号-2" target="_blank">粤ICP备14007024号-2</a>
						</div>
						<div class="visible-xs col-xs-12 nopadding">
							<a href="https://beian.miit.gov.cn/" title="粤ICP备14007024号-2" target="_blank">粤ICP备14007024号-2</a>
						</div>
					</div>
					<div class="pull-right">
						<a href="/sitemap.xml" target="_blank" style="color: #16181d;cursor:default;text-decoration: none;">网站地图</a>
						<a href="//szcert.ebs.org.cn/d8fe3fb8-82a2-4e74-a24a-103a844fdb5c" target="_blank" title="工商网监">
							<img src="{config.relative_path}/images/govIcon.gif" alt="工商网监"> 工商网监 </a>
					</div>
				</div>
			</div>
		</div>
	</div>
<div class="topic-search hidden">
<div class="btn-group">
    <button type="button" class="btn btn-default count"></button>
    <button type="button" class="btn btn-default prev"><i class="fa fa-fw fa-angle-up"></i></button>
    <button type="button" class="btn btn-default next"><i class="fa fa-fw fa-angle-down"></i></button>
</div>
</div>
<!-- ENDIF !toIframe -->
<div component="toaster/tray" class="alert-window">
<div id="reconnect-alert" class="alert alert-dismissable alert-warning clearfix hide" component="toaster/toast">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <p>[[global:reconnecting-message, {config.siteTitle}]]</p>
</div>
</div>
<div id="func-bar">
	<ul>
		<li><!-- IF loggedIn --><a href="{config.relative_path}/compose?cid=5" target="_blank" class="question"><i class="fa fa-question-circle"></i>
				<div>
					提问
				</div>
			</a><!-- ELSE --><a href="#" class="question" id="loginQuestion"><i class="fa fa-question-circle"></i>
				<div>
					提问
				</div>
			</a>
			<!-- ENDIF loggedIn -->
			
			<div class="tips">
				遇到问题？到社区提问，我们为您解答. <i class="fa fa-caret-right"></i>
			</div>
		</li>
		<li><a data-placement="left" href="mailto:support@myquant.cn" target="_blank" class="commercial"><i class="fa fa-envelope-o"></i><div style="line-height: 120%;">商业<br>咨询</div></a><div class="tips">有商业合作意向？请发邮件联系我们.<i class="fa fa-caret-right"></i></div></li>
		<li><a data-placement="left" href="https://jq.qq.com/?_wv=1027&k=tWIVoKM2"
			 target="_blank" class="qq"><i class="fa fa-qq"></i>
				<div>
					QQ
				</div>
			</a>
			<div class="tips">
				加入掘金量化QQ群 <i class="fa fa-caret-right"></i>
			</div>
		</li>
		<li><a href="https://www.myquant.cn/static/images/qrcode-wechat.jpg" target="_blank" class="wechat"><i class="fa fa-wechat"></i>
				<div>
					公众号
				</div>
			</a>
			<div class="tips">
				关注掘金量化微信公众号 <i class="fa fa-caret-right"></i>
			</div>
		</li>
		<li><a href="javascript:void(0);" class="top"><i class="fa fa-angle-up"></i>
				<div>
					TOP
				</div>
			</a>
			<div class="tips">
				返回顶部 <i class="fa fa-caret-right"></i>
			</div>
		</li>
	</ul>
	
</div>
<div id="modal-ask" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="ask-close close" data-dismiss="modal" aria-hidden="true"> × </button>
                <h4 class="modal-title" id="myModalLabel">温馨提示</h4>
            </div>
            <div class="modal-body">你当前未登录，是否登录后提问？</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-xs ask-close" style="background-color: #F1F1F1 !important;border-color:#dddddd !important;" data-dismiss="modal">暂不提问</button>
                <a href="/user/login?redirect=%2fcommunity%2fcompose%3fcid%3d5" type="button" class="btn btn-danger btn-xs" style="margin-left: 15px;background-color: #EC3C2E;border-color:#EC3C2E;">登录提问</a>
            </div>
        </div>
    </div>
</div>
<script defer src="{relative_path}/assets/nodebb.min.js?{config.cache-buster}"></script>

<!-- BEGIN scripts -->
<script defer type="text/javascript" src="{scripts.src}"></script>
<!-- END scripts -->
<script typet="text/javascript" src="//res.wx.qq.com/open/js/jweixin-1.2.0.js"></script>
<!-- IF config.statistics -->
<script type="text/javascript">{config.statistics}</script>
<!-- ENDIF config.statistics -->
<script>
window.addEventListener('load', function () {
    require(['forum/myquantHeader','forum/footer']);
    $(function () {
var _url="/wechat/share";
var o = {url: location.href}
$.get((config.myquantUrlDomain + _url),o,function (res) {
    if (res.errcode==0){
        wx.config({
            debug: false,
            appId:res.data.appId,
            timestamp:res.data.timestamp,
            nonceStr: res.data.nonceStr,
            signature:res.data.signature,
            jsApiList: [
                'checkJsApi',
                'onMenuShareTimeline',
                'onMenuShareAppMessage',
                'onMenuShareQQ',
                'onMenuShareQZone',
                'onMenuShareWeibo',
            ]
        });

        wx.ready(function () {
            var link = res.data.link;
            var title = $("title").text();
            var desc =$("[name=description]").attr("content") || $("title").text(); 
            var imgUrl="http://www.myquant.cn/static/images/logo-share.png";
            var shareObj={
                title: title,
                desc: desc, 
                link: link,
                imgUrl: imgUrl
            };

            wx.onMenuShareTimeline(shareObj);
            wx.onMenuShareAppMessage(shareObj);
            wx.onMenuShareQQ(shareObj);
            wx.onMenuShareQZone(shareObj);
            wx.onMenuShareWeibo(shareObj);
        });
    }
});

})
$("#func-bar .top").click(function () {
		var a = { scrollTop: 0 };
        $('html,body,.main-content,#doc-search .search-right').animate( a, 200);
    });
    <!-- IF useCustomJS -->
    {{customJS}}
    <!-- ENDIF useCustomJS -->
});
</script>
<div class="hide">
<!-- IMPORT 500-embed.tpl -->
</div>
</body>
</html>
