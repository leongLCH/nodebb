
<div class="notifications">
	<div class="notification-title">消息列表</div>
	<div class="notification-line"></div>
	<div class="alert alert-warning text-center <!-- IF notifications.length -->hidden<!-- ENDIF notifications.length -->">
		[[notifications:no_notifs]]
	</div>

	<ul class="notifications-list" data-nextstart="{nextStart}">
	<!-- BEGIN notifications -->
		<li data-nid="{notifications.nid}" class="{notifications.readClass}" component="notifications/item">
			<!-- IF notifications.user.picture -->
			<a href="{config.relative_path}/user/{notifications.user.myquantid}" target="_blank" class="avatarimg"><img component="user/picture" data-uid="{notifications.user.uid}" src="{notifications.user.picture}" class="user-img" /></a>
			<!-- ELSE -->
			<a href="{config.relative_path}/user/{notifications.user.myquantid}" target="_blank" class="avatarimg"><img src="{config.relative_path}/images/user-icon.png"/></a>
			<!-- ENDIF notifications.user.picture -->
			<p>
				<a component="notifications/item/link" href="{config.relative_path}{notifications.path}"><!-- IF notifications.groupsend --><span class="admin-msg">[掘金官方消息]</span><!-- ENDIF notifications.groupsend -->{notifications.bodyLong}</a></a>
			</p>
			<p class="timestamp">
				<span class="timeago" title="{notifications.datetimeISO}"></span>
				{notifications.bodyShort}
			</p>
		</li>
	<!-- END notifications -->
	</ul>
	<!-- IMPORT partials/paginator.tpl -->
</div>


