<div widget-area="header">
	<!-- BEGIN widgets.header -->
	{{widgets.header.html}}
	<!-- END widgets.header -->
</div>
<div class="recent search-result"  <!-- IF searchQuery -->data-search="{searchQuery}"<!-- ENDIF searchQuery -->>
	<div class="topic-banner">
	</div>
	<div class="topic-banner-container">
	</div>
	<div class="category">
		<div class="nav-choose">
			<span class="all-category search-result" style="padding-left:85px;">
				共{matchCount}条结果匹配 "{term}"
			</span>
			<nav class="navbar nav-container navbar-default nav-phone visible-xs" style="min-height:0!important;">
				<div class="navbar-header visible-xs-block visible-nav-show">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bbs-nav">
						<span class="sr-only">切换导航</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<div class="collapse navbar-collapse nav-phone-visible-xs" id="bbs-nav">
					<ul class="nav nav-pills category-nav col-lg-12 col-md-12 col-sm-12 category-select-side">
						<!-- BEGIN allCategoriesData -->
						<li data-cid="{allCategoriesData.cid}" class="main-labels"><a href="{config.relative_path}/category/{allCategoriesData.cid}/"  class="categorylink">{allCategoriesData.name}</a></li>
						<!-- END allCategoriesData -->
					</ul>
				</div>
			</nav>
			<div class="bbs-search">
				<input type="text" class="search-form" placeholder="[[global:search]]" name="query" value="" id="bbs-search-input">
				<a href="#" component="" class="btn btn-primary search-btn" role="button" id="bbs-search-button">搜索</a>
			</div>
		</div>


		<a href="{config.relative_path}/{selectedFilter.url}{querystring}">
			<div class="alert alert-warning hide" id="new-topics-alert"></div>
		</a>
		<div class="recent-container">
			<div class="container-topic-list" style="margin-right:36px;">
				<!-- IF !posts.length -->
				<div class="alert alert-warning" id="category-no-topics">[[recent:no_recent_topics]]</div>
				<!-- ENDIF !posts.length -->
				<!-- IMPORT partials/search_topics_list.tpl -->

				<!-- IF config.usePagination -->
				<!-- IMPORT partials/paginator.tpl -->
				<!-- ENDIF config.usePagination -->
			</div>
		</div>
	</div>
</div>