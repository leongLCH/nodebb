<div class="account">
	<!-- IMPORT partials/account/header.tpl -->

	<div class="account-container"<!-- IF toIframe -->style="margin-top:250px;"<!-- ENDIF toIframe -->>

		<!-- IF !topics.length -->
			<div class="alert alert-warning text-center">{noItemsFoundKey}</div>
		<!-- ENDIF !topics.length -->

		<div class="category">
			<!-- IMPORT partials/topics_list.tpl -->
			<!-- IF config.usePagination -->
				<!-- IMPORT partials/paginator.tpl -->
			<!-- ENDIF config.usePagination -->
		</div>
	</div>
</div>
