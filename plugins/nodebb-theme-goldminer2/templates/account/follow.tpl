<div class="account" >
	<!-- IMPORT partials/account/header.tpl -->
	<div class="account-container" style="<!-- IF toIframe -->margin-top:250px;<!-- ENDIF toIframe --> padding-top:15px;">
	<div class="users row follow">
        <div class="title">Ta关注</div>
		<ul id="users-container" class="users-container" data-nextstart="{nextStart}">
			<!-- IMPORT partials/following_list.tpl -->
		</ul>
		<!-- IF !following.length -->
		<div class="alert alert-warning text-center">[[user:has_no_follower]]</div>
		<!-- ENDIF !following.length -->

		<!-- IMPORT partials/paginator.tpl -->
	</div>
    <div class="users row follow">
        <div class="title">关注Ta</div>
		<ul id="users-container" class="users-container" data-nextstart="{nextStart}">
			<!-- IMPORT partials/followers_list.tpl -->
		</ul>

		<!-- IF !followers.length -->
		<div class="alert alert-warning text-center">[[user:has_no_follower]]</div>
		<!-- ENDIF !followers.length -->

		<!-- IMPORT partials/paginator.tpl -->
	</div>
	</div>
	
</div>