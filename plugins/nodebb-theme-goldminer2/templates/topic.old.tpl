<div widget-area="header">
	<!-- BEGIN widgets.header -->
	{{widgets.header.html}}
	<!-- END widgets.header -->
</div>
<div class="row topic">

	<div class="topic-items col-lg-9 col-md-8 col-sm-12">
		<div class="topic-title row col-lg-12 col-md-12 col-sm-12"><span class="row col-lg-10 col-md-10 col-sm-10">{title}<i class="hasPinned <!-- IF !pinned -->hide<!-- ENDIF !pinned -->">[[topic:pinned]]</i>
				<i class="hasHighLighted <!-- IF !highlighted -->hide<!-- ENDIF !highlighted -->">[[topic:highlighted]]</i></span><!-- IF selfPost --><span class="row col-lg-2 col-md-2 col-sm-2" data-pid ="{mainPid}"<!-- IMPORT partials/data/topic.tpl -->><span class="s-edit"><span class="goldminerimg g-edit"></span><a component="topic/edit" role="menuitem" tabindex="-1" href="#" style="font-size:14px">[[topic:edit]]</a></span><span><span class="goldminerimg g-delete"></span><a component="topic/user/delete" role="menuitem" tabindex="-1" href="#" style="font-size:14px;color:#ababab;font-weight:normal">[[topic:delete]]</a><span></span><!-- ENDIF selfPost --></div>
		<div class="topic-relate row col-lg-12 col-md-12 col-sm-12">
			<div class="releaseT"><span class="name">{getUserData.username}</span>发表在<span class="classify">{classify}</span>{releaseT}</div>
			<div class="record row">
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center">浏览: <span class="viewcount">{viewcount}</span></div><div class="col-lg-4 col-md-4 text-center col-sm-4 col-xs-4 ">回复: <span class="postcount">{postcount}</span></div>
				<div class="col-lg-4 text-center col-md-4 col-sm-4 col-xs-4 ">收藏: <span class="upvotes"><!-- IF bookmarks -->{bookmarks}<!-- ELSE -->0<!-- ENDIF bookmarks --></span></div>
			</div>
		</div>
		<div class="topic-container row col-lg-12 col-md-12 col-sm-12">
		{content}
		</div>
		<!-- IF iframeUrl -->
		<div class="iframe-container row col-lg-12 col-md-12 col-sm-12 hidden-xs">
			<iframe src="{iframeUrl}" scrolling="no" data-id="{strategyId}" data-downLoad = '{downLoadGoldminer}'></iframe>
			<div class="clone-strategy-btn"><img src="{config.relative_path}/images/goldminer-clone.png"/>克隆策略</div>
		</div>
		<div class="row visible-xs-block text-center">
			<img src="{iframeThumb}"/>
		</div>
		<!-- ENDIF iframeUrl -->
		<div class="topic-menu row col-lg-12 col-md-12 col-sm-12 <!-- IF posts.deleted -->deleted<!-- ENDIF posts.deleted -->"  component="topic/manage" <!-- IMPORT partials/data/topic.tpl --> data-tid="{tid}">
			<div class="collection" data-pid = "{mainPid}"<!-- IMPORT partials/data/topic.tpl -->><a component="post/bookmark" role="menuitem" tabindex="-1" href="#" data-bookmarked="{bookmarked}" id="bookmarkedTopic" ><span class="goldminerimg g-collect <!-- IF bookmarked -->hidden<!-- ENDIF bookmarked -->" component="post/bookmark/off"></span><span class="goldminerimg g-has-collect <!-- IF !bookmarked -->hidden<!-- ENDIF !bookmarked -->" component="post/bookmark/on" ></span></a>[[topic:collect]]</div><div class="shareicons">[[topic:share]]<span class="goldminerimg g-wx" id="g-wx" data-url="{originUrl}"><div class="g-qrcode-banner"><span id="g-qrcode"></span><span class="triangle_border_down"></span><div class="triangle_border_down"><span></span></div></div></span><a href="{shareWB}" target="_blank"><span class="goldminerimg g-wb"></span></a><a href="{shareXQ}" target="_blank"><span class="goldminerimg g-xq"></span></a></div>
			<!-- IF deleted -->
		<span class="deleteReply">该帖子已被用户删除，可以恢复或完全清除</span>
		<!-- ENDIF deleted -->
			<!-- IF privileges.isAdminOrMod -->
			<div class="topic-handle">
		<!-- IMPORT partials/thread_tools.tpl -->
		
		</div>
		<!-- ENDIF privileges.isAdminOrMod -->
		</div>
		<div class="rely-quick row col-lg-12 col-md-12 col-sm-12">
			<!-- IMPORT partials/topic/reply-button.tpl -->	
		</div>
		
		<div class="post-list row col-lg-12 col-md-12 col-sm-12">
			<!-- IF postcount -->
			<div class="postcount">{postcount}条回复</div>
			<!-- ELSE -->
			<div class="postcount">暂无回复</div>
			<!-- ENDIF postcount -->
			<ul component="topic" class="posts" data-tid="{tid}" data-cid="{cid}">
			<!-- BEGIN posts -->
				<li component="post" class="<!-- IF posts.deleted --><!-- IF !privileges.isAdminOrMod -->deleted<!-- ENDIF !privileges.isAdminOrMod --><!-- ENDIF posts.deleted -->" <!-- IMPORT partials/data/topic.tpl -->>
					<a component="post/anchor" data-index="{posts.index}" id="{posts.index}"></a>

					<meta itemprop="datePublished" content="{posts.timestampISO}">
					<meta itemprop="dateModified" content="{posts.editedISO}">
					<!-- IF privileges.isAdminOrMod --><!-- IF posts.deleted --><span class="deleteReply">该回复已被用户删除，可以恢复或者永久删除</span><!-- ENDIF posts.deleted -->
					<!-- ENDIF privileges.isAdminOrMod -->
					<!-- IMPORT partials/topic/post.tpl -->
					<!-- IF !posts.index -->
					<div class="post-bar-placeholder"></div>
					<!-- ENDIF !posts.index -->
				</li>
			<!-- END posts -->
			</ul>
			<!-- IF config.usePagination -->
				<!-- IMPORT partials/paginator.tpl -->
			<!-- ENDIF config.usePagination -->
		</div>
		<div class="reply-container row col-lg-12 col-md-12 col-sm-12"></div>
	</div>
	<div class="container-fluid g-side-bar col-lg-3 col-md-4 hidden-xs hidden-sm">
		<div class="row text-center">
			<!-- IF getUserData.picture -->
			<a href="{config.relative_path}/user/{getUserData.myquantid}"><img component="user/picture" data-uid="{getUserData.uid}" src="{getUserData.picture}" class="user-img" /></a>
			<!-- ELSE -->
			<a href="{config.relative_path}/user/{getUserData.myquantid}"><img src="{config.relative_path}/images/user-icon.png"/></a>
			<!-- ENDIF getUserData.picture -->
		</div>
		<div class="text-center username">{getUserData.username}</div>
		<div class="row msgcount">
			<span class="col-lg-4 col-md-4 text-center"><a href="{config.relative_path}/user/{getUserData.myquantid}">{getUserData.topiccount}</a></span><span class="col-lg-4 col-md-4 text-center"><a href="{config.relative_path}/user/{getUserData.myquantid}/posts">{getUserData.replycount}</a></span><span class="col-lg-4 col-md-4 text-center"><a href="{config.relative_path}/user/{getUserData.myquantid}/bookmarks">{getUserData.bookmark}</a></span>
		</div>
		<div class="row msgname">
			<span class="col-lg-4 col-md-4 text-center" ><!-- IF isUser --><a href="/ucenter/topic">[[category:my_topic]]</a><!-- ELSE --><a href="{config.relative_path}/user/{getUserData.myquantid}">他的话题</a><!-- ENDIF isUser --></span>
			<span class="col-lg-4 col-md-4 text-center"><!-- IF isUser --><a href="/ucenter/reply">[[category:my_reply]]</a><!-- ELSE --><a href="{config.relative_path}/user/{getUserData.myquantid}/posts">他的回复</a><!-- ENDIF isUser --></span>
			<span class="col-lg-4 col-md-4 text-center"><!-- IF isUser --><a href="/ucenter/collection">[[category:my_book]]</a><!-- ELSE --><a href="{config.relative_path}/user/{getUserData.myquantid}/bookmarks">他的收藏</a><!-- ENDIF isUser --></span>
		</div>
		<div class="clearfix">
				<!-- IF privileges.topics:create -->
				<a href="{config.relative_path}/compose?cid={cid}" component="category/post" class="btn btn-primary col-lg-12 col-md-12 new-topic-btn" role="button" target="_blank">[[category:new_topic_button]]</a>
				<!-- ELSE -->
				<!-- IF !loggedIn -->
				<a component="category/post/guest" href="{domainLogin}?redirect={originUrl}" class="btn btn-primary col-lg-12 col-md-12 new-topic-btn">[[category:guest-login-post]]</a>
				<!-- ENDIF !loggedIn -->
				<!-- ENDIF privileges.topics:create -->
		</div>
	</div>
</div>
<div widget-area="footer">
	<!-- BEGIN widgets.footer -->
	{{widgets.footer.html}}
	<!-- END widgets.footer -->
</div>

<!-- IF !config.usePagination -->
<noscript>
	<!-- IMPORT partials/paginator.tpl -->
</noscript>
<!-- ENDIF !config.usePagination -->
