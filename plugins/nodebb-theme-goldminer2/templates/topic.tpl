<div widget-area="header">
	<!-- BEGIN widgets.header -->
	{{widgets.header.html}}
	<!-- END widgets.header -->
</div>
<div class="row topic" <!-- IF searchQuery -->data-search="{searchQuery}"<!-- ENDIF searchQuery -->>
	<div class="suspension row col-lg-12 col-md-12 col-sm-12 <!-- IF posts.deleted -->deleted<!-- ENDIF posts.deleted -->" component="topic/manage"
	 <!-- IMPORT partials/data/topic.tpl --> data-tid="{tid}">
	 <div class="suspension-container">
	 	<p class="title">掘金社区</p>
	 	<div class="nav-choose">
			<span class="choose all-category" style="padding-left:26px;">
				<i class="goldminerimg2 g-all"></i>
				<a href="/">所有</a>
			</span>
			<span>
				<i class="goldminerimg2 g-new"></i>
				<a href="/recent">最新</a>
			</span>
			<span>
				<i class="goldminerimg2 g-hot"></i>
				<a href="/popular">热门</a>
			</span>
		</div>
	 </div>
	</div>
	<!--<div class="topic-banner">
	</div>-->
	<div class="topic-banner-container" style="display:none">
		<p class="publish-msg">{getUserData.username} 发表在 
			<span class="classify">{classify} </span>{releaseT}</p>
		<p class="topic-title">{title}</p>
		<div class="publish-user hidden-xs">
			<!-- IF getUserData.picture -->
			<a href="{config.relative_path}/user/{getUserData.myquantid}">
				<img component="user/picture" data-uid="{getUserData.uid}" src="{getUserData.picture}" class="user-img" /><!-- IF getUserData.isOfficial --><i class="goldminerimg2 g-official"></i><!-- ENDIF getUserData.isOfficial -->
			</a>
			<!-- ELSE -->
			<a href="{config.relative_path}/user/{getUserData.myquantid}">
				<img src="{config.relative_path}/images/user-icon.png" class="user-img"/><!-- IF getUserData.isOfficial --><i class="goldminerimg2 g-official"></i><!-- ENDIF getUserData.isOfficial -->
			</a>
			<!-- ENDIF getUserData.picture -->
			{getUserData.username}<!-- IF getUserData.isOfficial --> <span class="official-tips-group">【 {getUserData.groupName} 】</span><!-- ENDIF getUserData.isOfficial -->
		</div>
	</div>
	<div class="topic-new-container">
		<div class="topic-new-container-left">
			<div class="topic-new-container-header">
				<p class="topic-title">{title}<i class="hasPinned <!-- IF !pinned -->hide<!-- ENDIF !pinned -->">[[topic:pinned]]</i>
					<i class="hasHighLighted <!-- IF !highlighted -->hide<!-- ENDIF !highlighted -->">[[topic:highlighted]]</i></p>
				<p class="publish-msg">
					<!-- IF getUserData.picture -->
					<a href="{config.relative_path}/user/{getUserData.myquantid}">
						<img component="user/picture" data-uid="{getUserData.uid}" src="{getUserData.picture}" class="user-img" /><!-- IF getUserData.isOfficial --><i class="goldminerimg2 g-official"></i><!-- ENDIF getUserData.isOfficial -->
					</a>
					<!-- ELSE -->
					<a href="{config.relative_path}/user/{getUserData.myquantid}">
						<img src="{config.relative_path}/images/user-icon.png" /><!-- IF getUserData.isOfficial --><i class="goldminerimg2 g-official"></i><!-- ENDIF getUserData.isOfficial -->
					</a>
					<!-- ENDIF getUserData.picture -->
				{getUserData.username}<!-- IF getUserData.isOfficial --> <span class="official-tips-group">【 {getUserData.groupName} 】</span><!-- ENDIF getUserData.isOfficial -->
				发表在<span class="classify">{classify} </span>{releaseT}</p>
				<div class="topic-add">
					<span class="classify">{classify}</span>
					<div class="topic-add-right">
						<div class="row topic-add-right-view">
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center"><span class="goldminerimg2 g-view"></span>
							<span class="viewcount">{viewcount}</span>
							</div>
							<div class="col-lg-4 col-md-4 text-center col-sm-4 col-xs-4 "><span class="goldminerimg2 g-replyicon"></span>
								<span class="postcount">{postcount}</span>
							</div>
							<div class="col-lg-4 text-center col-md-4 col-sm-4 col-xs-4 "><span class="goldminerimg2 g-bookmarked"></span>
								<span class="upvotes">
									<!-- IF bookmarks -->{bookmarks}
									<!-- ELSE -->0
									<!-- ENDIF bookmarks -->
								</span>
							</div>
						</div>
						<div class="collection" data-pid="{mainPid}" <!-- IMPORT partials/data/topic.tpl -->>
							<a component="post/bookmark" role="menuitem" tabindex="-1" href="#" data-bookmarked="{bookmarked}" id="bookmarkedTopic">
							<span class="collection-box <!-- IF bookmarked -->hidden<!-- ENDIF bookmarked -->" component="post/bookmark/off">收藏</span>
							<span class="collection-box <!-- IF !bookmarked -->hidden<!-- ENDIF !bookmarked -->" component="post/bookmark/on">取消收藏</span>
							</a>
						</div>
						<div class="shareicons">本文分享到:
							<span class="goldminerimg g-wx" id="g-wx" data-url="{originUrl}">
								<div class="g-qrcode-banner">
									<span id="g-qrcode"></span>
									<span class="triangle_border_down"></span>
									<div class="triangle_border_down">
										<span></span>
									</div>
								</div>
							</span>
							<a href="{shareWB}" target="_blank">
								<span class="goldminerimg g-wb"></span>
							</a>
							<a href="{shareXQ}" target="_blank">
								<span class="goldminerimg g-xq"></span>
							</a>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			
			<div class="topic-extra-right">
				<!-- IF selfPost -->
				<span class="row" data-pid="{mainPid}" <!-- IMPORT partials/data/topic.tpl -->>
					<span class="s-edit">
						<span class="goldminerimg g-edit"></span>
						<a component="topic/edit" role="menuitem" tabindex="-1" href="#" style="font-size:14px">[[topic:edit]]</a>
					</span>
					<span>
						<span class="goldminerimg g-delete"></span>
						<a component="topic/user/delete" role="menuitem" tabindex="-1" href="#" style="font-size:14px;color:#ccc;font-weight:normal">[[topic:delete]]</a>
						<span></span>
						<!-- ENDIF selfPost -->
			</div>
			<!-- IF deleted -->
			<span class="deleteReply">该帖子已被用户删除，可以恢复或完全清除</span>
			<!-- ENDIF deleted -->
			<div class="topic-container row col-lg-12 col-md-12 col-sm-12">
				{content}
			</div>
			<!-- IF iframeUrl -->
			<div class="iframe-container row col-lg-12 col-md-12 col-sm-12 hidden-xs">
				<iframe src="{iframeUrl}" scrolling="no" data-id="{strategyId}" data-downLoad='{downLoadGoldminer}'></iframe>
				<div class="clone-strategy-btn">
					<img src="{config.relative_path}/images/goldminer-clone.png" />克隆策略</div>
			</div>
			<div class="row visible-xs-block text-center">
				<img src="{iframeThumb}" />
			</div>
			<!-- ENDIF iframeUrl -->
			<div class="bottom-banner hidden-xs" id="bottombanner" data-url="{offical}"></div>
			<!-- IF hasLogin -->
			<div class="reply-container row col-lg-12 col-md-12 col-sm-12"></div>
			<!-- ELSE -->
			<a href="{domainLogin}?redirect={originUrl}" class="login-to-reply">登录后回复</a>
			<!-- ENDIF hasLogin -->
			<div class="post-list row col-lg-12 col-md-12 col-sm-12">
				<!-- IF privileges.isAdminOrMod -->
				<div class="topic-handle">
					<!-- IMPORT partials/thread_tools.tpl -->

				</div>
				<!-- ENDIF privileges.isAdminOrMod -->
				<!-- IF postcount -->
				<div class="postcount">
					<i class="countline"></i>
					</span>
					<span>评论: {postcount}</span>
				</div>
				<!-- ELSE -->
				<div class="postcount">
					<i class="countline"></i>
					</span>
					<span>暂无评论</span>
				</div>
				<!-- ENDIF postcount -->
				<ul component="topic" class="posts" data-tid="{tid}" data-cid="{cid}">
					<!-- BEGIN posts -->
					<li component="post" class="<!-- IF posts.deleted --><!-- IF !privileges.isAdminOrMod -->deleted<!-- ENDIF !privileges.isAdminOrMod --><!-- ENDIF posts.deleted -->"
					<!-- IMPORT partials/data/topic.tpl -->>
						<a component="post/anchor" data-index="{posts.index}" id="{posts.index}"></a>

						<meta itemprop="datePublished" content="{posts.timestampISO}">
						<meta itemprop="dateModified" content="{posts.editedISO}">
						<!-- IF privileges.isAdminOrMod -->
						<!-- IF posts.deleted -->
						<span class="deleteReply">该回复已被用户删除，可以恢复或者永久删除</span>
						<!-- ENDIF posts.deleted -->
						<!-- ENDIF privileges.isAdminOrMod -->
						<!-- IMPORT partials/topic/post.tpl -->
						<!-- IF !posts.index -->
						<div class="post-bar-placeholder"></div>
						<!-- ENDIF !posts.index -->
					</li>
					<!-- END posts -->
				</ul>
				<!-- IF config.usePagination -->
				<!-- IMPORT partials/paginator.tpl -->
				<!-- ENDIF config.usePagination -->
				<div class="relative-topics">
					<!-- IF relativeTopic -->
					<span class="title">相关阅读</span>
					<!-- ENDIF relativeTopic -->
					<!-- BEGIN relativeTopic -->
					<li><span class="relativeTid">{relativeTopic.cidname}</span><a href="{config.relative_path}/topic/{relativeTopic.tid}">{relativeTopic.title}</a></li>
					<!-- END relativeTopic -->
				</div>
			</div>
		</div>
		<div class="topic-new-container-right hidden-xs hidden-sm">
			<div class="topic-right-user">
				<span class="topic-right-user_img">
					<!-- IF getUserData.picture -->
					<a href="{config.relative_path}/user/{getUserData.myquantid}">
						<img component="user/picture" data-uid="{getUserData.uid}" src="{getUserData.picture}" class="user-img" /><!-- IF getUserData.isOfficial --><i class="goldminerimg2 g-official"></i><!-- ENDIF getUserData.isOfficial -->
					</a>
					<!-- ELSE -->
					<a href="{config.relative_path}/user/{getUserData.myquantid}">
						<img src="{config.relative_path}/images/user-icon.png" class="user-img"/><!-- IF getUserData.isOfficial --><i class="goldminerimg2 g-official"></i><!-- ENDIF getUserData.isOfficial -->
					</a>
					<!-- ENDIF getUserData.picture -->
				</span>
				<span class="topic-right-user_name">
					<p>{getUserData.username}</p>
					<!-- IF getUserData.isOfficial --> <span class="official-tips-group">{getUserData.groupName}</span><!-- ENDIF getUserData.isOfficial -->
				</span>
				<span class="topic-right-user_bookmark">
					<!-- IF !isUser -->
					<span class="operate-follow">
					<!-- IF !hasLogin -->
					<a href="{domainLogin}?redirect={originUrl}" class="logintofollow">
						登录后关注
					</a>
					<!-- ELSE -->
					<button type="button" data-uid="{getUserData.uid}" data-username = "{getUserData.username}" data-ln = "{hasLogin}" class="btn-morph fab btn btn-primary <!-- IF hasFollowing -->plus heart<!-- ENDIF isFollowing -->">
						<!-- IF !hasFollowing -->[[user:following]]
						<!-- ELSE -->[[user:watched]]
						<!-- ENDIF !hasFollowing -->
					</button>
					<!-- ENDIF !hasLogin -->
				</span>
		<!-- ENDIF !isSelf -->
				</span>
			</div>
			<div class="hotlabel">
				<!-- IF allCategoriesData -->
				<p><i class="hotTopic-ico"></i>热门标签</p>
				<!-- ENDIF allCategoriesData -->
				<!-- BEGIN allCategoriesData -->
				<li><a href="{config.relative_path}/category/{allCategoriesData.cid}">{allCategoriesData.name}</a></li>
				<!-- END allCategoriesData -->
			</div>
			<div class="hotTopic">
				<p class="title"><i class="hotTopic-ico"></i>热门推荐</p>
				<ol>
				<!-- BEGIN monthlyPopular -->
				<li><a href="{config.relative_path}/topic/{monthlyPopular.tid}" target="_blank">{monthlyPopular.title}</a></li>
				<!-- END monthlyPopular -->
				</ol>
			</div>
			<div class="row advert" id="advert" data-url="{offical}">
			</div>
		</div>
	</div>
</div>
<div widget-area="footer">
	<!-- BEGIN widgets.footer -->
	{{widgets.footer.html}}
	<!-- END widgets.footer -->
</div>

<!-- IF !config.usePagination -->
<noscript>
	<!-- IMPORT partials/paginator.tpl -->
</noscript>
<!-- ENDIF !config.usePagination -->