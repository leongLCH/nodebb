<div class="cover" component="account/cover" <!-- IF toIframe -->style="top:0;"
	<!-- ENDIF toIframe -->>
</div>
<div class="container" style="position:absolute;<!-- IF toIframe -->top:0;<!-- ELSE -->top:35px;<!-- ENDIF toIframe -->" component="account/cover">
	<div class="account-img">	
		<div class="account-img-container"><!-- IF picture -->
		<img component="user/picture" data-uid="{uid}" src="{picture}" class="user-img" />
		<!-- ELSE -->
		<img src="{config.relative_path}/images/user-icon.png" />
		<!-- ENDIF picture --><!-- IF isOfficial --><i class="goldminerimg2 g-official"></i><!-- ENDIF isOfficial --></div>

	</div>
	<div class="user-detail">
		<span class="username">{username}<!-- IF isOfficial --> <span class="official-tips-group">【 {groupName} 】</span><!-- ENDIF isOfficial --></span>
		<!-- IF loggedIn -->

		<!-- IF !isSelf -->
		<span class="operate-follow">
			<!-- <button type="button" class="btn btn-default" component="account/chat" href="#">[[user:chat]]</button> -->
			<button type="button" class="btn-morph fab btn btn-primary <!-- IF isFollowing -->plus heart<!-- ENDIF isFollowing -->">
				<!-- IF !isFollowing -->[[user:following]]
				<!-- ELSE -->[[user:watched]]
				<!-- ENDIF !isFollowing -->
			</button>
		</span>
		<!-- ENDIF !isSelf -->
		<!-- ENDIF loggedIn -->
	</div>
	<div class="personal">
		<!-- IF aboutme -->
		{aboutme}
		<!-- ELSE -->
		<p class="description" style="font-size:24px;
		color:#fff;">[[user:info.has-no-description]]</p>
		<!-- ENDIF aboutme -->

	</div>
	<!-- IF isAdmin -->
	<!-- IMPORT partials/account/menu.tpl -->
	<!-- ENDIF isAdmin -->
</div>
	<ul class="nav nav-pills category-nav col-lg-12 col-md-12 col-sm-12 hidden-xs" <!-- IF toIframe -->style="top:258px;"
		<!-- ENDIF toIframe -->>
		<!-- BEGIN nav -->
		<li <!-- IF nav.css -->class="haschoose"
			<!-- ENDIF nav.css -->>
			<a href="{config.relative_path}/user/{myquantid}{nav.slug}">{nav.name}</a>
		</li>
		<!-- END nav -->
	</ul>

<nav class="navbar nav-container navbar-default nav-phone visible-xs" >
		<div class="navbar-header visible-xs-block" <!-- IF toIframe -->style="margin-top:2px;"
		<!-- ENDIF toIframe -->>
			<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target="#bbs-account-nav">
				<span class="sr-only">切换导航</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<!-- BEGIN nav -->
			<a class="navbar-brand <!-- IF !nav.css --> hidden <!-- ENDIF !nav.css -->" href="{config.relative_path}/user/{myquantid}{nav.slug}" >{nav.name}</a>
			<!-- END nav -->
		</div>
		<div class="collapse navbar-collapse" id="bbs-account-nav">
			<ul class="nav nav-pills category-nav col-lg-12 col-md-12 col-sm-12" <!-- IF toIframe -->style="top:258px;"
				<!-- ENDIF toIframe -->>
				<!-- BEGIN nav -->
				<li <!-- IF nav.css -->class="haschoose"
					<!-- ENDIF nav.css -->>
					<a href="{config.relative_path}/user/{myquantid}{nav.slug}">{nav.name}</a>
				</li>
				<!-- END nav -->
			</ul>
		</div>
	</nav>