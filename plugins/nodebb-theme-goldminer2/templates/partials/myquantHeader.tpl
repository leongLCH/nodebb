<div class="container-fluid myquantHeader" id="header">
	<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#example-navbar-collapse">
					<span class="sr-only">切换导航</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="{myquantUrl}" title="掘金量化">
					<img src="{config.relative_path}/images/logo.png" class="visible-xs" alt="掘金量化">
					<img src="{config.relative_path}/images/logo.png" class="hidden-xs" alt="掘金量化">
				</a>
			</div>
			<div class="collapse navbar-collapse" id="example-navbar-collapse">
				<ul class="nav navbar-nav nav-line hidden-xs">
					<li>|</li>
				</ul>
				<ul class="nav navbar-nav nav-main">
					<li class="active">
						<a href="{myquantUrl}" title="掘金3">掘金3</a>
					</li>
					<li>
						<a href="{myquantUrl}/service" title="产品服务">产品服务</a>
					</li>
					<li><a href="https://sim.myquant.cn/sim/home" title="仿真交易" target="_blank">仿真交易 <img src="https://www.myquant.cn/static/images/new.png" alt="new"></a></li>
					<li>
						<a href="https://www.myquant.cn/apply-real-trading" title="实盘申请">实盘申请</a>
					</li>
					<li class="hidden-xs nav-hover"><a href="#"> 绩效分析 <b class="caret"></b></a>
						<div class="text-center caret-reverse">
							<b></b>
						</div>
						<ul class="hover-menu">
							<li><a href="{myquantUrl}/analyse" title="分析首页">分析首页</a></li>
							<li><a href="{myquantUrl}/analyse/report" title="我的分析">我的分析</a></li>
						</ul>
					</li>
					<li>
						<a href="#" title="社区" id="community">社区</a>
					</li>
					<li class="dropdown visible-xs"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><span> 帮助中心 <b class="caret"></b></span></a>
						<ul class="dropdown-menu">
							<li><a href="{myquantUrl}/docs/guide" title="快速开始">快速开始</a></li>
							<li><a href="javascript:void(0);" class="highlight show-sub-menu" title="API文档">API文档</a><b class="caret show-sub-menu"></b>
								<ul class="sub-menu">
									<li><a href="{myquantUrl}/docs/python" title="Python">Python</a></li>
									<li><a href="{myquantUrl}/docs/matlab" title="Matlab">Matlab</a></li>
									<li><a href="{myquantUrl}/docs/cpp" title="C++">C++</a></li>
									<li><a href="{myquantUrl}/docs/csharp" title="C#">C#</a></li>
								</ul>
							</li>
							<li><a class="disabled" href="{myquantUrl}/docs/data" title="数据文档">数据文档</a></li>
							<li><a class="disabled" href="{myquantUrl}/docs/python_strategyies" title="经典策略">经典策略</a></li>
							<li><a class="disabled" href="{myquantUrl}/docs/subject" title="专题介绍">专题介绍</a></li>
							<li><a class="disabled" href="{myquantUrl}/docs/gm3_faq" title="常见问题">常见问题</a></li>
							<li><a class="disabled" href="{myquantUrl}/docs/logs" title="升级日志">升级日志</a></li>
						</ul>
					</li>
					<li class="show-in-computer hidden-xs"><a href="#"><span> 帮助中心 <b class="caret"></b></span></a>
						<div class="text-center caret-reverse">
							<b></b>
						</div>
						<div class="computer-menus">
							<ul>
								<li><a href="{myquantUrl}/docs/guide" title="新手指引">新手指引</a></li>
								<li><a href="{myquantUrl}/docs/guide/35#32961c39feb7af92" title="获取终端">获取终端</a></li>
								<li><a href="{myquantUrl}/docs/guide/35#9eaf32451fc2d41d" title="推介配置">推介配置</a></li>
								<li><a href="{myquantUrl}/docs/guide/35#43718ac9377b2dc1" title="获取帮助&支持">获取帮助&支持</a></li>
								<li><a href="{myquantUrl}/docs/guide/35#a0e2f0be08fe7317" title="安装SDK（什么是策略SDK？）">安装SDK</a></li>
								<li><a href="{myquantUrl}/docs/guide/35#e7b7cf96061b4dab" title="新建策略（两个身份ID很重要！）">新建策略</a></li>
								<li><a href="{myquantUrl}/docs/guide/35#76f34cdef1d30ac0" title="策略本地编写与回测（什么是策略&回测？）">策略本地编写与回测</a></li>
								<li><a href="{myquantUrl}/docs/guide/35#db3d25d1213e66bc" title="实盘交易（五年实盘保障）">实盘交易</a></li>
								<li><a href="{myquantUrl}/docs/guide/35#25959952c2a99861" title="扫单策略">扫单策略</a></li>
								<li><a href="{myquantUrl}/docs/guide/672" title="策略指引">策略指引</a></li>
							</ul>
							<ul>
								<li><a href="{myquantUrl}/docs/python" title="Python API 文档">Python SDK 文档</a></li>
								<li><a href="{myquantUrl}/docs/python" title="快速创建我的策略 ">快速创建我的策略</a></li>
								<li><a href="{myquantUrl}/docs/python/152" title="策略程序架构">策略程序架构</a></li>
								<li><a href="{myquantUrl}/docs/python/45" title="重要概念">重要概念</a></li>
								<li><a href="{myquantUrl}/docs/python/47" title="数据结构">数据结构</a></li>
								<li><a href="{myquantUrl}/docs/python/43" title="API 介绍">API 介绍</a></li>
								<li><a href="{myquantUrl}/docs/python/50" title="枚举常量">枚举常量</a></li>
								<li><a href="{myquantUrl}/docs/python/71" title="错误码">错误码</a></li>
							</ul>
							<ul>
								<li><a href="{myquantUrl}/docs/matlab" title="Matlab SDK 文档">Matlab SDK 文档</a></li>
								<li><a href="{myquantUrl}/docs/matlab/301" title="matlab策略SDK概述">matlab策略SDK概述</a></li>
								<li><a href="{myquantUrl}/docs/matlab/302" title="典型策略场景">典型策略场景</a></li>
								<li><a href="{myquantUrl}/docs/matlab/303" title="策略文件">策略文件</a></li>
								<li><a href="{myquantUrl}/docs/matlab/304" title="策略结构">策略结构</a></li>
								<li><a href="{myquantUrl}/docs/matlab/305" title="获取数据">获取数据</a></li>
								<li><a href="{myquantUrl}/docs/matlab/306" title="策略交易">策略交易</a></li>
								<li><a href="{myquantUrl}/docs/matlab/307" title="枚举常量">枚举常量</a></li>
								<li><a href="{myquantUrl}/docs/matlab/308" title="错误码">错误码</a></li>
							</ul>
							<ul>
								<li><a href="{myquantUrl}/docs/cpp" title="C++ SDK文档">C++ SDK文档</a></li>
								<li><a href="{myquantUrl}/docs/cpp/177" title="快速开始">快速开始</a></li>
								<li><a href="{myquantUrl}/docs/cpp/171" title="重要概念">重要概念</a></li>
								<li><a href="{myquantUrl}/docs/cpp/192" title="策略基类">策略基类</a></li>
								<li><a href="{myquantUrl}/docs/cpp/174" title="数据查询函数">数据查询函数</a></li>
								<li><a href="{myquantUrl}/docs/cpp/183" title="结果集合类">结果集合类</a></li>
								<li><a href="{myquantUrl}/docs/cpp/186" title="结果数组类">结果数组类</a></li>
								<li><a href="{myquantUrl}/docs/cpp/180" title="数据结构">数据结构</a></li>
								<li><a href="{myquantUrl}/docs/cpp/172" title="枚举常量">枚举常量</a></li>
								<li><a href="{myquantUrl}/docs/cpp/170" title="错误码">错误码</a></li>
							</ul>
							<ul>
								<li><a href="{myquantUrl}/docs/csharp" title="C# SDK文档">C# SDK文档</a></li>
								<li><a href="{myquantUrl}/docs/csharp/197" title="快速开始">快速开始</a></li>
								<li><a href="{myquantUrl}/docs/csharp/210" title="重要概念">重要概念</a></li>
								<li><a href="{myquantUrl}/docs/csharp/206" title="策略基类">策略基类</a></li>
								<li><a href="{myquantUrl}/docs/csharp/198" title="数据查询函数">数据查询函数</a></li>
								<li><a href="{myquantUrl}/docs/csharp/209" title="结果集合类">结果集合类</a></li>
								<li><a href="{myquantUrl}/docs/csharp/200" title="数据结构">数据结构</a></li>
								<li><a href="{myquantUrl}/docs/csharp/201" title="枚举常量">枚举常量</a></li>
								<li><a href="{myquantUrl}/docs/csharp/211" title="错误码">错误码</a></li>
							</ul>
							<ul>
								<li><a href="{myquantUrl}/docs/data" title="数据文档">数据文档</a></li>
								<li><a href="{myquantUrl}/docs/data/98" title="基础数据">基础数据</a></li>
								<li><a href="{myquantUrl}/docs/data/86" title="股票数据">股票数据</a></li>
								<li><a href="{myquantUrl}/docs/data/88" title="期货数据">期货数据</a></li>
								<li><a href="{myquantUrl}/docs/data/90" title="财务数据">财务数据</a></li>
								<li><a href="{myquantUrl}/docs/data/81" title="指数数据">指数数据</a></li>
								<li><a href="{myquantUrl}/docs/data/96" title="行业概念数据">行业概念数据</a></li>
								<li><a href="{myquantUrl}/docs/data/84" title="交易日数据">交易日数据</a></li>
							</ul>
							<ul>
								<li><a href="{myquantUrl}/docs/python_strategyies" title="经典策略">经典策略</a></li>
								<li><a href="{myquantUrl}/docs/python_strategyies/153" title="双均线策略(期货) ">双均线策略(期货) </a></li>
								<li><a href="{myquantUrl}/docs/python_strategyies/101" title="alpha对冲(股票+期货)">alpha对冲(股票+期货)</a></li>
								<li><a href="{myquantUrl}/docs/python_strategyies/103" title="多因子选股(股票)">多因子选股(股票)</a></li>
								<li><a href="{myquantUrl}/docs/python_strategyies/104" title="网格交易(期货)">网格交易(期货)</a></li>
								<li><a href="{myquantUrl}/docs/python_strategyies/105" title="指数增强(股票)">指数增强(股票)</a></li>
								<li><a href="{myquantUrl}/docs/python_strategyies/106" title="跨品种套利(期货)">跨品种套利(期货)</a></li>
								<li><a href="{myquantUrl}/docs/python_strategyies/107" title="跨期套利(期货)">跨期套利(期货)</a></li>
								<li><a href="{myquantUrl}/docs/python_strategyies/108" title="日内回转交易(股票)">日内回转交易(股票)</a></li>
							</ul>
							<ul>
								<li><a href="{myquantUrl}/docs/gm3_faq" title="常见问题">常见问题</a></li>
								<li><a href="{myquantUrl}/docs/gm3_faq/154#8638aa5349035660" title="技术支持服务信息">技术支持服务信息</a></li>
								<li><a href="{myquantUrl}/docs/gm3_faq/154#92bfb72b10dd7504" title="终端问题">终端问题</a></li>
								<li><a href="{myquantUrl}/docs/gm3_faq/154#dea2c9a50975bae2" title="python 编程问题">python 编程问题</a></li>
								<li><a href="{myquantUrl}/docs/gm3_faq/154#0e65774921d74bb7" title="SDK 问题">SDK 问题</a></li>
								<li><a href="{myquantUrl}/docs/gm3_faq/154#71dade7ab5421f32" title="回测问题">数据问题</a></li>
								<li><a href="{myquantUrl}/docs/gm3_faq/154#9639505ce6158918" title="回测问题">仿真&amp;实盘问题</a></li>
								<li><a href="{myquantUrl}/docs/gm3_faq/154#9229cf3294353bc5" title="回测问题">回测问题</a></li>
							</ul>
							<ul>
								<li><a href="{myquantUrl}/docs/subject" title="量化专题">量化专题</a></li>
								<li><a href="{myquantUrl}/docs/subject/scanner_v2" title="文件单(新版)">文件单(新版)</a></li>
								<li><a href="{myquantUrl}/docs/subject/statistics" title="交易归档">交易归档</a></li>
								<li><a href="{myquantUrl}/docs/subject/194" title="文件单(旧版)">文件单(旧版)</a></li>
								<li><a href="{myquantUrl}/docs/subject/algo_trade1" title="算法交易">算法交易</a></li>
								<li><a href="{myquantUrl}/docs/subject/L2data" title="L2 数据">L2 数据</a></li>
							</ul>
							<ul>
								<li><a href="{myquantUrl}javascript:void(0);" title="综合其他">综合其他</a></li>
								<li><a href="{myquantUrl}/docs/logs" title="终端升级日志">终端升级日志</a></li>
								<li><a href="{myquantUrl}/docs/downloads" title="终端升级日志">各语言 SDK 下载</a></li>
								<li><a href="{myquantUrl}/docs/markdown" title="Markdown语法快速入门 ">Markdown语法快速入门 </a></li>
							</ul>
						</div>
					</li>
				</ul>
				<ul class="nav navbar-nav nav-line hidden-xs">
					<li>|</li>
				</ul>
				<!-- IF user.uid -->
				<ul class="nav navbar-nav navbar-right">
					<li class="visible-xs nav-dropdown dropdown ">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<span>
								<!-- IF user.picture -->
								<img component="user/picture" src="{user.picture}" class="nav-avatar" /><!-- IF user.isOfficial --><i class="goldminerimg2 g-official"></i><!-- ENDIF user.isOfficial -->
								<!-- ELSE -->
								<img src="{config.relative_path}/images/user-icon.png" class="nav-avatar"/><!-- IF user.isOfficial --><i class="goldminerimg2 g-official"></i><!-- ENDIF user.isOfficial -->
								<!-- ENDIF user.picture -->{user.username}
								<b class="caret"></b>
							</span>
						</a>
						<ul class="dropdown-menu">
							<li>
								<a href="{myquantUrl}/ucenter" title="个人中心">个人中心</a>
							</li>
							<li>
								<a href="{myquantUrl}/ucenter/strategy" title="我的策略">我的策略</a>
							</li>
							<li>
								<a href="{myquantUrl}/ucenter/community" title="我的社区">我的社区</a>
							</li>
							<li>
								<a href="{url}/notifications" title="消息提醒" component="notifications/mark_all">消息提醒</a>
								<span class="noticeBell noticeBellNav hidden" component="notifications/iconbell"></span>
							</li>
							<li component="user/logout">
								<form method="post" action="{relative_path}/logout">
									<input type="hidden" name="_csrf" value="{config.csrf_token}">
									<input type="hidden" name="noscript" value="true">
									<button type="submit" class="btn btn-link" style="padding:7px 0;">
										<span class="logoutBtn">退出登录</span>
									</button>
								</form>
							</li>
						</ul>
					</li>
					<li class="hidden-xs nav-hover">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<span>
								<!-- IF user.picture -->
								<img component="user/picture" src="{user.picture}" class="nav-avatar" /><!-- IF user.isOfficial --><i class="goldminerimg2 g-official"></i><!-- ENDIF user.isOfficial -->
								<!-- ELSE -->
								<img src="{config.relative_path}/images/user-icon.png" class="nav-avatar"/><!-- IF user.isOfficial --><i class="goldminerimg2 g-official"></i><!-- ENDIF user.isOfficial -->
								<!-- ENDIF user.picture -->{user.username}
								<b class="caret"></b>
							</span>
						</a>
						<ul class="hover-menu">
							<li>
								<a href="{myquantUrl}/ucenter" title="个人中心">个人中心</a>
							</li>
							<li>
								<a href="{myquantUrl}/ucenter/strategy" title="我的策略">我的策略</a>
							</li>
							<li>
								<a href="{myquantUrl}/ucenter/community" title="我的社区">我的社区</a>
							</li>
							<li>
								<a href="{url}/notifications" title="消息提醒" component="notifications/mark_all">消息提醒</a>
								<span class="noticeBell noticeBellNav hidden" component="notifications/iconbell"></span>
							</li>
							<li component="user/logout">
								<form method="post" action="{relative_path}/logout">
									<input type="hidden" name="_csrf" value="{config.csrf_token}">
									<input type="hidden" name="noscript" value="true">
									<button type="submit" class="btn btn-link" style="padding:7px 0;">
										<span class="logoutBtn">退出登录</span>
									</button>
								</form>
							</li>
						</ul>
					</li>
				</ul>
				<!-- ELSE -->
				<ul class="nav navbar-nav navbar-right">
					<li class="hidden-sm hidden-xs ">
						<a href="/user/reg" class="nav-reg nav-redirect" title="新用户注册"> 新用户注册</a>
					</li>
					<li class="hidden-xs active">
						<a href="/user/login" class="nav-login nav-redirect" title="登录">
							<span class="fa fa-user hidden-xs"></span> 登录</a>
					</li>
				</ul>
				<!-- ENDIF user.uid -->
			</div>
		</div>
	</nav>
</div>