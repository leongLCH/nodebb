<div class="container-fluid myquantHeader" id="header">
	<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#example-navbar-collapse">
					<span class="sr-only">切换导航</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<button type="button" class="navbar-toggle navbar-toggle-search" data-toggle="collapse" data-target="#example-navbar-search">
					<span class="sr-only">打开搜索</span>
					<span class="fa fa-search"></span>
				</button>
				<a class="navbar-brand" href="{myquantUrl}" title="掘金量化">
					<img src="{config.relative_path}/images/logo.png" class="visible-xs" alt="掘金量化">
					<img src="{config.relative_path}/images/logo.png" class="hidden-xs" alt="掘金量化">
				</a>
			</div>
			<div class="visible-xs">
				<div class="collapse navbar-collapse" id="example-navbar-search">
					<form class="navbar-form navbar-left" action="{search}" role="search">
						<div class="form-group">
							<input type="text" name="keyword" value="" required="required" class="form-control" placeholder="搜索..." type="submit">
						</div>
					</form>
				</div>
			</div>
			<div class="collapse navbar-collapse" id="example-navbar-collapse">
				<ul class="nav navbar-nav">
					<li>
						<a href="{myquantUrl}" title="掘金3">掘金3</a>
					</li>
					<li class="active">
						<a href="#" title="社区" id="community">社区</a>
					</li>
					<li class="dropdown ">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<span> 帮助中心
								<b class="caret"></b>
							</span>
						</a>
						<ul class="dropdown-menu">
							<li>
								<a href="{helpCenter.guide}" title="快速开始">快速开始</a>
							</li>
							<li>
								<a href="javascript:void(0);" class="highlight show-sub-menu" title="API文档">API文档</a>
								<b class="caret show-sub-menu"></b>
								<ul class="sub-menu">
									<li>
										<a href="{helpCenter.python}" title="Python">Python</a>
									</li>
									<li>
										<a href="javascript:void(0);" title="Matlab">Matlab</a>
									</li>
									<li>
										<a href="javascript:void(0);" title="C/C++">C/C++</a>
									</li>
									<li>
										<a href="javascript:void(0);" title="C#">C#</a>
									</li>
								</ul>
							</li>
							<li>
								<a class="disabled" href="{helpCenter.data}" title="数据文档">数据文档</a>
							</li>
							<li>
								<a class="disabled" href="{helpCenter.strategy}" title="经典策略">经典策略</a>
							</li>
							<li>
								<a class="disabled" href="{helpCenter.faq}" title="常见问题">常见问题</a>
							</li>
						</ul>
					</li>
					<li>
						<a href="{joinus}" title="加入我们">加入我们</a>
					</li>
				</ul>
				<form class="navbar-form navbar-left hidden-xs" action="{search}" role="search" method="get">
					<div class="form-group">
						<input type="text" name="keyword" value="" required="required" class="form-control" placeholder="搜索..." width="145px">
						<input type="submit" name="keyword" value="" class="form-control">
						<span class="fa fa-search"></span>
					</div>
				</form>
				<ul class="nav navbar-nav navbar-right">
					<li class="hidden-sm">
						<a href="{gm2}" target="_blank" class="official" title="旧版官网(掘金2)">旧版官网(掘金2)</a>
					</li>
					<!-- IF !user.uid -->
					<li>
						<a href="{login}" title="登录">
							<span class="fa fa-user hidden-xs"></span> 登录</a>
					</li>
					<li class="hidden-sm ">
						<a href="{reg}"> 免费注册</a>
					</li>
					<!-- ELSE -->
					<li class="dropdown " style="padding-top:4px;">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<span>
								<span class="noticeBell hidden" component="notifications/iconbell"></span>
								<!-- IF user.picture -->
								<img component="user/picture" src="{user.picture}" class="user-img" />
								<!-- ELSE -->
								<img src="{config.relative_path}/images/user-icon.png" class="user-img"/>
								<!-- ENDIF user.picture -->{user.username}
								<b class="caret"></b>
							</span>
						</a>
						<ul class="dropdown-menu">
							<li>
								<a href="/ucenter" title="个人中心">个人中心</a>
							</li>
							<li>
								<a href="/ucenter/topic" title="我的社区">我的社区</a>
							</li>
							<li >
								<a href="/notifications" title="消息提醒" component="notifications/mark_all">消息提醒</a><span class="noticeBell noticeBellNav hidden" component="notifications/iconbell"></span>
							</li>
							<li>
								<a href="/ucenter/setting" title="账户设置">账户设置</a>
							</li>
							<li component="user/logout">
								<form method="post" action="{relative_path}/logout">
									<input type="hidden" name="_csrf" value="{config.csrf_token}">
									<input type="hidden" name="noscript" value="true">
									<button type="submit" class="btn btn-link">
										<span class="logoutBtn">退出登录</span>
									</button>
								</form>
							</li>
						</ul>
					</li>
					<!-- ENDIF !user.uid -->
				</ul>
			</div>
		</div>
	</nav>
</div>