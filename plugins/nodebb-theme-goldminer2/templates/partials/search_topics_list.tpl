<ul component="category" class="topic-list" itemscope itemtype="http://www.schema.org/ItemList" data-nextstart="{nextStart}"
 data-set="{set}">
	<meta itemprop="itemListOrder" content="descending">
	<!-- BEGIN posts -->
	<li component="category/topic" class="row clearfix category-item {function.generateTopicClass}" <!-- IMPORT partials/data/category.tpl -->>
		<meta itemprop="name" content="{function.stripTags, title}">
		<!-- IF posts.deleted -->
		<span class="userDelete">该帖子已被用户删除，可以恢复或永久删除</span>
		<!-- ENDIF posts.deleted -->
		<div class="topic-container row col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="user-info">
				<p class="year hidden-xs">{posts.releaseT.year}</p>
				<p class="date hidden-xs">{posts.releaseT.date}</p>
				<div class="avatar-container">
					<a href="<!-- IF posts.isUser -->/ucenter/topic<!-- ELSE -->{config.relative_path}/user/{posts.user.myquantid}<!-- ENDIF posts.isUser -->"
					 target="_blank" title="{posts.user.username}">
						<!-- IF posts.user.picture -->
						<img component="user/picture" data-uid="{posts.user.uid}" src="{posts.user.picture}" class="user-img" />
						<!-- ELSE -->
						<img src="{config.relative_path}/images/user-icon.png" />
						<!-- ENDIF posts.user.picture -->
						<!-- IF posts.isOfficial --><i class="goldminerimg2 g-official"></i><!-- ENDIF posts.isOfficial -->
						<span class="official-tips">{posts.user.username} <!-- IF posts.isOfficial --> <span class="official-tips-group">【 {posts.groupName} 】</span><!-- ENDIF posts.isOfficial --><i class="triangle"></i></span>
					</a>
				</div>

			</div>
			<div class="topic-list-container">
				<div class="date-phone visible-xs">
					<p>{posts.releaseT.year} {posts.releaseT.date}</p>
				</div>
				<div class="title">
					<a href="{config.relative_path}/topic/{posts.topic.slug}<!-- IF posts.topic.bookmark -->/{posts.bookmark}<!-- ENDIF posts.topic.bookmark -->"
					 itemprop="url" target="_blank">{posts.topic.title}</a>
					<i class="hasPinned <!-- IF !posts.topic.pinned -->hide<!-- ENDIF !posts.topic.pinned -->">[[topic:pinned]]</i>
					<i class="hasHighLighted <!-- IF !posts.topic.highlighted -->hide<!-- ENDIF !posts.topic.highlighted -->">[[topic:highlighted]]</i>
				</div>
				<div class="topic-list-detail">
					<div class="detail-left">
						{posts.thumbnail}
						<div class="detail-all-label">
							<span class="detail-label">{posts.category.name}</span>
							<!-- IF posts.topic.strategyId -->
							<span class="detail-label"><i class="goldminerimg2 g-strategy"></i>源码分享</span>
							<!-- ENDIF posts.topic.strategyId -->
							<!-- IF posts.hasIframe -->
							<span class="detail-label label-performance"><i class="goldminerimg2 g-performance"></i>绩效分析<span class="thumb"><img src="{posts.iframeUrl}"/><i class="triangle"></i></span></span>
							<!-- ENDIF posts.hasIframe -->
							
						</div>
					</div>
					<div class="detail-right">
						<div class="detail-reply-msg hidden-xs">
							<!-- IF !posts.unreplied -->
							<a href="<!-- IF posts.topic.teaser.isUser -->/ucenter/topic<!-- ELSE -->{config.relative_path}/user/{posts.topic.teaser.user.myquantid}<!-- ENDIF posts.topic.teaser.isUser -->"
							 target="_blank" class="detail-right-avatar">
							 	<!-- IF posts.topic.teaser.user.picture -->
								<img component="user/picture" data-uid="{posts.topic.teaser.user.myquantid}" src="{posts.topic.teaser.user.picture}" class="user-img" />
								<!-- ELSE -->
								<img src="{config.relative_path}/images/user-icon.png"/>
								<!-- ENDIF posts.topic.teaser.user.picture -->
								<!-- IF posts.topic.teaser.user.isOfficial --><i class="goldminerimg2 g-official"></i><!-- ENDIF posts.topic.teaser.user.isOfficial -->
						<span class="official-tips">{posts.topic.teaser.user.username} <!-- IF posts.topic.teaser.user.isOfficial --> <span class="official-tips-group">【 {posts.topic.teaser.user.groupName} 】</span><!-- ENDIF posts.topic.teaser.user.isOfficial --><i class="triangle"></i></span>
							</a>
							<div class="detail-reply-t">
								<p>最近回复:
									<span class="username"><a href="<!-- IF posts.topic.teaser.isUser -->/ucenter/topic<!-- ELSE -->{config.relative_path}/user/{posts.topic.teaser.user.myquantid}<!-- ENDIF posts.topic.teaser.isUser -->" target="_blank">{posts.topic.teaser.user.username}</a></span>
								</p>
								<p>{posts.topic.teaser.timestamp}</p>
								
							</div>
							<!-- ELSE -->
							<span class="no-reply">暂无回复</span>
							<a href="{config.relative_path}/topic/{posts.topic.slug}<!-- IF topic.bookmark -->/{topic.bookmark}<!-- ENDIF topic.bookmark -->"
								itemprop="url" target="_blank" class="btn btn-primary new-reply hidden-xs">消灭0回复</a>
							<!-- ENDIF !posts.unreplied -->
						</div>
						<div class="relevance col">
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center"><span class="goldminerimg2 g-view"></span>
								<span class="viewcount">{posts.topic.viewcount}</span>
							</div>
							<div class="col-lg-4 col-md-4 text-center col-sm-4 col-xs-4 "><span class="goldminerimg2 g-replyicon"></span>
								<span class="postcount">{posts.topic.postcount}</span>
							</div>
							<div class="col-lg-4 text-center col-md-4 col-sm-4 col-xs-4 "><span class="goldminerimg2 g-bookmarked"></span>
								<span class="upvotes">{posts.bookmarked}</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</li>
	<!-- END posts -->
</ul>