<ul component="category" class="topic-list" itemscope itemtype="http://www.schema.org/ItemList" data-nextstart="{nextStart}"
 data-set="{set}">
	<meta itemprop="itemListOrder" content="descending">
	<!-- BEGIN topics -->
	<li component="category/topic" class="row clearfix category-item {function.generateTopicClass}" <!-- IMPORT partials/data/category.tpl -->>
		<meta itemprop="name" content="{function.stripTags, title}">
		<!-- IF topics.deleted -->
		<span class="userDelete">该帖子已被用户删除，可以恢复或永久删除</span>
		<!-- ENDIF topics.deleted -->
		<div class="topic-container row col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="user-info">
				<p class="year hidden-xs">{topics.releaseT.year}</p>
				<p class="date hidden-xs">{topics.releaseT.date}</p>
				<div class="avatar-container">
					<a href="<!-- IF topics.isUser -->/ucenter/topic<!-- ELSE -->{config.relative_path}/user/{topics.user.myquantid}<!-- ENDIF topics.isUser -->"
					 target="_blank" class="">
						<!-- IF topics.user.picture -->
						<img component="user/picture" data-uid="{topics.user.uid}" src="{topics.user.picture}" class="user-img" />
						<!-- ELSE -->
						<img src="{config.relative_path}/images/user-icon.png" />
						<!-- ENDIF topics.user.picture -->
						<!-- IF topics.isOfficial --><i class="goldminerimg2 g-official"></i><!-- ENDIF topics.isOfficial -->
						<span class="official-tips">{topics.user.username} <!-- IF topics.isOfficial --> <span class="official-tips-group">【 {topics.groupName} 】</span><!-- ENDIF topics.isOfficial --><i class="triangle"></i></span>
					</a>
				</div>

			</div>
			<div class="topic-list-container">
				<div class="date-phone visible-xs">
					<p>{topics.releaseT.year} {topics.releaseT.date}</p>
				</div>
				<div class="title">
					<a href="{config.relative_path}/topic/{topics.tid}<!-- IF topics.bookmark -->/{topics.bookmark}<!-- ENDIF topics.bookmark -->"
					 itemprop="url" target="_blank" class="<!-- IF topics.highlighted --><!-- IF topics.isOfficial -->markedT<!-- ENDIF topics.isOfficial --><!-- ENDIF topics.highlighted -->">{topics.title}</a>
					<i class="hasPinned <!-- IF !topics.pinned -->hide<!-- ENDIF !topics.pinned -->">[[topic:pinned]]</i>
					<i class="hasHighLighted <!-- IF !topics.highlighted -->hide<!-- ENDIF !topics.highlighted -->">[[topic:highlighted]]</i>
				</div>
				<div class="topic-list-detail">
					<div class="detail-left">
						{topics.thumbnail}
						<div class="detail-all-label">
							<span class="detail-label">{topics.category.name}</span>
							<!-- IF topics.strategyId -->
							<span class="detail-label"><i class="goldminerimg2 g-strategy"></i>源码分享</span>
							<!-- ENDIF topics.strategyId -->
							<!-- IF topics.hasIframe -->
							<span class="detail-label label-performance"><i class="goldminerimg2 g-performance"></i>绩效分析<span class="thumb"><img src="{topics.iframeUrl}"/><i class="triangle"></i></span></span>
							<!-- ENDIF topics.hasIframe -->
							
						</div>
					</div>
					<div class="detail-right">
						<div class="detail-reply-msg hidden-xs">
							<!-- IF !topics.unreplied -->
							<a href="<!-- IF topics.teaser.isUser -->/ucenter/topic<!-- ELSE -->{config.relative_path}/user/{topics.teaser.user.myquantid}<!-- ENDIF topics.teaser.isUser -->"
							 target="_blank" class="detail-right-avatar">
							 	<!-- IF topics.teaser.user.picture -->
								<img component="user/picture" data-uid="{topics.teaser.user.uid}" src="{topics.teaser.user.picture}" class="user-img" />
								<!-- ELSE -->
								<img src="{config.relative_path}/images/user-icon.png"/>
								<!-- ENDIF topics.teaser.user.picture -->
								<!-- IF topics.teaser.user.isOfficial --><i class="goldminerimg2 g-official"></i><!-- ENDIF topics.teaser.user.isOfficial -->
						<span class="official-tips">{topics.teaser.user.username} <!-- IF topics.teaser.user.isOfficial --> <span class="official-tips-group">【 {topics.teaser.user.groupName} 】</span><!-- ENDIF topics.teaser.user.isOfficial --><i class="triangle"></i></span>
							</a>
							<div class="detail-reply-t">
								<p>最近回复:
									<span class="username"><a href="<!-- IF topics.teaser.isUser -->/ucenter/topic<!-- ELSE -->{config.relative_path}/user/{topics.teaser.user.myquantid}<!-- ENDIF topics.teaser.isUser -->" target="_blank">{topics.teaser.user.username}</a></span>
								</p>
								<p>{topics.teaser.timestamp}</p>
								
							</div>
							<!-- ELSE -->
							<span class="no-reply">暂无回复</span>
							<a href="{config.relative_path}/topic/{topics.slug}<!-- IF topics.bookmark -->/{topics.bookmark}<!-- ENDIF topics.bookmark -->"
								itemprop="url" target="_blank" class="btn btn-primary new-reply hidden-xs">消灭0回复</a>
							<!-- ENDIF !topics.unreplied -->
						</div>
						<div class="relevance col">
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center"><span class="goldminerimg2 g-view"></span>
								<span class="viewcount">{topics.viewcount}</span>
							</div>
							<div class="col-lg-4 col-md-4 text-center col-sm-4 col-xs-4 "><span class="goldminerimg2 g-replyicon"></span>
								<span class="postcount">{topics.postcount}</span>
							</div>
							<div class="col-lg-4 text-center col-md-4 col-sm-4 col-xs-4 "><span class="goldminerimg2 g-bookmarked"></span>
								<span class="upvotes">{topics.bookmarked}</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</li>
	<!-- END topics -->
</ul>