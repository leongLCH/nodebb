"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = require("path");
const fs_1 = require("fs");
const multer = require("multer");
const settings = require("./settings");
const pubsub_1 = require("./pubsub");
const nconf = require.main.require('nconf');
const version = require(path_1.join(__dirname, '../../package.json')).version;
function controllers({ router, middleware }) {
    const renderAdmin = (req, res, next) => {
        settings.get((err, sets) => {
            if (err) {
                next(err);
                return;
            }
            res.render('admin/plugins/emoji', {
                version,
                settings: sets,
            });
        });
    };
    router.get('/admin/plugins/emoji', middleware.admin.buildHeader, renderAdmin);
    router.get('/api/admin/plugins/emoji', renderAdmin);
    const saveAdmin = (req, res, next) => {
        const data = JSON.parse(req.query.settings);
        settings.set(data, (err) => {
            if (err) {
                next(err);
                return;
            }
            res.send('OK');
        });
    };
    router.get('/api/admin/plugins/emoji/save', saveAdmin);
    const adminBuild = (req, res, next) => {
        pubsub_1.build((err) => {
            if (err) {
                next(err);
            }
            else {
                res.send('OK');
            }
        });
    };
    router.get('/api/admin/plugins/emoji/build', adminBuild);
    const uploadEmoji = (req, res, next) => {
        if (!req.file) {
            res.sendStatus(400);
            return;
        }
        const fileName = req.body.fileName;
        fs_1.rename(req.file.path, path_1.join(nconf.get('upload_path'), 'emoji', fileName), (err) => {
            if (err) {
                next(err);
            }
            else {
                res.sendStatus(200);
            }
        });
    };
    const upload = multer({
        dest: path_1.join(nconf.get('upload_path'), 'emoji'),
    });
    router.post('/api/admin/plugins/emoji/upload', upload.single('emojiImage'), uploadEmoji);
}
exports.default = controllers;
//# sourceMappingURL=controllers.js.map