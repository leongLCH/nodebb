"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const async_1 = require("async");
const fs_1 = require("fs");
const settings = require("./settings");
const plugins = require("./plugins");
exports.plugins = plugins;
const parse = require("./parse");
exports.parse = parse;
const build_1 = require("./build");
const pubsub_1 = require("./pubsub");
const controllers_1 = require("./controllers");
require("./customizations");
const nconf = require.main.require('nconf');
const db = require.main.require('./src/database');
const buster = require.main.require('./src/meta').config['cache-buster'];
const init = (params, callback) => {
    controllers_1.default(params);
    async_1.waterfall([
        settings.get,
        ({ parseAscii, parseNative }, next) => {
            // initialise ascii flag
            parse.setOptions({
                ascii: parseAscii,
                native: parseNative,
            });
            // always build on startup if in dev mode
            if (nconf.any('build_emoji', 'BUILD_EMOJI')) {
                next(null, true);
                return;
            }
            // otherwise, build if never built before
            fs_1.access(build_1.tableFile, (err) => {
                if (err && err.code !== 'ENOENT') {
                    return next(err);
                }
                next(null, !!err);
            });
        },
        (shouldBuild, next) => {
            if (shouldBuild) {
                pubsub_1.build(next);
            }
            else {
                next();
            }
        },
    ], callback);
};
exports.init = init;
const adminMenu = (header, callback) => {
    header.plugins.push({
        route: '/plugins/emoji',
        icon: 'fa-smile-o',
        name: 'Emoji',
    });
    callback(null, header);
};
exports.adminMenu = adminMenu;
const composerFormatting = (data, callback) => {
    data.options.push({
        name: 'emoji-add-emoji',
        className: 'fa fa-smile-o emoji-add-emoji',
        title: '[[emoji:composer.title]]',
    });
    callback(null, data);
};
exports.composerFormatting = composerFormatting;
const addStylesheet = (data, callback) => {
    const rel = nconf.get('relative_path');
    data.links.push({
        rel: 'stylesheet',
        href: `${rel}/plugins/nodebb-plugin-emoji/emoji/styles.css?${buster}`,
    });
    callback(null, data);
};
exports.addStylesheet = addStylesheet;
//# sourceMappingURL=index.js.map