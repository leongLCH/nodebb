"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const settings = require.main.require('./src/meta').settings;
const defaults = {
    parseNative: true,
    parseAscii: true,
};
const get = (callback) => {
    settings.get('emoji', (err, data) => {
        if (err) {
            callback(err);
            return;
        }
        const sets = {};
        Object.keys(defaults).forEach((key) => {
            const defaultVal = defaults[key];
            const str = data[key];
            if (typeof str !== 'string') {
                sets[key] = defaultVal;
                return;
            }
            const val = JSON.parse(str);
            if (typeof val !== typeof defaultVal) {
                sets[key] = defaultVal;
                return;
            }
            sets[key] = val;
        });
        callback(null, sets);
    });
};
exports.get = get;
const set = (data, callback) => {
    const sets = {};
    Object.keys(data).forEach((key) => {
        sets[key] = JSON.stringify(data[key]);
    });
    settings.set('emoji', sets, callback);
};
exports.set = set;
const getOne = (field, callback) => {
    settings.getOne('emoji', field, (err, val) => {
        if (err) {
            callback(err);
            return;
        }
        callback(null, JSON.parse(val));
    });
};
exports.getOne = getOne;
const setOne = (field, value, callback) => {
    settings.setOne('emoji', field, JSON.stringify(value), callback);
};
exports.setOne = setOne;
//# sourceMappingURL=settings.js.map