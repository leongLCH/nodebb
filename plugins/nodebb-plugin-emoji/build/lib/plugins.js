"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = require("fs");
const path_1 = require("path");
const pubsub_1 = require("./pubsub");
const nconf = require.main.require('nconf');
const baseDir = nconf.get('base_dir');
const noop = () => { };
// build when a plugin is (de)activated if that plugin is an emoji pack
const toggle = ({ id }, cb = noop) => {
    fs_1.readFile(path_1.join(baseDir, 'node_modules', id, 'plugin.json'), 'utf8', (err, file) => {
        if (err && err.code !== 'ENOENT') {
            cb(err);
            return;
        }
        if (err || !file) {
            cb();
            return;
        }
        let plugin;
        try {
            plugin = JSON.parse(file);
        }
        catch (e) {
            cb(e);
            return;
        }
        const hasHook = plugin.hooks && plugin.hooks
            .some((hook) => hook.hook === 'filter:emoji.packs');
        if (hasHook) {
            pubsub_1.build(cb);
        }
    });
};
exports.activation = toggle;
exports.deactivation = toggle;
//# sourceMappingURL=plugins.js.map