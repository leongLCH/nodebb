"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = require("fs");
const path_1 = require("path");
const fs_extra_1 = require("fs-extra");
const lodash_1 = require("lodash");
const async = require("async");
const cssBuilders = require("./css-builders");
const parse_1 = require("./parse");
const customizations_1 = require("./customizations");
const nconf = require.main.require('nconf');
const winston = require.main.require('winston');
const db = require.main.require('./src/database');
const plugins = require.main.require('./src/plugins');
exports.assetsDir = path_1.join(__dirname, `../emoji`);
const linkDirs = (sourceDir, destDir, callback) => {
    const type = (process.platform === 'win32') ? 'junction' : 'dir';
    fs_1.symlink(sourceDir, destDir, type, callback);
};
exports.tableFile = path_1.join(exports.assetsDir, 'table.json');
exports.aliasesFile = path_1.join(exports.assetsDir, 'aliases.json');
exports.asciiFile = path_1.join(exports.assetsDir, 'ascii.json');
exports.charactersFile = path_1.join(exports.assetsDir, 'characters.json');
exports.categoriesFile = path_1.join(exports.assetsDir, 'categories.json');
exports.packsFile = path_1.join(exports.assetsDir, 'packs.json');
function build(callback) {
    winston.verbose('[emoji] Building emoji assets');
    async.waterfall([
        // fetch the emoji definitions
        (next) => plugins.fireHook('filter:emoji.packs', { packs: [] }, next),
        // filter out invalid ones
        ({ packs }, next) => {
            const filtered = packs.filter((pack) => {
                if (pack && pack.id && pack.name && pack.mode && pack.dictionary && pack.path) {
                    return true;
                }
                winston.warn('[emoji] pack invalid', pack.path || pack.id);
                return false;
            });
            winston.verbose('[emoji] Loaded packs', filtered.map(pack => pack.id).join(', '));
            async.series([
                    cb => fs_extra_1.remove(exports.assetsDir, cb),
                    cb => fs_extra_1.mkdirp(exports.assetsDir, cb),
            ], (err) => next(err, filtered));
        },
        (packs, next) => {
            customizations_1.getCustomizations((err, custs) => next(err, packs, custs));
        },
        (packs, customizations, next) => {
            const table = {};
            const aliases = {};
            const ascii = {};
            const characters = {};
            const categoriesInfo = {};
            const packsInfo = [];
            packs.forEach((pack) => {
                packsInfo.push({
                    name: pack.name,
                    id: pack.id,
                    attribution: pack.attribution,
                    license: pack.license,
                });
                Object.keys(pack.dictionary).forEach((key) => {
                    const name = key.toLowerCase();
                    const emoji = pack.dictionary[key];
                    if (!table[name]) {
                        table[name] = {
                            name,
                            character: emoji.character || `:${name}:`,
                            image: emoji.image || '',
                            pack: pack.id,
                            aliases: emoji.aliases || [],
                            keywords: emoji.keywords || [],
                        };
                    }
                    if (!characters[emoji.character]) {
                        characters[emoji.character] = name;
                    }
                    if (emoji.aliases) {
                        emoji.aliases.forEach((alias) => {
                            const a = alias.toLowerCase();
                            if (!aliases[a]) {
                                aliases[a] = name;
                            }
                        });
                    }
                    if (emoji.ascii) {
                        emoji.ascii.forEach((str) => {
                            if (!ascii[str]) {
                                ascii[str] = name;
                            }
                        });
                    }
                    const categories = emoji.categories || ['other'];
                    categories.forEach((category) => {
                        categoriesInfo[category] = categoriesInfo[category] || [];
                        categoriesInfo[category].push(name);
                    });
                });
            });
            Object.keys(categoriesInfo).forEach((category) => {
                categoriesInfo[category] = lodash_1.uniq(categoriesInfo[category]);
            });
            customizations.emojis.forEach((emoji) => {
                const name = emoji.name.toLowerCase();
                table[name] = {
                    name,
                    character: `:${name}:`,
                    pack: 'customizations',
                    keywords: [],
                    image: emoji.image,
                    aliases: emoji.aliases,
                };
                emoji.aliases.forEach((alias) => {
                    const a = alias.toLowerCase();
                    if (!aliases[a]) {
                        aliases[a] = name;
                    }
                });
                emoji.ascii.forEach((str) => {
                    if (!ascii[str]) {
                        ascii[str] = name;
                    }
                });
                categoriesInfo.custom = categoriesInfo.custom || [];
                categoriesInfo.custom.push(name);
            });
            customizations.adjuncts.forEach((adjunct) => {
                const name = adjunct.name;
                if (!table[name]) {
                    return;
                }
                table[name] = Object.assign({}, table[name], { aliases: table[name].aliases.concat(adjunct.aliases) });
                adjunct.aliases.forEach(alias => aliases[alias] = name);
                adjunct.ascii.forEach(str => ascii[str] = name);
            });
            async.parallel([
                // generate CSS styles and store them
                (cb) => {
                    const css = packs.map(pack => cssBuilders[pack.mode](pack)).join('\n');
                    fs_1.writeFile(path_1.join(exports.assetsDir, 'styles.css'), `${css}\n.emoji-customizations {` +
                        'display: inline-block;' +
                        'height: 23px;' +
                        'margin-top: -1px;' +
                        'margin-bottom: -1px;' +
                        '}', { encoding: 'utf8' }, cb);
                },
                // persist metadata to disk
                    cb => fs_1.writeFile(exports.tableFile, JSON.stringify(table), cb),
                    cb => fs_1.writeFile(exports.aliasesFile, JSON.stringify(aliases), cb),
                    cb => fs_1.writeFile(exports.asciiFile, JSON.stringify(ascii), cb),
                    cb => fs_1.writeFile(exports.charactersFile, JSON.stringify(characters), cb),
                    cb => fs_1.writeFile(exports.categoriesFile, JSON.stringify(categoriesInfo), cb),
                    cb => fs_1.writeFile(exports.packsFile, JSON.stringify(packsInfo), cb),
                // handle copying or linking necessary assets
                    cb => async.each(packs, (pack, next) => {
                    const dir = path_1.join(exports.assetsDir, pack.id);
                    if (pack.mode === 'images') {
                        linkDirs(path_1.resolve(pack.path, pack.images.directory), dir, next);
                    }
                    else if (pack.mode === 'sprite') {
                        const filename = path_1.basename(pack.sprite.file);
                        async.series([
                                cb => fs_extra_1.mkdirp(dir, cb),
                                cb => fs_extra_1.copy(path_1.resolve(pack.path, pack.sprite.file), path_1.join(dir, filename), cb),
                        ], next);
                    }
                    else {
                        const fontFiles = [
                            pack.font.eot,
                            pack.font.svg,
                            pack.font.woff,
                            pack.font.ttf,
                            pack.font.woff2,
                        ].filter(Boolean);
                        async.series([
                                cb => fs_extra_1.mkdirp(dir, cb),
                                cb => async.each(fontFiles, (file, next) => {
                                const filename = path_1.basename(file);
                                fs_extra_1.copy(path_1.resolve(pack.path, file), path_1.join(dir, filename), next);
                            }, cb),
                        ], next);
                    }
                }, cb),
                // link customizations to public/uploads/emoji
                    cb => linkDirs(path_1.join(nconf.get('upload_path'), 'emoji'), path_1.join(exports.assetsDir, 'customizations'), cb),
            ], next);
        },
        (results, next) => {
            parse_1.clearCache();
            next();
        },
    ], callback);
}
exports.default = build;
//# sourceMappingURL=build.js.map