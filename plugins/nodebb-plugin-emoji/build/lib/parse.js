"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = require("fs");
const async_1 = require("async");
const build_1 = require("./build");
const buster = require.main.require('./src/meta').config['cache-buster'];
const nconf = require.main.require('nconf');
const url = nconf.get('url');
let metaCache = null;
function clearCache() {
    metaCache = null;
}
exports.clearCache = clearCache;
const escapeRegExpChars = (text) => text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
const getTable = (callback) => {
    if (metaCache) {
        callback(null, metaCache);
        return;
    }
    async_1.parallel({
        table: next => fs_1.readFile(build_1.tableFile, next),
        aliases: next => fs_1.readFile(build_1.aliasesFile, next),
        ascii: next => fs_1.readFile(build_1.asciiFile, next),
        characters: next => fs_1.readFile(build_1.charactersFile, next),
    }, (err, results) => {
        if (err) {
            callback(err);
            return;
        }
        try {
            const ascii = JSON.parse(results.ascii.toString());
            const asciiPattern = Object.keys(ascii)
                .sort((a, b) => b.length - a.length)
                .map(escapeRegExpChars)
                .join('|');
            const characters = JSON.parse(results.characters.toString());
            const charPattern = Object.keys(characters)
                .sort((a, b) => b.length - a.length)
                .map(escapeRegExpChars)
                .join('|');
            metaCache = {
                ascii,
                characters,
                table: JSON.parse(results.table.toString()),
                aliases: JSON.parse(results.aliases.toString()),
                asciiPattern: new RegExp(`(?:^|\\s)(${asciiPattern})(?=\\s|$)`, 'g'),
                charPattern: new RegExp(charPattern, 'g'),
            };
        }
        catch (e) {
            callback(e);
            return;
        }
        callback(null, metaCache);
    });
};
const outsideCode = /(^|<\/code>)([^<]*|<(?!code[^>]*>))*(<code[^>]*>|$)/g;
const outsideElements = /(<[^>]*>)?([^<>]*)/g;
const emojiPattern = /:([a-z\-.+0-9_]+):/g;
exports.buildEmoji = (emoji, whole) => {
    if (emoji.image) {
        const route = `${url}/plugins/nodebb-plugin-emoji/emoji/${emoji.pack}`;
        return `<img
      src="${route}/${emoji.image}?${buster}"
      class="not-responsive emoji emoji-${emoji.pack} emoji--${emoji.name}"
      title="${whole}"
      alt="${emoji.character}"
    />`;
    }
    return `<span
    class="emoji emoji-${emoji.pack} emoji--${emoji.name}"
    title="${whole}"
  ><span>${emoji.character}</span></span>`;
};
const replaceAscii = (str, { ascii, asciiPattern, table }) => {
    return str.replace(asciiPattern, (full, text) => {
        const emoji = ascii[text] && table[ascii[text]];
        if (emoji) {
            return full.replace(text, exports.buildEmoji(emoji, text));
        }
        return text;
    });
};
const replaceNative = (str, { characters, charPattern }) => {
    return str.replace(charPattern, (char) => `:${characters[char]}:`);
};
const options = {
    ascii: false,
    native: false,
};
function setOptions(newOptions) {
    Object.assign(options, newOptions);
}
exports.setOptions = setOptions;
const parse = (content, callback) => {
    getTable((err, store) => {
        if (err) {
            callback(err);
            return;
        }
        const { table, aliases } = store;
        const parsed = content.replace(outsideCode, outsideCodeStr => outsideCodeStr.replace(outsideElements, (whole, inside, outside) => {
            let output = outside;
            if (options.native) {
                // avoid parsing native inside HTML tags
                // also avoid converting ascii characters
                output = output.replace(/(<[^>]+>)|([^0-9a-zA-Z`~!@#$%^&*()\-=_+{}|[\]\\:";'<>?,./\s\n]+)/g, (full, tag, text) => {
                    if (text) {
                        return replaceNative(text, store);
                    }
                    return full;
                });
            }
            output = output.replace(emojiPattern, (whole, text) => {
                const name = text.toLowerCase();
                const emoji = table[name] || table[aliases[name]];
                if (emoji) {
                    return exports.buildEmoji(emoji, whole);
                }
                return whole;
            });
            if (options.ascii) {
                // avoid parsing native inside HTML tags
                output = output.replace(/(<[^>]+>)|([^<]+)/g, (full, tag, text) => {
                    if (text) {
                        return replaceAscii(text, store);
                    }
                    return full;
                });
            }
            return (inside || '') + (output || '');
        }));
        callback(null, parsed);
    });
};
function raw(content, callback) {
    parse(content, callback);
}
exports.raw = raw;
function post(data, callback) {
    parse(data.postData.content, (err, content) => {
        if (err) {
            callback(err);
            return;
        }
        data.postData.content = content;
        callback(null, data);
    });
}
exports.post = post;
//# sourceMappingURL=parse.js.map