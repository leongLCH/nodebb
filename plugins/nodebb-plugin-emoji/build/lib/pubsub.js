"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const os_1 = require("os");
const build_1 = require("./build");
const nconf = require.main.require('nconf');
const winston = require.main.require('winston');
const pubsub = require.main.require('./src/pubsub');
const primary = nconf.get('isPrimary') === 'true' || nconf.get('isPrimary') === true;
function build(callback) {
    if (pubsub.pubClient) {
        pubsub.publish('emoji:build', {
            hostname: os_1.hostname(),
        });
    }
    if (primary) {
        build_1.default(callback);
    }
    else {
        callback();
    }
}
exports.build = build;
const logErrors = (err) => {
    if (err) {
        winston.error(err);
    }
};
if (primary) {
    pubsub.on('emoji:build', (data) => {
        if (data.hostname !== os_1.hostname()) {
            build_1.default(logErrors);
        }
    });
}
//# sourceMappingURL=pubsub.js.map