"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const adminSockets = require.main.require('./src/socket.io/admin');
const db = require.main.require('./src/database');
const hash = require('string-hash');
const async = require("async");
const emojisKey = 'emoji:customizations:emojis';
const adjunctsKey = 'emoji:customizations:adjuncts';
exports.getCustomizations = (callback) => {
    async.waterfall([
        (next) => async.parallel({
            emojis: cb => db.getSortedSetRangeWithScores(emojisKey, 0, -1, cb),
            adjuncts: cb => db.getSortedSetRangeWithScores(adjunctsKey, 0, -1, cb),
        }, next),
        ({ emojis, adjuncts }, next) => {
            const emojisParsed = emojis.map(emoji => JSON.parse(emoji.value));
            const adjunctsParsed = adjuncts.map(adjunct => JSON.parse(adjunct.value));
            next(null, {
                emojis: emojisParsed,
                adjuncts: adjunctsParsed,
            });
        },
    ], callback);
};
const editThing = (key, name, thing) => {
    async.series([
        (next) => {
            const num = hash(name);
            db.sortedSetsRemoveRangeByScore([key], num, num, next);
        },
        (next) => {
            const num = hash(thing.name);
            db.sortedSetAdd(key, num, JSON.stringify(thing), next);
        },
    ]);
};
const deleteThing = (key, name) => {
    const num = hash(name);
    db.sortedSetsRemoveRangeByScore([key], num, num);
};
const emojiSockets = {};
emojiSockets.getCustomizations = (socket, callback) => exports.getCustomizations(callback);
emojiSockets.editEmoji = (socket, [name, emoji]) => editThing(emojisKey, name, emoji);
emojiSockets.deleteEmoji = (socket, name) => deleteThing(emojisKey, name);
emojiSockets.editAdjunct = (socket, [name, adjunct]) => editThing(adjunctsKey, name, adjunct);
emojiSockets.deleteAdjunct = (socket, name) => deleteThing(adjunctsKey, name);
adminSockets.plugins.emoji = emojiSockets;
//# sourceMappingURL=customizations.js.map