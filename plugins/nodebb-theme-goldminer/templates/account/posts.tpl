<div class="account" 
			style="padding:10px 15px;<!-- IF toIframe -->margin-top:-45px;<!-- ENDIF toIframe -->"
		>
	<!-- IMPORT partials/account/header.tpl -->

	<div class="row">

		<!-- IF !posts.length -->
			<div class="alert alert-warning text-center">{noItemsFoundKey}</div>
		<!-- ENDIF !posts.length -->

		<div class="col-xs-12" style="padding:0">
			<!-- IMPORT partials/posts_list.tpl -->

			<!-- IF config.usePagination -->
				<!-- IMPORT partials/paginator.tpl -->
			<!-- ENDIF config.usePagination -->
		</div>
	</div>
</div>