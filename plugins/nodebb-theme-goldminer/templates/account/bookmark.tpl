<div class="account" 
			style="padding:10px 15px;<!-- IF toIframe -->margin-top:-45px;<!-- ENDIF toIframe -->"
		>
	<!-- IMPORT partials/account/header.tpl -->

	<div class="row">

		<!-- IF !posts.length -->
			<div class="alert alert-warning text-center">{noItemsFoundKey}</div>
		<!-- ENDIF !posts.length -->

		<div class="col-xs-12" style="padding:0">
			<ul component="posts" class="posts-list" data-nextstart="{nextStart}">

	<!-- BEGIN posts -->
	<li component="post" class="posts-list-item row bookmark-list-item<!-- IF posts.deleted --> deleted<!-- ELSE --><!-- IF posts.topic.deleted --> deleted<!-- ENDIF posts.topic.deleted --><!-- ENDIF posts.deleted -->" data-pid="{posts.pid}" data-uid="{posts.uid}">
		<div class="col-lg-12 col-sm-12 col-xs-12 post-body">
			<a class="topic-title" href="{config.relative_path}/post/{posts.pid}" target="_blank">{posts.topic.title}
			</a>
 
			<div component="post/content" class="content">
			<div class="content-reply">{posts.content}</div>
			</div>
			<span class="timeago" title="{posts.timestampISO}"></span>
		</div>
	</li>
	<!-- END posts -->
</ul>
<div component="posts/loading" class="loading-indicator text-center hidden">
	<i class="fa fa-refresh fa-spin"></i>
</div>

			<!-- IF config.usePagination -->
				<!-- IMPORT partials/paginator.tpl -->
			<!-- ENDIF config.usePagination -->
		</div>
	</div>
</div>