<div class="account" <!-- IF toIframe -->
			style="margin-top:-45px;padding:10px 15px;"
		<!-- ENDIF toIframe -->>
	<!-- IMPORT partials/account/header.tpl -->

	<div class="row">

		<!-- IF !topics.length -->
			<div class="alert alert-warning text-center">{noItemsFoundKey}</div>
		<!-- ENDIF !topics.length -->

		<div class="category">
			<!-- IMPORT partials/topics_list.tpl -->
			<!-- IF config.usePagination -->
				<!-- IMPORT partials/paginator.tpl -->
			<!-- ENDIF config.usePagination -->
		</div>
	</div>
</div>
