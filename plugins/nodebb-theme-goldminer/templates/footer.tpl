</div><!-- END container -->
<!-- IF !toIframe -->
<!-- IMPORT partials/myquantFooter.tpl -->
<!-- ENDIF !toIframe -->
</main>
<!-- IF !toIframe -->
<div class="container-fluid" id="footer-bottom" data-referer="/"><div class="row"><div class="col-xs-12"><div class="pull-left"><div class="col-xs-12 nopadding">©2013-2018 深圳红树科技有限公司 版权所有 <a href="http://www.miitbeian.gov.cn/" class="hidden-xs" title="粤ICP备14007024号" target="_blank">粤ICP备14007024号</a></div><div class="visible-xs col-xs-12 nopadding"><a href="http://www.miitbeian.gov.cn/" title="粤ICP备14007024号" target="_blank">粤ICP备14007024号</a></div></div><div class="pull-right"><a href="/sitemap.xml" target="_blank" style="color: #2C3E50;cursor:default;text-decoration: none;">网站地图</a><a href="//szcert.ebs.org.cn/d8fe3fb8-82a2-4e74-a24a-103a844fdb5c" target="_blank" title="工商网监"><img src="{config.relative_path}/images/govIcon.gif" alt="工商网监">                    工商网监                </a></div></div></div></div>
<div class="topic-search hidden">
<div class="btn-group">
    <button type="button" class="btn btn-default count"></button>
    <button type="button" class="btn btn-default prev"><i class="fa fa-fw fa-angle-up"></i></button>
    <button type="button" class="btn btn-default next"><i class="fa fa-fw fa-angle-down"></i></button>
</div>
</div>
<!-- ENDIF !toIframe -->
<div component="toaster/tray" class="alert-window">
<div id="reconnect-alert" class="alert alert-dismissable alert-warning clearfix hide" component="toaster/toast">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <p>[[global:reconnecting-message, {config.siteTitle}]]</p>
</div>
</div>

<script defer src="{relative_path}/assets/nodebb.min.js?{config.cache-buster}"></script>

<!-- BEGIN scripts -->
<script defer type="text/javascript" src="{scripts.src}"></script>
<!-- END scripts -->
<script typet="text/javascript" src="//res.wx.qq.com/open/js/jweixin-1.2.0.js"></script>
<!-- IF config.statistics -->
<script type="text/javascript">{config.statistics}</script>
<!-- ENDIF config.statistics -->
<script>
window.addEventListener('load', function () {
    require(['forum/myquantHeader','forum/footer']);
    $(function () {
var _url="/wechat/share";
var o = {url: location.href}
$.get(_url,o,function (res) {
    if (res.errcode==0){
        wx.config({
            debug: false,
            appId:res.data.appId,
            timestamp:res.data.timestamp,
            nonceStr: res.data.nonceStr,
            signature:res.data.signature,
            jsApiList: [
                'checkJsApi',
                'onMenuShareTimeline',
                'onMenuShareAppMessage',
                'onMenuShareQQ',
                'onMenuShareQZone',
                'onMenuShareWeibo',
            ]
        });

        wx.ready(function () {
            var link = res.data.link;
            var title = $("title").text();
            var desc =$("[name=description]").attr("content") || $("title").text(); 
            var imgUrl="http://www.myquant.cn/static/images/logo-share.png";
            var shareObj={
                title: title,
                desc: desc, 
                link: link,
                imgUrl: imgUrl
            };

            wx.onMenuShareTimeline(shareObj);
            wx.onMenuShareAppMessage(shareObj);
            wx.onMenuShareQQ(shareObj);
            wx.onMenuShareQZone(shareObj);
            wx.onMenuShareWeibo(shareObj);
        });
    }
});

})

    <!-- IF useCustomJS -->
    {{customJS}}
    <!-- ENDIF useCustomJS -->
});
</script>
<div class="hide">
<!-- IMPORT 500-embed.tpl -->
</div>
</body>
</html>
