<ul component="category" class="topic-list" itemscope itemtype="http://www.schema.org/ItemList" data-nextstart="{nextStart}" data-set="{set}">
	<meta itemprop="itemListOrder" content="descending">
	<!-- BEGIN topics -->
	<li component="category/topic" class="row clearfix category-item {function.generateTopicClass}" <!-- IMPORT partials/data/category.tpl -->>
		<meta itemprop="name" content="{function.stripTags, title}">
		<!-- IF topics.deleted -->
		<span class="userDelete">该帖子已被用户删除，可以恢复或永久删除</span>
		<!-- ENDIF topics.deleted -->
		<div class="topic-container row col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<!-- IF !topics.personal -->
			<div class="avatar-container">
				<a href="<!-- IF topics.isUser -->/ucenter/topic<!-- ELSE -->{config.relative_path}/user/{topics.user.myquantid}<!-- ENDIF topics.isUser -->" target="_blank">
					<!-- IF topics.user.picture -->
					<img component="user/picture" data-uid="{topics.user.uid}" src="{topics.user.picture}" class="user-img" />
					<!-- ELSE -->
					<img src="{config.relative_path}/images/user-icon.png"/>
					<!-- ENDIF topics.user.picture -->
				</a>
				
			</div>
			<!-- ENDIF !topics.personal -->
			<div class="topic-title col-lg-12 col-md-12 col-sm-12 col-xs-12 row" <!-- IF topics.personal -->style="padding-left:0"<!-- ENDIF topics.personal -->>
				<div class="title">
				<a href="{config.relative_path}/topic/{topics.slug}<!-- IF topics.bookmark -->/{topics.bookmark}<!-- ENDIF topics.bookmark -->" itemprop="url" target="_blank"><span class="categoryname">【{topics.category.name}】</span> {topics.title}</a>
				<i class="hasPinned <!-- IF !topics.pinned -->hide<!-- ENDIF !topics.pinned -->">[[topic:pinned]]</i>
				<i class="hasHighLighted <!-- IF !topics.highlighted -->hide<!-- ENDIF !topics.highlighted -->">[[topic:highlighted]]</i>
				</div>
				<div class="release-time">
					<span class="author"><a href="{config.relative_path}/user/{topics.user.myquantid}" target="_blank">{topics.user.username}</a></span>发布于{topics.releaseT}
				</div>
			</div>
		</div>
		<div class="row col-lg-12 col-md-12 content col-sm-12 col-xs-12">
			<!-- IF topics.hasIframe -->
			<div class="thumb col-lg-2 col-md-2 col-sm-0"><img src="{topics.iframeUrl}"/></div>
			<!-- ENDIF topics.hasIframe -->
			<div class="<!-- IF topics.strategyId -->col-lg-10 col-md-10 col-sm-12<!-- ELSE --> col-lg-12 col-md-12 col-sm-12 <!-- ENDIF topics.strategyId --> content-main">{topics.thumbnail}</div>
		</div>
		<div class="row topic-relevance col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="col-lg-8 col-md-6 hidden-sm hidden-xs topic-relevance-left">
				<!-- IF topics.unreplied -->
				<p class="unreply">
					[[category:no_replies]]
				</p>
				<!-- ELSE -->
				<!-- IF topics.teaser.pid -->
				<p class="reply">
					<span class="author"><a href="<!-- IF topics.teaser.isUser -->/ucenter/topic<!-- ELSE -->{config.relative_path}/user/{topics.teaser.user.myquantid}<!-- ENDIF topics.teaser.isUser -->" target="_blank">{topics.teaser.user.username}</a></span> 回复于: {topics.teaser.timestamp}
				</p>
				<!-- ENDIF topics.teaser.pid -->
				<!-- ENDIF topics.unreplied -->
			</div>
			<div class="row col-lg-4 col-md-6 relevance-right col-sm-8 col-xs-12">
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center">浏览: <span class="viewcount">{topics.viewcount}</span></div><div class="col-lg-4 col-md-4 text-center col-sm-4 col-xs-4 ">回复: <span class="postcount">{topics.postcount}</span></div>
				<div class="col-lg-4 text-center col-md-4 col-sm-4 col-xs-4 ">收藏: <span class="upvotes">{topics.bookmarked}</span></div>
			</div>
		</div>
	</li>
	<!-- END topics -->
</ul>
