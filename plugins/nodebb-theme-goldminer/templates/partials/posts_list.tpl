<ul component="posts" class="posts-list" data-nextstart="{nextStart}">

	<!-- BEGIN posts -->
	<li component="post" class="posts-list-item row<!-- IF posts.deleted --> deleted<!-- ELSE --><!-- IF posts.topic.deleted --> deleted<!-- ENDIF posts.topic.deleted --><!-- ENDIF posts.deleted -->" data-pid="{posts.pid}" data-uid="{posts.uid}">
		<div class="col-lg-12 col-sm-12 col-xs-12 post-body">
			<a class="topic-title" href="{config.relative_path}/post/{posts.pid}" target="_blank">{posts.topic.title}
			</a>

			<div component="post/content" class="content">
			回复: <div class="content-reply">{posts.content}</div>
			</div>
			<span class="timeago" title="{posts.timestampISO}"></span>
		</div>
	</li>
	<!-- END posts -->
</ul>
<div component="posts/loading" class="loading-indicator text-center hidden">
	<i class="fa fa-refresh fa-spin"></i>
</div>