<!-- BEGIN following -->
<li class="users-box registered-user" data-uid="{users.uid}">
	<a href="{config.relative_path}/user/{following.myquantid}">
		<!-- IF following.picture -->
		<img src="{following.picture}" />
		<!-- ELSE -->
		<img src="{config.relative_path}/images/user-icon.png"/>
		<!-- ENDIF following.picture -->
	</a>
	<br/>
	<div class="user-info follow-other">
		<span>
			<a href="{config.relative_path}/user/{following.myquantid}">{following.username}</a>
		</span>
		<!-- IF loggedIn -->
		<!-- IF !following.isUser -->
        <button data-uid="{following.uid}" data-name="{following.username}" type="button" class="btn-morph fab btn btn-primary <!-- IF following.isFollowing -->plus heart<!-- ENDIF following.isFollowing -->"><!-- IF !following.isFollowing -->[[user:addfollowing]]<!-- ELSE -->[[user:watched]]<!-- ENDIF !following.isFollowing --></button>
		<!-- ENDIF !following.isUser -->
		<!-- ENDIF loggedIn -->
		<br/>
	</div>
</li>
<!-- END following -->