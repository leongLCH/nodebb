<!-- IF !toIframe -->
<div class="cover" component="account/cover">
<!-- ENDIF !toIframe -->
	<!-- IF !toIframe -->
	<div class="container" <!-- IF toIframe -->style="width:100%;"<!-- ENDIF toIframe -->>
		<span class="account-img">
		<!-- IF picture -->
			<img component="user/picture" data-uid="{uid}" src="{picture}" class="user-img" />
		<!-- ELSE -->
			<img src="{config.relative_path}/images/user-icon.png"/>
		<!-- ENDIF picture -->
		</span>
		<div class="personal">
			<p class="username">{username}</p>
			<!-- IF aboutme -->
			{aboutme}
			<!-- ELSE -->
			<p class="description">[[user:info.has-no-description]]</p>
			<!-- ENDIF aboutme -->
		</div>
		<!-- IF loggedIn -->
		
			<!-- IF !isSelf -->
			<div class="operate">
				<!-- <button type="button" class="btn btn-default" component="account/chat" href="#">[[user:chat]]</button> -->
				<button type="button" class="btn-morph fab btn btn-primary <!-- IF isFollowing -->plus heart<!-- ENDIF isFollowing -->"><!-- IF !isFollowing -->[[user:following]]<!-- ELSE -->[[user:watched]]<!-- ENDIF !isFollowing --></button>
			</div>
			<!-- ENDIF !isSelf -->
		<!-- ENDIF loggedIn -->
		<!-- IF isAdmin -->
		<!-- IMPORT partials/account/menu.tpl -->
		<!-- ENDIF isAdmin -->
	</div>
	<!-- ENDIF !toIframe -->
	<nav class="navbar nav-container navbar-default nav-phone">
		<div class="navbar-header visible-xs-block">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target="#bbs-account-nav">
				<span class="sr-only">切换导航</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<!-- BEGIN nav -->
			<a class="navbar-brand <!-- IF !nav.css --> hidden <!-- ENDIF !nav.css -->" href="{config.relative_path}/user/{myquantid}{nav.slug}" >{nav.name}</a>
			<!-- END nav -->
		</div>
		<div class="collapse navbar-collapse" id="bbs-account-nav">
			<ul class="nav nav-pills category-nav col-lg-9 col-md-9 col-sm-12" style="width:100%">
				<!-- BEGIN nav -->
				<li <!-- IF nav.css -->class="active"<!-- ENDIF nav.css -->><a href="{config.relative_path}/user/{myquantid}{nav.slug}">{nav.name}</a></li>
				<!-- END nav -->
			</ul>
		</div>
	</nav>
<!-- IF !toIframe --></div><!-- ENDIF !toIframe -->
