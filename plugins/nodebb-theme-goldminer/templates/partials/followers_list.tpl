<!-- BEGIN followers -->
<li class="users-box registered-user" data-uid="{users.uid}">
	<a href="{config.relative_path}/user/{following.myquantid}">
		<!-- IF followers.picture -->
		<img src="{followers.picture}" />
		<!-- ELSE -->
		<img src="{config.relative_path}/images/user-icon.png"/>
		<!-- ENDIF followers.picture -->
	</a>
	<br/>
	<div class="user-info follow-other">
		<span>
			<a href="{config.relative_path}/user/{followers.myquantid}">{followers.username}</a>
		</span>
		<!-- IF loggedIn -->
        <!-- IF !followers.isUser -->
        <button data-uid="{followers.uid}" data-name="{followers.username}" type="button" class="btn-morph fab btn btn-primary <!-- IF followers.isFollowing -->plus heart<!-- ENDIF followers.isFollowing -->"><!-- IF !followers.isFollowing -->[[user:addfollowing]]<!-- ELSE -->[[user:watched]]<!-- ENDIF !followers.isFollowing --></button>
        <!-- ENDIF !followers.isUser -->
		<!-- ENDIF loggedIn -->
        
		<br/>
	</div>
</li>
<!-- END followers -->