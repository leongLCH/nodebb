<div class="clearfix post-header">
	<div class="icon pull-left">
		<a href="<!-- IF posts.user.userslug -->{config.relative_path}/user/{posts.user.myquantid}<!-- ELSE -->#<!-- ENDIF posts.user.userslug -->">
			<!-- IF posts.user.picture -->
			<img component="user/picture" data-uid="{posts.user.uid}" src="{posts.user.picture}" class="user-img" />
			<!-- ELSE -->
			<img src="{config.relative_path}/images/user-icon.png"/>
			<!-- ENDIF posts.user.picture -->
		</a>
	</div>

	<small class="pull-left post-user">
		<strong>
			<!-- IF posts.isUser --><a href="<!-- IF posts.user.userslug -->/ucenter<!-- ELSE -->#<!-- ENDIF posts.user.userslug -->" itemprop="author" data-username="{posts.user.username}" data-uid="{posts.user.uid}">{posts.user.username}</a><!-- ELSE --><a href="<!-- IF posts.user.userslug -->{config.relative_path}/user/{posts.user.myquantid}<!-- ELSE -->#<!-- ENDIF posts.user.userslug -->" itemprop="author" data-username="{posts.user.username}" data-uid="{posts.user.uid}">{posts.user.username}</a><!-- ENDIF posts.isUser -->
			
		</strong>
	</small>
</div>

<div class="content col-lg-12 col-md-12 col-sm-12 col-xs-12" component="post/content" itemprop="text">
	<!-- IF posts.strategyId -->
	<div class="thumb col-lg-3 col-md-3 col-sm-0"><img src="{posts.iframeUrl}"/></div>
	<!-- ENDIF posts.strategyId -->
	<div class="<!-- IF posts.strategyId -->col-lg-9 col-md-9 col-sm-12<!-- ELSE --> col-lg-12 col-md-12 col-sm-12 <!-- ENDIF posts.strategyId --> content-main">{posts.content}</div>
	
</div>
<!-- IF posts.selfPost --><!-- IF !privileges.isAdminOrMod -->
<div class="reply-delete"><a component="post/delete" tabindex="-1" href="#">删除</a></div>
<!-- ENDIF !privileges.isAdminOrMod -->
<!-- ENDIF posts.selfPost -->
<div class="post-relate">
	<span class="reply-time">{posts.replyT}</span><!-- IF loggedIn --><span component="post/reply" class="post-reply"><i class="goldminerimg g-reply"></i>[[topic:quick-reply]]</span><span component="post/upvote"><i class="goldminerimg g-up"></i> <span class="votes-count">{posts.votes}</span></span><!-- ENDIF loggedIn -->
</div>
<div class="clearfix post-footer">
	<!-- IF posts.user.signature -->
	<div component="post/signature" data-uid="{posts.user.uid}" class="post-signature">{posts.user.signature}</div>
	<!-- ENDIF posts.user.signature -->

	<small class="pull-right">
		<!-- IF privileges.isAdminOrMod -->
			<!-- IMPORT partials/topic/post-menu.tpl -->
		<!-- ENDIF privileges.isAdminOrMod -->
	</small>
</div>
