<div component="category-selector" class="btn-group">
	
	
		<!-- BEGIN categories -->
		<div role="presentation" class="category <!-- IF categories.disabledClass -->disabled<!-- ENDIF categories.disabledClass -->" data-cid="{categories.cid}" data-name="{categories.name}">
			{categories.name}
		</div>
		<!-- END categories -->
</div>