<div widget-area="header">
	<!-- BEGIN widgets.header -->
	{{widgets.header.html}}
	<!-- END widgets.header -->
</div>
<nav class="navbar navbar-default nav-phone">
	<div class="navbar-header visible-xs-block">
        <button type="button" class="navbar-toggle" data-toggle="collapse"
                data-target="#bbs-nav">
            <span class="sr-only">切换导航</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
		<!-- BEGIN allCategoriesData -->
        <a class="navbar-brand <!-- IF !allCategoriesData.css --> hidden <!-- ENDIF !allCategoriesData.css -->" href="{config.relative_path}/category/{allCategoriesData.cid}/{allCategoriesData.name}" >{allCategoriesData.name}</a>
		<!-- END allCategoriesData -->
    </div>
	<div class="collapse navbar-collapse" id="bbs-nav">
		<ul class="nav nav-pills category-nav col-lg-9 col-md-9 col-sm-12">
			<!-- BEGIN allCategoriesData -->
			<li><a href="{config.relative_path}/category/{allCategoriesData.cid}/{allCategoriesData.name}" class="{allCategoriesData.css}">{allCategoriesData.name}</a></li>
			<!-- END allCategoriesData -->
		</ul>
	</div>
</nav>
<div class="clearfix mobile-new-topic visible-xs-block">
		<!-- IF privileges.topics:create -->
		<a href="{config.relative_path}/compose?cid={cid}" component="category/post" class="btn btn-primary col-lg-12 col-md-12 new-topic-btn" role="button" target="_blank" target="_blank">+ [[category:new_topic_button]]</a>
		<!-- ELSE -->
		<!-- IF !loggedIn -->
		<a component="category/post/guest" href="{config.relative_path}/login" class="btn btn-primary col-lg-12 col-md-12 new-topic-btn">[[category:guest-login-post]]</a>
		<!-- ENDIF !loggedIn -->
		<!-- ENDIF privileges.topics:create -->
</div>
<div class="row">
	<div class="category-items col-lg-9 col-md-8 col-sm-12">
		<div class="category col-lg-12">
			<!-- IF !topics.length -->
			<!-- IF privileges.topics:create -->
			<hr class="visible-xs" />
			<div class="alert alert-warning" id="category-no-topics">
				[[category:no_topics]]
			</div>
			<!-- ENDIF privileges.topics:create -->
			<!-- ENDIF !topics.length -->

			<a href="{url}">
				<div class="alert alert-warning hide" id="new-topics-alert"></div>
			</a>

			<!-- IMPORT partials/topics_list.tpl -->

			<!-- IF config.usePagination -->
				<!-- IMPORT partials/paginator.tpl -->
			<!-- ENDIF config.usePagination -->
		</div>
	</div>
	<div class="container-fluid g-side-bar col-lg-3 col-md-4 hidden-xs hidden-sm ">
		<!-- IF !loggedIn -->
		<div class="row text-center">
			<img src="{config.relative_path}/images/user-icon.png"/>
		</div>
		<div class="text-center username">[[user:welcome-to-bbs]]</div>
		<div class="text-center description">[[user:welcome-description]]</div>
		<div class="clearfix">
				
				<a href="{domainLogin}?redirect={originUrl}" class="btn btn-primary col-lg-12 col-md-12 new-topic-btn">[[category:guest-login-post]]</a>
				
				<a href="{domainReg}?redirect={originUrl}" class="btn btn-primary col-lg-12 col-md-12 btn-reg">[[category:guest-register]]</a>
		</div>
		<!-- ELSE -->
		<div class="row text-center">
			<!-- IF getUserData.picture -->
			<a href="{config.relative_path}/user/{getUserData.myquantid}" target="_blank"><img component="user/picture" data-uid="{getUserData.uid}" src="{getUserData.picture}" class="user-img" /></a>
			<!-- ELSE -->
			<a href="{config.relative_path}/user/{getUserData.myquantid}" target="_blank"><img src="{config.relative_path}/images/user-icon.png"/></a>
			<!-- ENDIF getUserData.picture -->
		</div>
		<div class="text-center username"><a href="{config.relative_path}/user/{getUserData.myquantid}" target="_blank">{getUserData.username}</a></div>
		<div class="text-center description"><!-- IF getUserData.aboutme -->{getUserData.aboutme}<!-- ELSE -->[[user:info.has-no-description]]<!-- ENDIF getUserData.aboutme --></div>
		<div class="row msgcount">
			<span class="col-lg-4 col-md-4 text-center"><a href="/ucenter/topic" target="_blank">{getUserData.topiccount}</a></span><span class="col-lg-4 col-md-4 text-center"><a href="/ucenter/reply" target="_blank">{getUserData.replycount}</a></span><span class="col-lg-4 col-md-4 text-center"><a href="/ucenter/collection" target="_blank">{getUserData.bookmark}</a></span>
		</div>
		<div class="row msgname">
			<span class="col-lg-4 col-md-4 text-center" ><a href="/ucenter/topic" target="_blank">[[category:my_topic]]</a></span>
			<span class="col-lg-4 col-md-4 text-center"><a href="/ucenter/reply" target="_blank">[[category:my_reply]]</a></span>
			<span class="col-lg-4 col-md-4 text-center"><a href="/ucenter/collection" target="_blank">[[category:my_book]]</a></span>
		</div>
		<div class="clearfix">
				<!-- IF privileges.topics:create -->
				<a href="{config.relative_path}/compose?cid={cid}" component="category/post" class="btn btn-primary col-lg-12 col-md-12 new-topic-btn" role="button" target="_blank" target="_blank"><span class="goldminerimg g-post-category"></span>[[category:new_topic_button]]</a>
				<!-- ELSE -->
				<!-- IF !loggedIn -->
				<a component="category/post/guest" href="{config.relative_path}/login" class="btn btn-primary col-lg-12 col-md-12 new-topic-btn">[[category:guest-login-post]]</a>
				<!-- ENDIF !loggedIn -->
				<!-- ENDIF privileges.topics:create -->
		</div>
		<!-- ENDIF !loggedIn -->
		<div class="row popular-side-bar">
			<div class="title"><span class="goldminerimg g-hot"></span>热门话题</div>
			<!-- IF getPopular.length -->
			<ul>
				<!-- BEGIN getPopular -->
					<li><a href="{config.relative_path}/topic/{getPopular.slug}">{getPopular.title}</a></li>
				<!-- END getPopular -->
			</ul>
			<!-- ENDIF getPopular.length -->
		</div>
		<div class="row advert">
			<a href="{config.relative_path}/topic/544" target="_blank"><img src="{config.relative_path}/images/advertisement-1.png" /></a>
			<a href="{config.relative_path}/topic/545" target="_blank"><img style="margin-top:25px;" src="{config.relative_path}/images/advertisement-2.png" /></a>
		</div>
	</div>
	
</div>
<div widget-area="footer">
	<!-- BEGIN widgets.footer -->
	{{widgets.footer.html}}
	<!-- END widgets.footer -->
</div>

<!-- IMPORT partials/move_thread_modal.tpl -->

<!-- IF !config.usePagination -->
<noscript>
	<!-- IMPORT partials/paginator.tpl -->
</noscript>
<!-- ENDIF !config.usePagination -->
