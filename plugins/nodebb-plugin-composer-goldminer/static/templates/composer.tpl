<div component="composer" class="composer<!-- IF resizable --> resizable<!-- ENDIF resizable --><!-- IF !isTopicOrMain --> reply<!-- ENDIF !isTopicOrMain -->">

	<div class="composer-container">
		<div class="write-all-container">
			<div class="write-container col-lg-12 col-md-12">
				<div class="category-tag-row">
					<div class="btn-toolbar formatting-bar">
						<ul class="formatting-group">
							<!-- BEGIN formatting -->
							<!-- IF formatting.spacer -->
							<li class="spacer"></li>
							<!-- ELSE -->
							<!-- IF !formatting.custom -->
							<!-- IF !formatting.mobile -->
							<li tabindex="-1" data-format="{formatting.name}" title="{formatting.title}">
								<i class="{formatting.className}"></i>
							</li>
							<!-- ENDIF !formatting.mobile -->
							<!-- ELSE -->
							<li tabindex="-1" data-format="{formatting.name}" title="{formatting.title}">
								<i class="{formatting.className}" style="font-weight:bold">{formatting.font}</i>
							</li>
							<!-- ENDIF !formatting.custom -->
							<!-- ENDIF formatting.spacer -->
							<!-- END formatting -->

							<!--[if gte IE 9]><!-->
							<li class="img-upload-btn hide" data-format="picture" tabindex="-1" title="[[modules:composer.upload-picture]]">
								<i class="fa fa-picture-o"></i>
							</li>
							<!--<![endif]-->
							<!-- IF allowTopicsThumbnail -->
							<li tabindex="-1">
								<i class="fa fa-th-large topic-thumb-btn topic-thumb-toggle-btn hide" title="[[topic:composer.thumb_title]]"></i>
							</li>
							<div class="topic-thumb-container center-block hide">
								<form id="thumbForm" method="post" class="topic-thumb-form form-inline" enctype="multipart/form-data">
									<img class="topic-thumb-preview"></img>
									<div class="form-group">
										<label for="topic-thumb-url">[[topic:composer.thumb_url_label]]</label>
										<input type="text" id="topic-thumb-url" class="form-control" placeholder="[[topic:composer.thumb_url_placeholder]]" />
									</div>
									<div class="form-group">
										<label for="topic-thumb-file">[[topic:composer.thumb_file_label]]</label>
										<input type="file" id="topic-thumb-file" class="form-control" />
									</div>
									<div class="form-group topic-thumb-ctrl">
										<i class="fa fa-spinner fa-spin hide topic-thumb-spinner" title="[[topic:composer.uploading]]"></i>
										<i class="fa fa-times topic-thumb-btn hide topic-thumb-clear-btn" title="[[topic:composer.thumb_remove]]"></i>
									</div>
								</form>
							</div>
							<!-- ENDIF allowTopicsThumbnail -->

							<form id="fileForm" method="post" enctype="multipart/form-data">
								<!--[if gte IE 9]><!-->
								<input type="file" id="files" name="files[]" multiple class="gte-ie9 hide" />
								<!--<![endif]-->
								<!--[if lt IE 9]>
											<input type="file" id="files" name="files[]" class="lt-ie9 hide" value="Upload"/>
										<![endif]-->
							</form>
						</ul>
					</div>
				</div>
				<textarea name="content" form="compose-form" class="write" tabindex="5" style="resize:none;"></textarea>
			</div>
		</div>
		<div class="share-strategy row">
			<!-- <div class="input-select composer-select">+ [[topic:share_strategy]]</div>
			<span class="strategy-title-show"></span> -->
			<button class="btn btn-primary composer-submit" data-action="post" tabindex="6">[[topic:confirm_submit]]</button>
		</div>
		<!-- IF isTopicOrMain -->
		<div class="tag-row">
			<div class="tags-container">
				<div class="btn-group dropup <!-- IF !tagWhitelist.length -->hidden<!-- ENDIF !tagWhitelist.length -->" component="composer/tag/dropdown">
					<button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button">
						<span class="visible-sm-inline visible-md-inline visible-lg-inline">
							<i class="fa fa-tags"></i>
						</span>
						<span class="caret"></span>
					</button>

					<ul class="dropdown-menu">
						<!-- BEGIN tagWhitelist -->
						<li data-tag="@value">
							<a href="#">@value</a>
						</li>
						<!-- END tagWhitelist -->
					</ul>
				</div>
				<input class="tags" type="text" class="form-control" placeholder="[[tags:enter_tags_here, {minimumTagLength}, {maximumTagLength}]]"
				 tabindex="5" />
			</div>
		</div>
		<!-- ENDIF isTopicOrMain -->

		<!-- IF isTopic -->
		<ul class="category-selector visible-xs visible-sm">

		</ul>
		<!-- ENDIF isTopic -->

		<div class="imagedrop">
			<div>[[topic:composer.drag_and_drop_images]]</div>
		</div>

	</div>

</div>