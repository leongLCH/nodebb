<div component="composer" class="composer<!-- IF resizable --> resizable<!-- ENDIF resizable --><!-- IF !isTopicOrMain --> reply<!-- ENDIF !isTopicOrMain -->"
	<!-- IF !disabled --> style="visibility: inherit;"
	   <!-- ENDIF !disabled -->>
   
	   <div class="composer-container">
		   <form id="compose-form" method="post">
			   <!-- IF pid -->
			   <input type="hidden" name="pid" value="{pid}" />
			   <input type="hidden" name="thumb" value="{thumb}" />
			   <!-- ENDIF pid -->
			   <!-- IF tid -->
			   <input type="hidden" name="tid" value="{tid}" />
			   <!-- ENDIF tid -->
			   <!-- IF cid -->
			   <input type="hidden" name="cid" value="{cid}" />
			   <!-- ENDIF cid -->
			   <input type="hidden" name="_csrf" value="{config.csrf_token}" />
		   </form>
		   <div class="post-title row text-center">
			   <span class="goldminerimg g-edit-topics"></span>[[topic:composer.title]]
		   </div>
		   <div class="topic-title-container">
			   <div class="input-group">
				   <div class="topic-title">[[topic:topic_module]]</div>
				   <div class="category-list-container input-group-btn" data-cid="{cid}">
				   </div>
				   <div class="topic-title">[[topic:topic_title]]</div>
				   <input name="title" form="compose-form" class="title form-control topic-input" type="text" tabindex="1" placeholder="[[topic:composer.title_placeholder]]"
					value="{topicTitle}" />
			   </div>
		   </div>
		   <div class="row edit-container">
			   <div class="topic-title">[[topic:topic_content]]</div>
			   <div class="markdown-help">使用MarkDown语法编辑。查看<a href="https://www.myquant.cn/docs/markdown" class="markdown-link" target="_blank">编写帮助</a></div>			
			   <div class="write-all-container">
				   <div class="write-container">
					   <div class="category-tag-row">
						   <div class="btn-toolbar formatting-bar">
							   <ul class="formatting-group">
								   <!-- BEGIN formatting -->
								   <!-- IF formatting.spacer -->
								   <li class="spacer"></li>
								   <!-- ELSE -->
								   <!-- IF !formatting.custom -->
								   <!-- IF !formatting.mobile -->
								   <li tabindex="-1" data-format="{formatting.name}" title="{formatting.title}">
									   <i class="{formatting.className}"></i>
								   </li>
								   <!-- ENDIF !formatting.mobile -->
								   <!-- ELSE -->
								   <li tabindex="-1" data-format="{formatting.name}" title="{formatting.title}">
									   <i class="{formatting.className}" style="font-weight:bold">{formatting.font}</i>
								   </li>
								   <!-- ENDIF !formatting.custom -->
								   <!-- ENDIF formatting.spacer -->
								   <!-- END formatting -->
   
								   <!--[if gte IE 9]><!-->
								   <li class="img-upload-btn hide" data-format="picture" tabindex="-1"  title="[[modules:composer.upload-picture]]">
									   <i class="fa fa-picture-o"></i>
								   </li>
								   <!--<![endif]-->
								   <li class="hide hidden-sm hidden-xs" data-format="preview" title="[[modules:composer.formatting.preview]]"><i class="fa fa-eye"></i></li>
								   <li class="hidden-sm hidden-xs" data-format="closepreview" title="[[modules:composer.formatting.closepreview]]"><i class="fa fa-eye-slash"></i></li>
								   <!-- IF allowTopicsThumbnail -->
								   <li tabindex="-1">
									   <i class="fa fa-th-large topic-thumb-btn topic-thumb-toggle-btn hide" title="[[topic:composer.thumb_title]]"></i>
								   </li>
								   <div class="topic-thumb-container center-block hide">
									   <form id="thumbForm" method="post" class="topic-thumb-form form-inline" enctype="multipart/form-data">
										   <img class="topic-thumb-preview"></img>
										   <div class="form-group">
											   <label for="topic-thumb-url">[[topic:composer.thumb_url_label]]</label>
											   <input type="text" id="topic-thumb-url" class="form-control" placeholder="[[topic:composer.thumb_url_placeholder]]" />
										   </div>
										   <div class="form-group">
											   <label for="topic-thumb-file">[[topic:composer.thumb_file_label]]</label>
											   <input type="file" id="topic-thumb-file" class="form-control" />
										   </div>
										   <div class="form-group topic-thumb-ctrl">
											   <i class="fa fa-spinner fa-spin hide topic-thumb-spinner" title="[[topic:composer.uploading]]"></i>
											   <i class="fa fa-times topic-thumb-btn hide topic-thumb-clear-btn" title="[[topic:composer.thumb_remove]]"></i>
										   </div>
									   </form>
								   </div>
								   <!-- ENDIF allowTopicsThumbnail -->
   
								   <form id="fileForm" method="post" enctype="multipart/form-data">
									   <!--[if gte IE 9]><!-->
									   <input type="file" id="files" name="files[]" multiple class="gte-ie9 hide" />
									   <!--<![endif]-->
									   <!--[if lt IE 9]>
										   <input type="file" id="files" name="files[]" class="lt-ie9 hide" value="Upload"/>
									   <![endif]-->
								   </form>
							   </ul>
						   </div>
					   </div>
					   <!-- <div class="help-text">
					   [[modules:composer.compose]] <span class="help hidden"><i class="fa fa-question-circle"></i></span>
					   <span class="toggle-preview hide">[[modules:composer.show_preview]]</span>
					   </div> -->
					   <div class="row">
						   <div class="col-md-6 composer-write" id="composer-write">
							   <textarea name="content" form="compose-form" class="write" tabindex="5" style="resize: none;box-shadow:none;outline:0;"></textarea>
						   </div>
						   <div class="col-md-6 hidden-sm hidden-xs preview-container">
							   <div class="preview well"></div>
						   </div>
					   </div>
				   </div>
   
			   </div>
		   </div>
		   <div class="strategy-choose row">
			   <div class="topic-title">[[topic:topic_strategy]]</div>
			   <div class="choose">
				   <div class="strategy-title-show" <!-- IF strategyId --> data-strategyid="{strategyId}"
					   <!-- ENDIF strategyId --> data-hasPerformance="{hasPerformance}">{strategyName}</div>
				   <div class="input-select composer-select">[[topic:topic_choose]]</div>
			   </div>
		   </div>
		   <div class="strategy-choose row backtest-title-show">
			   <div class="topic-title">回测</div>
			   <div class="title"></div>
		   </div>
		   <div class="row text-right submitTopic">
			   
			   <button type="submit" form="compose-form" class="btn btn-primary composer-submit" data-action="post" tabindex="7">[[topic:composer.submit]]</button>
		   </div>
	   </div>
   </div>