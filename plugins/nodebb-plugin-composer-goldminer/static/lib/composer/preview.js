'use strict';

/* globals define, socket*/

define('composer/preview', ['utils'], function (util) {
	var preview = {};

	var timeoutId = 0;
	preview.render = function (postContainer, callback) {
		callback = callback || function () { };
		if (timeoutId) {
			clearTimeout(timeoutId);
			timeoutId = 0;
		}
		var textarea = postContainer.find('textarea');
		timeoutId = setTimeout(function () {
			socket.emit('plugins.composer.renderPreview', textarea.val(), function (err, preview) {
				timeoutId = 0;
				if (err) {
					return;
				}
				var viewSrc = config['composer-default'].view;
				// if ($('.strategy-title-show').attr('data-strategyId')) {
				// 	var strategyId = $('.strategy-title-show').attr('data-strategyId')
				// 	var iframe = '<iframe class="strategyIframe" scrolling="no" src='
				// 		+ viewSrc
				// 		+ strategyId
				// 		+ '?hide=1'
				// 		+ '></iframe>'
				// 	preview += iframe;
				// }
				preview = $(preview);
				preview.find('img:not(.not-responsive)').addClass('img-responsive');
				$('.preview').html(preview);
				MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
				$(window).trigger('action:composer.preview');
				callback();
			});
		}, 250);
	};

	preview.matchScroll = function (postContainer) {
		var textarea = postContainer.find('textarea');
		var preview = postContainer.find('.preview');

		if (textarea.length && preview.length) {
			var diff = textarea[0].scrollHeight - textarea.height();

			if (diff === 0) {
				return;
			}

			var scrollPercent = textarea.scrollTop() / diff;

			preview.scrollTop(Math.max(preview[0].scrollHeight - preview.height(), 0) * scrollPercent);
		}
	};

	preview.handleToggler = function (postContainer, post_uuid) {	
		function hidePreview() {
			togglePreview(false);
			localStorage.setItem('composer:previewToggled', true);
		}

		function showPreview() {
			togglePreview(true);
			localStorage.removeItem('composer:previewToggled');
		}

		function togglePreview(show) {
			previewContainer.toggleClass('hide', !show);
			writeContainer.toggleClass('maximized', !show);
			showBtn.toggleClass('hide', show);
			hideBtn.toggleClass('hide', !show);
			show ? postContainer.find('#composer-write').addClass('col-md-6').removeClass('col-md-12') : postContainer.find('#composer-write').addClass('col-md-12').removeClass('col-md-6')
			$('.write').focus();
			preview.matchScroll(postContainer);
		}

		// var showBtn = postContainer.find('.write-container .toggle-preview'),
			// hideBtn = postContainer.find('.preview-container .toggle-preview'),
		var showBtn =  postContainer.find('[data-format="preview"]'),
			hideBtn = postContainer.find('[data-format="closepreview"]'),	
			previewContainer = $('.preview-container'),
			writeContainer = $('.write-container');

		hideBtn.on('click', hidePreview);
		showBtn.on('click', showPreview);

		postContainer.find('.composer-review').on('click', function (e) {
			e.preventDefault();
			var titleEl = postContainer.find('.title');
			titleEl.val(titleEl.val().trim());
			var bodyEl = postContainer.find('textarea');

			var checkTitle = postContainer.find('input.title').length;
			if (checkTitle && titleEl.val().length < parseInt(config.minimumTitleLength, 10)) {
				return composerAlert(post_uuid, '[[error:title-too-short, ' + config.minimumTitleLength + ']]');
			} else if (checkTitle && titleEl.val().length > parseInt(config.maximumTitleLength, 10)) {
				return composerAlert(post_uuid, '[[error:title-too-long, ' + config.maximumTitleLength + ']]');
			} else if (bodyEl.val().length < parseInt(config.minimumPostLength, 10)) {
				return composerAlert(post_uuid, '[[error:content-too-short, ' + config.minimumPostLength + ']]');
			} else if (bodyEl.val().length > parseInt(config.maximumPostLength, 10)) {
				return composerAlert(post_uuid, '[[error:content-too-long, ' + config.maximumPostLength + ']]');
			}
			var box = '<div class="preview-container">'
				+ '<div class="preview well"></div>'
				+ '</div>';

			bootbox.dialog({
				className: 'review-dialog',
				message: box,
				title: utils.stripHTMLTags(utils.decodeHTMLEntities(titleEl.val().trim()))
			});
			preview.render(postContainer);
		})

		// if (localStorage.getItem('composer:previewToggled')) {
		// 	hidePreview();
		// } else {
		// 	showPreview();
		// }
	};
	function composerAlert(post_uuid, message) {
		$('#cmp-uuid-' + post_uuid).find('.composer-submit').removeAttr('disabled');
		app.alert({
			type: 'danger',
			timeout: 3000,
			title: '失败',
			message: message,
			alert_id: 'post_error'
		});
	}
	return preview;
});