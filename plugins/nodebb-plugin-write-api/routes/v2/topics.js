'use strict';
/* globals module, require */

var Topics = require.main.require('./src/topics'),
	Posts = require.main.require('./src/posts'),
	apiMiddleware = require('./middleware'),
	errorHandler = require('../../lib/errorHandler'),
	utils = require('./utils'),
	winston = require.main.require('winston');
var db = require.main.require('./src/database');

module.exports = function (middleware) {
	var app = require('express').Router();

	app.route('/')
		.post(apiMiddleware.requireUser, function (req, res) {
			function getUidByMyquantId(id, cb) {
				db.sortedSetScore('myquantid:uid', id, cb);
			}
			getUidByMyquantId(req.user.uid, function (err, uid) {
				if (!err) {
					req.user.uid = uid;
					if (!utils.checkRequired(['cid', 'title', 'content'], req, res)) {
						return false;
					}

					var payload = Object.assign({}, req.body);
					payload.tags = payload.tags || [];
					payload.uid = uid;
					payload._uid = uid;
					payload.lastposttime = req.body.lastposttime;
					payload.timestamp = req.body.timestamp;
					Topics.post(payload, function (err, data) {
						// console.log(data.topicData.tid)
						if (!err) {
							data = { tid: data.topicData && data.topicData.tid };
						}
						return errorHandler.handle(err, res, data);
					});
				} else {
					return errorHandler.handle(err, res, {});
				}
			});
		});

	app.route('/:tid')
		.post(apiMiddleware.requireUser, apiMiddleware.validateTid, function (req, res) {
			function getUidByMyquantId(id, cb) {
				db.sortedSetScore('myquantid:uid', id, cb);
			}
			getUidByMyquantId(req.user.uid, function (err, uid) {
				if (!err) {
					if (!utils.checkRequired(['content'], req, res)) {
						return false;
					}
					req.user.uid = uid;
					var payload = {
						tid: req.params.tid,
						uid: req.user.uid,
						req: req,	// For IP recording
						content: req.body.content,
						timestamp: req.body.timestamp,
						timestampISO: req.body.timestampISO,
					};

					if (req.body.toPid) { payload.toPid = req.body.toPid; }

					Topics.reply(payload, function (err, returnData) {
						errorHandler.handle(err, res, returnData);
					});
				} else {
					return errorHandler.handle(err, res, {});
				}
			});

		})
		.delete(apiMiddleware.requireUser, apiMiddleware.validateTid, function (req, res) {
			Topics.purgePostsAndTopic(req.params.tid, req.params._uid, function (err) {
				errorHandler.handle(err, res);
			});
		})
		.put(apiMiddleware.requireUser, function (req, res) {
			if (!utils.checkRequired(['pid', 'content'], req, res)) {
				return false;
			}

			var payload = {
				uid: req.user.uid,
				pid: req.body.pid,
				content: req.body.content,
				options: {}
			};

			// Maybe a "set if available" utils method may come in handy
			if (req.body.handle) { payload.handle = req.body.handle; }
			if (req.body.title) { payload.title = req.body.title; }
			if (req.body.topic_thumb) { payload.options.topic_thumb = req.body.topic_thumb; }
			if (req.body.tags) { payload.options.tags = req.body.tags; }

			Posts.edit(payload, function (err, returnData) {
				errorHandler.handle(err, res, returnData);
			});
		});

	app.route('/:tid/state')
		.put(apiMiddleware.requireUser, apiMiddleware.validateTid, function (req, res) {
			Topics.restore(req.params.tid, req.params._uid, function (err) {
				errorHandler.handle(err, res);
			});
		})
		.delete(apiMiddleware.requireUser, apiMiddleware.validateTid, function (req, res) {
			Topics.delete(req.params.tid, req.params._uid, function (err) {
				errorHandler.handle(err, res);
			});
		});

	app.route('/:tid/follow')
		.put(apiMiddleware.requireUser, apiMiddleware.validateTid, function (req, res) {
			Topics.follow(req.params.tid, req.user.uid, function (err) {
				errorHandler.handle(err, res);
			});
		})
		.delete(apiMiddleware.requireUser, apiMiddleware.validateTid, function (req, res) {
			Topics.unfollow(req.params.tid, req.user.uid, function (err) {
				errorHandler.handle(err, res);
			});
		});

	app.route('/:tid/tags')
		.put(apiMiddleware.requireUser, apiMiddleware.validateTid, function (req, res) {
			if (!utils.checkRequired(['tags'], req, res)) {
				return false;
			}

			Topics.updateTags(req.params.tid, req.body.tags, function (err) {
				errorHandler.handle(err, res);
			});
		})
		.delete(apiMiddleware.requireUser, apiMiddleware.validateTid, function (req, res) {
			Topics.deleteTopicTags(req.params.tid, function (err) {
				errorHandler.handle(err, res);
			});
		});

	app.route('/:uid/posts')
		.get(apiMiddleware.requireUser, function (req, res) {
			if (parseInt(req.params.uid, 10) !== parseInt(req.user.uid, 10)) {
				return errorHandler.respond(401, res);
			}
			var setName = 'uid:' + req.params.uid + ':topics'
			Topics.getTopicsFromSet(setName, req.params.uid, 0, 20, function (err, resp) {
				console.log(resp)
				return errorHandler.handle(err, res)
			});
		});
	
	
	// **DEPRECATED** Do not use.
	app.route('/follow')
		.post(apiMiddleware.requireUser, function (req, res) {
			winston.warn('[write-api] /api/v1/topics/follow route has been deprecated, please use /api/v1/topics/:tid/follow instead.');
			if (!utils.checkRequired(['tid'], req, res)) {
				return false;
			}

			Topics.follow(req.body.tid, req.user.uid, function (err) {
				errorHandler.handle(err, res);
			});
		})
		.delete(apiMiddleware.requireUser, function (req, res) {
			winston.warn('[write-api] /api/v1/topics/follow route has been deprecated, please use /api/v1/topics/:tid/follow instead.');
			if (!utils.checkRequired(['tid'], req, res)) {
				return false;
			}

			Topics.unfollow(req.body.tid, req.user.uid, function (err) {
				errorHandler.handle(err, res);
			});
		});

	return app;
};
