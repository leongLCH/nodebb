'use strict';
/* globals module, require */

var apiMiddleware = require('./middleware'),
	errorHandler = require('../../lib/errorHandler'),
	plugins = require.main.require('./src/plugins'),
	writeApi = module.parent.parent.exports,
	request = require('request'),
	utils = require('./utils'),
	db = require.main.require('./src/database'),
	user = require.main.require('./src/user'),

	nconf = module.parent.require('nconf'),
	async = module.parent.require('async'),
		Categories = require.main.require('./src/categories'),
		_ = module.parent.require('lodash'),
		htmlToText = module.parent.require('html-to-text'),
		Topics = require.main.require('./src/topics'),
		UserNotifications = require.main.require('./src/user/notifications');

module.exports = function (app, coreMiddleware) {
	app.use(function (req, res, next) {
		if (writeApi.settings.requireHttps === 'on' && req.protocol !== 'https') {
			res.set('Upgrade', 'TLS/1.0, HTTP/1.1');
			return errorHandler.respond(426, res);
		} else {
			next();
		}
	});

	app.use('/users', require('./users')(coreMiddleware));
	app.use('/groups', require('./groups')(coreMiddleware));
	app.use('/posts', require('./posts')(coreMiddleware));
	app.use('/topics', require('./topics')(coreMiddleware));
	app.use('/categories', require('./categories')(coreMiddleware));
	app.use('/util', require('./util')(coreMiddleware));

	app.get('/ping', function (req, res) {
		res.status(200).json({
			code: 'ok',
			message: 'pong',
			params: {}
		});
	});

	app.post('/ping', apiMiddleware.requireUser, function (req, res) {
		res.status(200).json({
			code: 'ok',
			message: 'pong, accepted test POST ping for uid ' + req.user.uid,
			params: {
				uid: req.user.uid
			}
		});
	});

	// 补充从终端发帖
	app.post('/postsFromClient', function (req, res) {
		if (req.headers.hasOwnProperty('authorization')) {
			var authorization = req.headers.authorization;
			fetchUserProfile(authorization, function (err, resp) {
				if (!err && resp.user_id) {
					// 
					getUidByMyquantId(resp.user_id, function (error, uid) {
						if (uid) {
							if (!utils.checkRequired(['cid', 'title', 'content'], req, res)) {
								return false;
							}

							var payload = Object.assign({}, req.body);
							payload.tags = payload.tags || []
							payload.uid = uid;
							payload._uid = uid;
							Topics.post(payload, function (e, data) {
								if (!e && data) {
									data = {
										tid: data.topicData && data.topicData.tid
									};
								}
								return errorHandler.handle(err, res, data);
							});
						} else {
							createUser(resp, function (er, r) {
								getUidByMyquantId(resp.user_id, function (error, uid) {
									if (!utils.checkRequired(['cid', 'title', 'content'], req, res)) {
										return false;
									}

									var payload = Object.assign({}, req.body);
									payload.tags = payload.tags || []
									payload.uid = uid;
									payload._uid = uid;
									Topics.post(payload, function (e, data) {
										if (!e) {
											data = {
												tid: data.topicData && data.topicData.tid
											};
										}
										return errorHandler.handle(err, res, data);
									});
								})
							})
						}
					})
				} else {
					return errorHandler.respond(401, res)
				}
			})

		}
	})
	// 查询消息提醒个数
	app.get('/notification', function (req, res) {
		if (req.headers.hasOwnProperty('authorization')) {
			var authorization = req.headers.authorization;
			fetchUserProfile(authorization, function (err, resp) {
				if (!err && resp && resp.user_id) {
					// 
					getUidByMyquantId(resp.user_id, function (error, uid) {
						if (uid) {
							UserNotifications.get(uid, function (err, resp) {
								if (!err) {
									var num = resp.unread && resp.unread.length;
									res.json({
										code: true,
										message: num
									})
								} else {
									res.json({
										code: false,
										message: '获取消息失败'
									})
								}
							})
						} else {
							res.json({
								code: false,
								message: '尚未登录论坛'
							})
						}
					})
				} else {
					return errorHandler.respond(401, res)
				}
			})
		} else {
			return errorHandler.respond(401, res)
		}
	})

	// 终端查询公告
	app.get('/notice', function (req, res) {
		if (req.headers.hasOwnProperty('authorization')) {
			var authorization = req.headers.authorization;
			fetchUserProfile(authorization, function (err, resp) {
				if (!err && resp && resp.user_id) {
					//
					getUidByMyquantId(resp.user_id, function (error, uid) {
						if (uid) {
							Categories.getCategoryById({
								uid,
								cid: '2',
								start: 0,
								stop: req.query.limit ? req.query.limit - 1 : 10,
								sort: 'newest_to_oldest',
							}, function (err, data) {
								if (err) return errorHandler.respond(401, res);
								let topics = data.topics;
								if (_.size(topics) < 1) return res.json({
									data: []
								});
								let response = _.map(topics, t => {
									return {
										tid: t.tid,
										title: t.title,
										created_at: t.timestampISO
									}
								})
								res.json({
									data: response
								})
							})

						} else {
							res.json({
								code: false,
								message: '账户未认证'
							})
						}
					})
				} else {
					return errorHandler.respond(401, res)
				}
			})
		} else {
			return errorHandler.respond(401, res)
		}
	})
	// 更新用户信息
	app.get('/update', function (req, res) {
		var authorization = req.headers.authorization;
		fetchUserProfile(authorization, function (err, resp) {
			if (!err && resp && resp.user_id) {
				// 
				getUidByMyquantId(resp.user_id, function (error, uid) {
					if (!uid) {
						res.send({
							message: '该用户未在论坛登录',
							code: false
						})
					} else {
						user.updateProfile(uid, {
							username: resp.nick_name,
							myquantid: resp.user_id,
							picture: resp.avatar_file || '',
							uid: uid,
							aboutme: resp.introduction
						}, function (err) {
							if (!err) {
								res.send({
									'message': '用户信息修改成功',
									code: true
								})
							} else {
								res.send({
									'message': '用户信息修改失败',
									code: false
								})
							}
						})
					}
				})
			} else {
				return errorHandler.respond(401, res)
			}
		})
	})


	// 搜索置顶话题
	app.get('/toptopic', function (req, res) {
		// var user = req.body;
		async.waterfall([
			function (next) {
				user.getSettings(req.uid, next)
			},
			function (settings, next) {
				var start = 0;
				var stop = 4;
				Categories.getCategoryById({
					uid: req.uid,
					cid: '0',
					start: start,
					stop: req.query.num || stop,
					sort: settings.categoryTopicSort,
					settings: settings,
					query: {},
					targetUid: 0
				}, next);
			}
		], function (err, data) {
			if (err || !data.topics) {
				errorHandler.handle(401, [])
			}
			let arr = _.filter(data.topics, function (value) {
				return value.pinned
			});
			var newA = [];
			_.forEach(arr, function (value) {
				var content = value.mainPost && value.mainPost.content;
				var text;
				if (content) {
					text = htmlToText.fromString(content, {
						hideLinkHrefIfSameAsText: true,
						ignoreHref: true,
						ignoreImage: true,
						wordwrap: 20,
						singleNewLineParagraphs: true,
					});
				} else {
					text = ''
				}

				var img = value.user && value.user.picture;
				var uid = value.user && value.user.uid;
				var username = value.user && value.user.username;

				var thumbnail = cutByte(text, 500);
				var releaseT = {
					year: getFormateTime(value.timestamp, 'year'),
					date: getFormateTime(value.timestamp, 'date'),
				};
				newA.push({
					tid: value.tid,
					title: value.title,
					category: value.category && value.category.name,
					thumbnail: thumbnail,
					cid: value.cid,
					user: {
						img,
						uid,
						username
					},
					releaseT: releaseT
				})
			})

			errorHandler.handle(err, res, newA)
		})
	})


	function fetchUserProfile(token, cb) {
		var url = nconf.get('sso').url + nconf.get('sso').getBaseInfo;
		var options = {
			url: url,
			headers: {
				Authorization: token
			},
			timeout: 2000
		};
		request(options, function callback(error, response, body) {
			if (!error && response.statusCode === 200) {
				try {
					cb(null, JSON.parse(body));
				} catch (e) {
					cb(e, null);
				}
			} else {
				cb(error, null);
			}
		});

	}

	function getUidByMyquantId(id, cb) {
		db.sortedSetScore('myquantid:uid', id, cb);
	}

	function createUser(profile, cb) {
		user.create({
			username: profile.nick_name,
			myquantid: profile.user_id,
			picture: profile.avatar_file || '',
			aboutme: profile.introduction
		}, cb);
	}
	// This router is reserved exclusively for plugins to add their own routes into the write api plugin. Confused yet? :trollface:
	var customRouter = require('express').Router();
	plugins.fireHook('filter:plugin.write-api.routes', {
		router: customRouter,
		apiMiddleware: apiMiddleware,
		middleware: coreMiddleware,
		errorHandler: errorHandler
	}, function (err, payload) {
		app.use('/', payload.router);

		app.use(function (req, res) {
			// Catch-all
			errorHandler.respond(404, res);
		});
	});

	function getFormateTime(s, type) {
		if (!s || typeof s !== 'number') return '-';

		function formate(time) {
			var zero = '0';
			return time < 10 ? zero + time : time;
		}
		var str;
		switch (type) {
		case 'year':
			str = new Date(s).getFullYear();
			break;
		case 'date':
			str = formate(new Date(s).getMonth() + 1) + '/' + formate((new Date(s)).getDate());
			break;
		default:
			str = new Date(s).getFullYear() + '-' + formate(new Date(s).getMonth() + 1) + '-' + formate((new Date(s)).getDate()) + ' ' + formate(new Date(s).getHours()) + ':' + formate(new Date(s).getMinutes()) + ':' + formate(new Date(s).getSeconds());
			break;
		}
		return str;
	}

	function cutByte(str, len, endstr) {
		if (!str) {
			return '';
		}
		str = escapeHTML(str);
		var lens = +len;
		var endstrs = typeof (endstr) === 'undefined' ? '...' : endstr.toString();

		function n2(a) {
			var n = (a / 2) | 0;
			return (n > 0 ? n : 1)
		} // 用于二分法查找
		if (!(str).length || !lens || lens <= 0) {
			return '';
		}
		var lenS = lens;
		var _lenS = 0;
		var _strl = 0;
		while (_strl <= lenS) {
			var _lenS1 = n2(lenS - _strl);
			var addn = getBlength(str.substr(_lenS, _lenS1));
			if (addn === 0) {
				return str
			}
			_strl += addn
			_lenS += _lenS1
		}
		if (str.length > _lenS || getBlength(str.substring(_lenS - 1))) {
			return str.substr(0, _lenS - 1) + endstrs
		} else {
			return str
		}
	}

	var freeze = Object.freeze || function (obj) {
		return obj;
	};


	var escapeChars = /[&<>"'`=]/g;

	function replaceChar(c) {
		return escapeCharMap[c];
	}

	var escapeCharMap = freeze({
		'&': '&amp;',
		'<': '&lt;',
		'>': '&gt;',
		'"': '&quot;',
		"'": '&#x27;',
		'`': '&#x60;',
		'=': '&#x3D;',
	});

	function escapeHTML(str) {
		if (str == null) {
			return '';
		}
		if (!str) {
			return String(str);
		}

		return str.toString().replace(escapeChars, replaceChar);
	}

	function getBlength(str) {
		var n = 0;
		for (var i = str.length; i--;) {
			n += str.charCodeAt(i) > 255 ? 2 : 1;
		}
		return n;
	}
	return app;
};