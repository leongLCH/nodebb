'use strict';

const cp = require('child_process');
// spawn the child using the same node process as ours
var child = cp.spawn(process.execPath, ['./nodebb', 'start'], { detached: true, cwd: __dirname });

child.stdout.on('data', function (data) {
	console.log(data.toString());
})
child.stderr.on('data', function (data) {
	console.log(data.toString());
})
// required so the parent can exit
child.unref();

setTimeout(process.exit, 3000, 0);