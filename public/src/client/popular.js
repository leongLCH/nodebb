'use strict';


define('forum/popular', ['forum/recent', 'components', 'forum/infinitescroll'], function (recent, components, infinitescroll) {
	var Popular = {};
	

	Popular.init = function () {
		app.enterRoom('popular_topics');

		// 添加
		Popular.recentSelectEvent();

		components.get('popular/tab')
			.removeClass('active')
			.find('a[href="' + window.location.pathname + '"]')
			.parent().addClass('active');

		if (!config.usePagination) {
			infinitescroll.init(loadMoreTopics);
		}

		// 
		initSideBanner()
		initTopBanner()
	};

	Popular.recentSelectEvent = function () {
		$('[component="category/popularSelect"] .category-select').on('click', function () {
			$('.category-select-dropdown').toggle();
			return false;
		});
		$(document).on('click', function () {
			$('.category-select-dropdown').hide();
		});
		$('.category-select-side li').on('click', function () {
			var slug = $(this).attr('data-cid');
			ajaxify.go('popular' + (slug > 0 ? '?cid[]=' + slug : ''));
			return false;
		});
	};
	function loadMoreTopics(direction) {
		if (direction < 0 || !$('[component="category"]').length) {
			return;
		}

		infinitescroll.loadMore('topics.loadMorePopularTopics', {
			after: $('[component="category"]').attr('data-nextstart'),
			count: config.topicsPerPage,
			term: ajaxify.data.term,
		}, function (data, done) {
			if (data.topics && data.topics.length) {
				recent.onTopicsLoaded('popular', data.topics, false, done);
				$('[component="category"]').attr('data-nextstart', data.nextStart);
			} else {
				done();
			}
		});
	}

	function initSideBanner() {
		var goldminer = $('#advert').attr('data-url');
		if (!goldminer) return;
		var url = goldminer + '/api-banner';
		$.get(url + '?identify=bbs-sidebar', function (data) {
			if (data.errcode == 0) {
				var arr = data.data;
				var str = '';
				arr.forEach(function (value, key) {
					str += '<a href="' +
						value.URL +
						'" target="_blank"><img src="' +
						goldminer +
						value.Picture +
						'"/></a>'
				})
				$('#advert').html(str)
			}
		})
	}

	function initTopBanner() {
		var goldminer = $('#topicBanner').attr('data-url');
		if (!goldminer) return;
		var url = goldminer + '/api-banner';
		$.get(url + '?identify=bbs-topbanner', function (data) {
			if (data.errcode == 0) {
				var arr = data.data;
				if (!arr[0]) return;
				var img = goldminer + arr[0].Picture;
				$('#topicBanner').css('background', 'url(' + img + ') center')
			}
		})
	}
	return Popular;
});
