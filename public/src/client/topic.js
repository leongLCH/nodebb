'use strict';


define('forum/topic', [
	'forum/infinitescroll',
	'forum/topic/threadTools',
	'forum/topic/postTools',
	'forum/topic/events',
	'forum/topic/posts',
	'forum/topic/images',
	'forum/topic/replies',
	'navigator',
	'sort',
	'components',
	'storage',
], function (infinitescroll, threadTools, postTools, events, posts, images, replies, navigator, sort, components, storage) {
	var Topic = {};
	var currentUrl = '';


	$(window).on('action:ajaxify.start', function (ev, data) {
		if (Topic.replaceURLTimeout) {
			clearTimeout(Topic.replaceURLTimeout);
			Topic.replaceURLTimeout = 0;
		}

		if (!String(data.url).startsWith('topic/')) {
			navigator.disable();
			components.get('navbar/title').find('span').text('').hide();
			app.removeAlert('bookmark');

			events.removeListeners();

			require(['search'], function (search) {
				if (search.topicDOM.active) {
					search.topicDOM.end();
				}
			});
		}
	});

	Topic.init = function () {
		var tid = ajaxify.data.tid;
		// 高亮关键字
		highlight();

		$(window).trigger('action:topic.loading');

		app.enterRoom('topic_' + tid);

		posts.processPage(components.get('post'));

		postTools.init(tid);
		threadTools.init(tid);
		replies.init(tid);
		events.init();

		sort.handleSort('topicPostSort', 'user.setTopicSort', 'topic/' + ajaxify.data.slug);

		if (!config.usePagination) {
			infinitescroll.init($('[component="topic"]'), posts.loadMorePosts);
		}

		addBlockQuoteHandler();

		addParentHandler();

		navigator.init('[component="post"]', ajaxify.data.postcount, Topic.toTop, Topic.toBottom, Topic.navigatorCallback, Topic.calculateIndex);

		handleBookmark(tid);

		$(window).on('scroll', updateTopicTitle);

		handleTopicSearch();

		$(window).trigger('action:topic.loaded', ajaxify.data);

		generateQote();

		// 
		initSideBanner()

		//
		initTopicBanner()
		$('#g-wx').hover(function (e) {
			e.preventDefault();
			$('.g-qrcode-banner').show();
		}, function (e) {
			e.preventDefault();
			$('.g-qrcode-banner').hide();
		});

		handleImg();
		cloneStrategyEvent();

		setupFavouriteMorph()
	};

	// 获取底部banner
	function initTopicBanner() {
		var goldminer = $('#bottombanner').attr('data-url');
		if (!goldminer) return;
		var url = goldminer + '/api-banner';
		$.get(url + '?identify=bbs-topicbanner', function (data) {
			if (data.errcode == 0) {
				var arr = data.data;
				if (!arr[0]) return;
				var str = '<a href="' +
					arr[0].URL +
					'" target="_blank"><img src="' + goldminer +
					arr[0].Picture +
					'"/></a>'
				$('#bottombanner').html(str)
			}
		})
	}

	function initSideBanner() {
		var goldminer = $('#advert').attr('data-url');
		if (!goldminer) return;
		var url = goldminer + '/api-banner';
		$.get(url + '?identify=bbs-sidebar', function (data) {
			if (data.errcode == 0) {
				var arr = data.data;
				var str = '';
				arr.forEach(function (value, key) {
					str += '<a href="' +
						value.URL +
						'" target="_blank"><img src="' +
						goldminer +
						value.Picture +
						'"/></a>'
				})
				$('#advert').html(str)
			}
		})
	}
	
	function setupFavouriteMorph() {
		$('.btn-morph').click(function (ev) {
			var type = $(this).hasClass('plus') ? 'unfollow' : 'follow';
			var uid = $(this).attr('data-uid');
			var username = $(this).attr('data-username')
			var ln = $(this).attr('data-ln')
			socket.emit('user.' + type, {
				uid: uid
			}, function (err) {
				if (err) {
					return app.alertError(err.message);
				}

				app.alertSuccess('[[global:alert.' + type + ', ' + username + ']]');
			});

			$(this).toggleClass('plus').toggleClass('heart');
			$(this).text(type === 'follow' ? '已关注' : '关注TA')

			// if ($(this).find('b.drop').length === 0) {
			// 	$(this).prepend('<b class="drop"></b>');
			// }

			// var drop = $(this).find('b.drop').removeClass('animate'),
			// 	x = ev.pageX - drop.width() / 2 - $(this).offset().left,
			// 	y = ev.pageY - drop.height() / 2 - $(this).offset().top;

			// drop.css({top: y + 'px', left: x + 'px'}).addClass('animate');
		});
	}

	function cloneStrategyEvent() {
		var downloadUrl
		$(".clone-strategy-btn").click(function (event) {
			var id = $(this).parent().children('iframe').attr('data-id');
			downloadUrl = $(this).parent().children('iframe').attr('data-downLoad');
			var center = {
				from: 'bbs',
				action: 'clone',
				strategyId: id
			};
			var href = "goldminer://" + btoa(JSON.stringify(center));
			window.protocolCheck(href,
				function () {
					$('body').append('<div id="protocolcheck"><div class="modal-content"><div class="modal-header">消息提示 <small class="clone-btn-sure">x</small></div><div class="modal-body"><div class="warning"><div class="warn-item left"></div><div class="warn-item right"><ul><li>克隆策略需要先启动终端！</li><li>您还没安装掘金量化终端，无法克隆此策略。</li></ul></div></div><div class="tips"><ul><li>1. 未安装终端，点击 <a href="javascript:void(0);" class="btn-download" id="clone-btn-download">下载安装</a>。</li><li>2. 已安装终端，点击重试或手动复制策略源码到终端新建。</li></ul></div></div><div class="modal-footer"><div class="btn btn-ensure clone-btn-sure" >确定</div></div></div></div>');
					$(".clone-btn-sure").click(function () {
						$("#protocolcheck").remove();
					});
					$("#clone-btn-download").click(function () {
						var url = downloadUrl;
						$.get(url, function (res) {
							if (res && res.url) {
								location.href = res.url;
							} else {
								console.log("下载失败==》", res);
							}
						})
					});
				});
			event.preventDefault ? event.preventDefault() : event.returnValue = false;
		});
	}

	function generateQote() {
		var url = $('#g-wx').attr('data-url');
		new QRCode($('#g-qrcode').get(0), {
			text: url,
			width: 128,
			height: 128,
			colorDark: '#000000',
			colorLight: '#ffffff',
			correctLevel: QRCode.CorrectLevel.H
		});
	}

	function handleImg() {
		var imgs = $('.topic-container img');
		if (imgs.length > 0) {
			imgs.each(function (i) {
				var src = $(this).attr('src');
				var newUrl = src.replace('-resized', '');
				$(this).wrap('<a href="' + newUrl + '" data-lightbox="image"></a>');
			})
		}
	}

	function handleTopicSearch() {
		require(['search', 'mousetrap'], function (search, mousetrap) {
			$('.topic-search').off('click')
				.on('click', '.prev', function () {
					search.topicDOM.prev();
				})
				.on('click', '.next', function () {
					search.topicDOM.next();
				});

			mousetrap.bind('ctrl+f', function (e) {
				if (config.topicSearchEnabled) {
					// If in topic, open search window and populate, otherwise regular behaviour
					var match = ajaxify.currentPage.match(/^topic\/([\d]+)/);
					var tid;
					if (match) {
						e.preventDefault();
						tid = match[1];
						$('#search-fields input').val('in:topic-' + tid + ' ');
						app.prepareSearch();
					}
				}
			});
		});
	}

	function highlight() {
		var html = $('.topic .topic-container').html();
		var title = $('.topic .topic-title').html();
		var keyword = $('.topic').attr('data-search');
		if (!keyword) return false;
		var keywords = keyword.split(',');
		for (var i = 0; i < keywords.length; i++) {
			var re = new RegExp(keywords[i], 'g');
			var newHtml = html.replace(/([^>]*)(<[^<>]+>)([^<]*)/gi, function (a, b, c, d) {
				return b.replace(re, `<span class="highkeyword">${keywords[i]}</span>`) + c + d.replace(re, `<span class="highkeyword">${keywords[i]}</span>`);
			});
			var newTitle = title.replace(re, `<span class="highkeyword">${keywords[i]}</span>`)
		}
		$('.topic .topic-container').html(newHtml);
		$('.topic .topic-title').html(newTitle)
	}
	Topic.toTop = function () {
		navigator.scrollTop(0);
	};

	Topic.toBottom = function () {
		socket.emit('topics.postcount', ajaxify.data.tid, function (err, postCount) {
			if (err) {
				return app.alertError(err.message);
			}
			if (config.topicPostSort !== 'oldest_to_newest') {
				postCount = 2;
			}
			navigator.scrollBottom(postCount - 1);
		});
	};

	function handleBookmark(tid) {
		// use the user's bookmark data if available, fallback to local if available
		var bookmark = ajaxify.data.bookmark || storage.getItem('topic:' + tid + ':bookmark');
		var postIndex = ajaxify.data.postIndex;

		if (postIndex > 0) {
			if (components.get('post/anchor', postIndex - 1).length) {
				return navigator.scrollToPostIndex(postIndex - 1, true, 0);
			}
		} else if (bookmark && (!config.usePagination || (config.usePagination && ajaxify.data.pagination.currentPage === 1)) && ajaxify.data.postcount > ajaxify.data.bookmarkThreshold) {
			app.alert({
				alert_id: 'bookmark',
				title: '提示',
				message: '[[topic:bookmark_instructions]]',
				timeout: 0,
				type: 'info',
				clickfn: function () {
					navigator.scrollToIndex(parseInt(bookmark - 1, 10), true);
				},
				closefn: function () {
					storage.removeItem('topic:' + tid + ':bookmark');
				},
			});
			setTimeout(function () {
				app.removeAlert('bookmark');
			}, 10000);
		}
	}

	function addBlockQuoteHandler() {
		components.get('topic').on('click', 'blockquote .toggle', function () {
			var blockQuote = $(this).parent('blockquote');
			var toggle = $(this);
			blockQuote.toggleClass('uncollapsed');
			var collapsed = !blockQuote.hasClass('uncollapsed');
			toggle.toggleClass('fa-angle-down', collapsed).toggleClass('fa-angle-up', !collapsed);
		});
	}

	function addParentHandler() {
		components.get('topic').on('click', '[component="post/parent"]', function (e) {
			var toPid = $(this).attr('data-topid');

			var toPost = $('[component="topic"]>[component="post"][data-pid="' + toPid + '"]');
			if (toPost.length) {
				e.preventDefault();
				navigator.scrollToIndex(toPost.attr('data-index'), true);
				return false;
			}
		});
	}

	function updateTopicTitle() {
		var span = components.get('navbar/title').find('span');
		if ($(window).scrollTop() > 50 && span.hasClass('hidden')) {
			span.html(ajaxify.data.title).removeClass('hidden');
		} else if ($(window).scrollTop() <= 50 && !span.hasClass('hidden')) {
			span.html('').addClass('hidden');
		}
		if ($(window).scrollTop() > 300) {
			app.removeAlert('bookmark');
		}
	}

	Topic.calculateIndex = function (index, elementCount) {
		if (index !== 1 && config.topicPostSort !== 'oldest_to_newest') {
			return elementCount - index + 2;
		}
		return index;
	};

	Topic.navigatorCallback = function (index, elementCount, threshold) {
		var path = ajaxify.removeRelativePath(window.location.pathname.slice(1));
		if (!path.startsWith('topic')) {
			return;
		}

		if (navigator.scrollActive) {
			return;
		}

		images.loadImages(threshold);
		var newUrl = 'topic/' + ajaxify.data.tid + (index > 1 ? ('/' + index) : '');
		if (newUrl !== currentUrl) {
			if (Topic.replaceURLTimeout) {
				clearTimeout(Topic.replaceURLTimeout);
			}

			Topic.replaceURLTimeout = setTimeout(function () {
				if (index >= elementCount && app.user.uid) {
					socket.emit('topics.markAsRead', [ajaxify.data.tid]);
				}

				updateUserBookmark(index);

				Topic.replaceURLTimeout = 0;
				if (history.replaceState) {
					var search = window.location.search || '';
					if (!config.usePagination) {
						search = (search && !/^\?page=\d+$/.test(search) ? search : '');
					}

					history.replaceState({
						url: newUrl + search,
					}, null, window.location.protocol + '//' + window.location.host + RELATIVE_PATH + '/' + newUrl + search);
				}
				currentUrl = newUrl;
			}, 500);
		}
	};

	function updateUserBookmark(index) {
		var bookmarkKey = 'topic:' + ajaxify.data.tid + ':bookmark';
		var currentBookmark = ajaxify.data.bookmark || storage.getItem(bookmarkKey);

		if (ajaxify.data.postcount > ajaxify.data.bookmarkThreshold && (!currentBookmark || parseInt(index, 10) > parseInt(currentBookmark, 10) || ajaxify.data.postcount < parseInt(currentBookmark, 10))) {
			if (app.user.uid) {
				socket.emit('topics.bookmark', {
					tid: ajaxify.data.tid,
					index: index,
				}, function (err) {
					if (err) {
						return app.alertError(err.message);
					}
					ajaxify.data.bookmark = index;
				});
			} else {
				storage.setItem(bookmarkKey, index);
			}
		}

		// removes the bookmark alert when we get to / past the bookmark
		if (!currentBookmark || parseInt(index, 10) >= parseInt(currentBookmark, 10)) {
			app.removeAlert('bookmark');
		}
	}


	return Topic;
});
