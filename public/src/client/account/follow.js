'use strict';


define('forum/account/follow', ['forum/account/header'], function (header) {
	var Followers = {};

	function setupFollowOther() {
		$('.follow-other').find('.btn-morph').click(function () {
			var type = $(this).hasClass('plus') ? 'unfollow' : 'follow';
			var uid = $(this).attr('data-uid');
			var username = $(this).attr('data-name')
			socket.emit('user.' + type, { uid: uid }, function (err) {
				if (err) {
					return app.alertError(err.message);
				}
				app.alertSuccess('[[global:alert.' + type + ', ' + username + ']]');
			});
			$(this).toggleClass('plus').toggleClass('heart');
			$(this).text(type === 'follow' ? '已关注' : '关注TA')
		});
	}

	Followers.init = function () {
		header.init();
		setupFollowOther();
	};

	return Followers;
});
