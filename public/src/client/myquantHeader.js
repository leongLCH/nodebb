'use strict';

define('forum/myquantHeader', ['components'], function (components) {
    $("#header .dropdown-menu li a").click(function (e) {
        e.stopPropagation();//阻止事件冒泡
    })
    $("#header .dropdown-menu li .show-sub-menu").click(function (e) {
        e.stopPropagation();//阻止事件冒泡
        var open = $(this).attr("data-open");
        if (open == "true") {
            $(this).attr("data-open", "false");
            $(this).siblings(".sub-menu").slideUp(100);
        } else {
            $(this).attr("data-open", "true");
            $(this).siblings(".sub-menu").slideDown(100);
        }
    })
    $('#community').click(function (e) {
        e.stopPropagation();
        window.location.href = config.url;
    })
    $('#hideNav').click(function (e) {
        e.stopPropagation();
        window.location.href = config.url;
    })

    $('.nav-redirect').click(function(e) {
        e.preventDefault();
        location.href = config.myquantUrlDomain + $(this).attr("href") + "?redirect=" + encodeURIComponent(location.href);
    })
    components.get('notifications/mark_all').on('click', function () {
        socket.emit('notifications.markAllRead', function (err) {
            if (err) {
                return app.alertError(err.message);
            }

            components.get('notifications/iconbell').addClass('hidden');
        });
    });
});
