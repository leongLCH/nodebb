
$(function () {
    var downloadUrl
    $(".clone-strategy-btn").click(function (event) {
        var id = $(this).parent().children('iframe').attr('data-id');
        downloadUrl = $(this).parent().children('iframe').attr('data-downLoad');
        var center = {
            from: 'bbs',
            action: 'clone',
            strategyId: id
        };
        var href = "goldminer://" + btoa(JSON.stringify(center));
        window.protocolCheck(href,
            function () {
                $('body').append('<div id="protocolcheck"><div class="modal-content"><div class="modal-header">消息提示 <small class="clone-btn-sure">x</small></div><div class="modal-body"><div class="warning"><div class="warn-item left"></div><div class="warn-item right"><ul><li>克隆策略需要先启动终端！</li><li>您还没安装掘金量化终端，无法克隆此策略。</li></ul></div></div><div class="tips"><ul><li>1. 未安装终端，点击 <a href="javascript:void(0);" class="btn-download" id="clone-btn-download">下载安装</a>。</li><li>2. 已安装终端，点击重试或手动复制策略源码到终端新建。</li></ul></div></div><div class="modal-footer"><div class="btn btn-ensure clone-btn-sure" >确定</div></div></div></div>');
                $(".clone-btn-sure").click(function () {
                    $("#protocolcheck").remove();
                });
                $("#clone-btn-download").click(function () {
                    var url = downloadUrl;
                    $.get(url, function (res) {
                        if (res && res.url) {
                            location.href = res.url;
                        } else {
                            console.log("下载失败==》", res);
                        }
                    })
                });
            });
        event.preventDefault ? event.preventDefault() : event.returnValue = false;
    });

});
