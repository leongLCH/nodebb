### 本地初次运行启动
1. 克隆项目```git clone git@bitbucket.org:waisiukei/nodebb.git```
2. 安装依赖 ```npm i```  node版本在10以上
3. 配置mongodb

    mongo 默认的端口是27017

    - 3.1 特别注意，mongodb的版本只能在4一下版本，不能包含4，否则无法运行。建议用3的版本

    - 3.2 mongo按照提示安装完后(customer),安装完后需要将mongodb bin目录添加到环境变量，例如：```C:\Program Files\MongoDB\Server\3.0\bin```

    - 3.3 需要指定数据库目录，和日志目录,首先在指定位置创建两个文件夹，例如：```C:\MongoDB\data```, ```C:\MongoDB\logs```

    - 3.4 启动mongodb，指定日志目录和数据库存储目录,管理员权限启动cmd

    ```mongod --dbpath "c://MongoDB//data" --logpath "c://MongoDB//logs//mongodb.log" --logappend```

    mongod --dbpath 命令是创建数据库文件的存放位置，启动mongodb服务时需要先确定数据库文件存放的位置，否则系统不会自动创建，启动会不成功。--logpath 表示日志文件存放的路径     --logappend  表示以追加的方式写日志文件

    打开浏览器，输入url: ```localhost:27017```,如果有内容显示，表明已经成功启动。

    - 3.5 每次启动服务都需要输入以上命令，为了方便，可以将启动数据库写成window服务的方式。

    ```mongod --logpath "d://mongodb//log//mongodb.log" --logappend --dbpath "d://mongodb//data" --directoryperdb --serviceName MongoDB --install```


    - 3.6 创建用户
    > mongo
    
    切换admin创建数据库管理员
    
    > use admin
    
    创建用户
    
    > db.createUser( { user: "admin", pwd: "123456", roles: [ { role: "root", db: "admin" } ] } )

    创建nodebb数据库表

    > use nodebb

    创建nodebb管理员

    > db.createUser( { user: "nodebb", pwd: "123456", roles: [ { role: "readWrite", db: "nodebb" }, { role: "clusterMonitor", db: "admin" } ] } )


    退出
    > quit()


tip: 可以下载mongodb的可视化界面，推荐一个简单的小工具
    [adminMongo](git@github.com:mrvautin/adminMongo.git)

4. 安装启动nodebb
    config.json是nodebb所有的配置文件，项目开始时config.json 是一个空的json文件，只有一个{}对象

    第一次使用需要初始化,初始化会设置数据库信息，论坛管理员账号密码。
    默认端口为 4567

    > ./nodebb setup

    初始化完需要将自定义插件放置到node_modules参与构建，放置完后运行（覆盖）

    > ./nodebb build

    dev模式运行项目
    > ./nodebb dev


5. 访问 localhost：4567，进入管理员界面  localhost:4567/admin, 输入在 setup环境配置的管理员账户密码进入后台管理界面。

6. 选择语言
![](./images/language.png)

7. 选择主题，目前掘金论坛使用的主题是goldminer2，选择完重新构建，并重启。
![](./images/主题.png)


8. 本地环境需要配置nginx方向代理，使用宝塔软件方便配置，下载地址：[宝塔](https://www.bt.cn/)



### 配置文件说明
config.json配置文件每次修改后不需要上传，因为开发环境和生产环境的配置会有不同，有需要添加的配置在生产环境中手动添加。每次更新生产环境需要备份！
```
{
	"url": "http://bbs.abc.com", // 论坛地址
	"secret": "804f8fc2-66fe-403a-a060-b9600ce4a298", // 用户体系密钥
	"database": "mongo", // 数据库
	"mongo": {              // mongo配置信息，在初始化数据库的时候设置，需要保持一致
		"host": "127.0.0.1",
		"port": "27017",
		"username": "nodebb",
		"password": "123",
		"database": "nodebb",
		"uri": ""
	},
	"domain": { // 官网相关连接，header使用
		"url": "http://www.abc.com", // 官网地址
		"domain": ".abc.com", // 域名
		"login": "/user/login",
		"logout": "user/logout",
		"reg": "/user/reg",
		"joinus": "/joinus",
		"guide": "/docs/guide", //  文档地址
		"data": "/docs/data",
		"python": "/docs/python",
		"strategy": "/docs/python_strategyies",
		"gm2": "https://www.myquant.cn/gm2/", // 掘金2
		"search": "/search"
	},
	"share": { // 论坛分享相关外部链接
		"logo": "https://www.myquant.cn/static/images/logo-share.png",
		"weibo": "http://service.weibo.com/share/share.php",
		"xueqiu": "https://xueqiu.com/service/share"
	},
	"colbugsUrl": "https://www.myquant.cn/colbugs/stat/colbugs.umd.js", // 上报脚本
	"strategyCenter": "http://test.sitestack.cn:7104/viewer/v1/embed/", // 策略中心相关地址
	"thumb": "http://test.sitestack.cn:7104/viewer/v1/snapshot/",
	"getStrategies": "http://test.sitestack.cn:7102/v3/strategy-center/strategies",
	"getStrategy": "http://test.sitestack.cn:7102/v3/strategy-center/strategy",
	"downLoadGoldminer": "http://gm3-sc.myquant.cn/api/update/win32/stable/last/", // 掘金3下载地址
	"migrationUser": {
		"token": "64c6e783-b027-43f4-9586-3df494763979",
		"url": "http://www.goldminer.com/community/api/v2/users/migrateUser" // 论坛提供给掘金2论坛迁移的api，和token
	},
	"sso": {
		"url": "https://sso.myquant.cn",
		"getBaseInfo": "/api/v3/get_base_info/"
	},
	"elasticsearch": "192.168.0.176:9200" // 搜索
}

```

### 用户数据迁移
```npm run migrate```


### 注意事项
plugins文件夹包括了所有论坛需要用到的插件。
composer-goldminer   论坛发帖编辑
markdown             基础markdown编辑器规则
emoji                发帖支持emoji
session              处理官网和论坛认证
write-api            官网对外暴露的api(终端使用)
goldminer            主题1  已经作废
goldminer2           主题2  当前主题




### 部署注意事项
0. 部署前切记需要备份
1. git pull origin mastter
2. 如果有依赖发生更新需要  npm install
3. 如果有plugin发生改变   npm run postinstall 将 plugin移动到node_modules
4. 如果有客户端代码发生改变   ./nodebb build
5. 重启服务   sudo pm2 restart community


### 后台管理
哪些功能可以通过后台管理进行配置
1. 板块的新增删除修改
2. 用户的拉黑
3. 管理员的新增删除
4. 运营群发功能
5. 其他。。。