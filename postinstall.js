'use strict';

const fs = require('fs-extra');
const path = require('path');
const exec = require('child-process-promise').exec;
fs.readdir('./plugins')
	.then(function (dirs) {
		var promises = dirs.map(function (dir) {
			if (dir !== 'nodebb-plugin-emoji') {
				return exec('cd ./plugins/' + dir + ' && npm i').then((results) => {
					if (!results.error) {
						return fs.copy(path.resolve(__dirname, './plugins/' + dir), path.resolve(__dirname, './node_modules/' + dir));
					}
				});
			}
		});
		return Promise.all(promises);
	}).then(function () {
		return fs.copy(path.resolve(__dirname, './scripts/emoji/android'), path.resolve(__dirname, './plugins/nodebb-plugin-emoji/build/emoji/android'));
	}).then(function () {
		return fs.copy(path.resolve(__dirname, './plugins/nodebb-plugin-emoji'), path.resolve(__dirname, './node_modules/nodebb-plugin-emoji'));
	}).then(function () {
		return fs.copy(path.resolve(__dirname, './plugins/nodebb-plugin-emoji/build/emoji'), path.resolve(__dirname, './build/public/plugins/nodebb-plugin-emoji/emoji'));
	});
