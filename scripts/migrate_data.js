/* eslint-disable */

'use strict';
const fs = require('fs');
const path = require('path');
const request = require('request');
const async = require('async');

fs.readFile(path.resolve(__dirname, '../config.json'), function(err, data) {
    if(err) throw err;
    var migrationUser = JSON.parse(data).migrationUser;
    var token = migrationUser.token;
    var url = migrationUser.url;
    migrateUsers(token, url)
})
function migrateUsers(token, url) {
    var filePath = path.resolve(__dirname, './migration');
    fs.readdir(filePath, function (err, files) {
        if (err) {
            throw err;
        }
        files.forEach(function (fileName) {
            var fileDir = path.join(filePath, fileName);
            var fileR = JSON.parse(fs.readFileSync(fileDir))
            createUser(token, { users: fileR }, url, function(err, resp) {
                console.log(resp)
                if(err) {
                    console.log(err)
                }
            })
        })
    })
}

function createUser(token, user, url, cb) {
    var options = {
        url: url,
        headers: { Authorization: 'Bearer ' + token, "Content-Type": 'application/json' },
        timeout: 10000,
        method: 'POST',
        form: user,
        pool: { maxSockets: Infinity },
    };

    request(options, function callback(error, response, body) {
        if (!error && response.statusCode === 200) {
            try {
                cb(null, JSON.parse(body));
            } catch (e) {
                cb(e, null);
            }
        } else {
            cb(error, null);
        }
    });
}